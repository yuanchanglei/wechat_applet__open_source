package com.platform.api;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.collections.MapUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.platform.annotation.IgnoreAuth;
import com.platform.entity.LoginInfo;
import com.platform.entity.MlsUserEntity2;
import com.platform.entity.SysUserVo;
import com.platform.service.ApiSysUserService;
import com.platform.service.MlsUserSer;
import com.platform.service.TokenService;
import com.platform.util.ApiBaseAction;
import com.platform.util.ApiUserUtils;
import com.platform.utils.Base64;
import com.platform.utils.R;
import com.platform.validator.Assert;
import com.qiniu.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * API登录授权
 *
 * @author lipengjun
 * @email 939961241@qq.com
 * @date 2017-03-23 15:31
 */
@Api(tags = "API商户登录授权接口")
@RestController
@RequestMapping("/api/sysUserAuth")
public class ApiSysAuthController extends ApiBaseAction {
    private Logger logger = Logger.getLogger(getClass());
    @Autowired
    private ApiSysUserService sysUserService;
    @Autowired
    private TokenService tokenService;
    @Autowired
    private RestTemplate restTemplate;
    @Autowired
	private MlsUserSer mlsUserSer;
    /**
     * 登录
     */
    @IgnoreAuth
    @PostMapping("sysUserLogin")
    @ApiOperation(value = "商户手机登录接口")
    public R login(String mobile, String password) {
        Assert.isBlank(mobile, "手机号不能为空");
        Assert.isBlank(password, "密码不能为空");

        //用户登录
        long userId = sysUserService.login(mobile, password);

        //生成token
        Map<String, Object> map = tokenService.createToken(userId);

        return R.ok(map);
    }

    /**
     * 商户微信登录或注册（第一次登陆会注册，且等待审核）  
     */
    @ApiOperation(value = "商户微信登录")
    @IgnoreAuth
    @PostMapping("sysUserlogin_by_weixin")
    public Object loginByWeixin(@RequestBody LoginInfo loginInfo,HttpServletRequest request) {

        //获取openid
        String requestUrl = ApiUserUtils.getWebAccess(loginInfo.getCode());//通过自定义工具类组合出小程序需要的登录凭证 code
        logger.info("》》》组合token为：" + requestUrl);
        String res = restTemplate.getForObject(requestUrl, String.class);
        logger.info("res=="+res);
        JSONObject sessionData = JSON.parseObject(res);
        String openid=sessionData.getString("openid");
        logger.info("》》》promoterId：" + loginInfo.getPromoterId());
        String session_key=sessionData.getString("session_key");//不知道啥用。
        if (null == sessionData || StringUtils.isNullOrEmpty(openid)) {
            return toResponsFail("登录失败");
        }
        Date nowTime = new Date();
        SysUserVo sysUserVo = sysUserService.queryByOpenId(openid);
        // 判断是否第一次登录， 保存到用户表
        if (null == sysUserVo) {
        	sysUserVo = new SysUserVo();
            
            sysUserVo.setUsername(Base64.encode(loginInfo.getNickName()));
            sysUserVo.setPassword(openid);
            sysUserVo.setCreateTime(nowTime);
            sysUserVo.setWeixinOpenid(openid);
            // 第一次登录
            sysUserVo.setStatus(2);
            sysUserService.save(sysUserVo);
            MlsUserEntity2 fmlsUser= new MlsUserEntity2();
            if(loginInfo.getPromoterId() != 0) {
            	 MlsUserEntity2 mlsuser = this.mlsUserSer.getEntityMapper().findByUserId(new Long(loginInfo.getPromoterId()));
                 fmlsUser.setFid(mlsuser.getMlsUserId());
            }
            fmlsUser.setUserId(sysUserVo.getUserId());
        	fmlsUser.setNickname(sysUserVo.getUsername());
        	fmlsUser.setLoginPassword(sysUserVo.getPassword());
            this.mlsUserSer.insUser(fmlsUser);
        }else {
        	// 判断注册是否已经审核
        	// 待审核
        	if(sysUserVo.getStatus()==2) {
        		return toResponsFail("等待管理员审核");
        	}
        	// 审核不通过
        	if(sysUserVo.getStatus()==3) {
        		return toResponsFail(sysUserVo.getRejectInfo());
        	}
        }

        Map<String, Object> tokenMap = tokenService.createToken(sysUserVo.getUserId());
        String token = MapUtils.getString(tokenMap, "token");

        if (StringUtils.isNullOrEmpty(token)) {
            return toResponsFail("登录失败");
        }
        Map<String, Object> resultObj = new HashMap<String, Object>();
        //resultObj.put("openid", openid);
        resultObj.put("sysUserVo",sysUserVo);
        return toResponsSuccess(resultObj);
    }
    
    
    /**
     * 登录
     */
    @ApiOperation(value = "生产二维码")
    @IgnoreAuth
    @GetMapping("createCode")
    public Object createCode(String scene) {

    	System.out.println(tokenService.getAccessToken());
        
        
        return toResponsSuccess(null);
    }
}
