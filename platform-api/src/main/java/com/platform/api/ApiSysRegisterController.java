package com.platform.api;

import com.platform.annotation.IgnoreAuth;
import com.platform.dao.SysUserDao;
import com.platform.entity.OssEntityVo;
import com.platform.entity.SysUserEntity;
import com.platform.entity.SysUserVo;
import com.platform.oss.OSSFactory;
import com.platform.service.ApiSysUserService;
import com.platform.service.ApiOssService;
import com.platform.service.SysUserRoleService;
import com.platform.util.ApiBaseAction;
import com.platform.utils.Constant;
import com.platform.utils.R;
import com.platform.utils.RRException;
import com.platform.validator.Assert;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.shiro.crypto.hash.Sha256Hash;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

/**
 * 商户注册
 *
 * @author lipengjun
 * @email 939961241@qq.com
 * @date 2017-03-26 17:27
 */
@Api(tags = "商户注册")
@RestController
@RequestMapping("/api/sysUserRegister")
public class ApiSysRegisterController extends ApiBaseAction  {
    @Autowired
    private ApiSysUserService sysUserService;
    @Autowired
    private SysUserRoleService sysUserRoleService;
    @Autowired
    private SysUserDao sysUserDao;
    @Autowired
    private ApiOssService sysOssService;
    /**
     * 微信注册商铺
     */
    @ApiOperation(value = "微信注册商铺")
    @IgnoreAuth
    @PostMapping("sysUserRegister_byWeixin")
    public R register(String username, String weixinOpenid
    		, String mobile,String merchantName ,String shopIntroduction,String wechatCardPic ,String gonghao) {
   	 Assert.isBlank(weixinOpenid, "微信openId不能为空");
     Assert.isBlank(username, "姓名不能为空");
     Assert.isBlank(merchantName, "店铺名称不能为空");
     Assert.isBlank(wechatCardPic, "请上传企业微信名片！");
     Assert.isBlank(gonghao, "工号不能为空");
//        Assert.isBlank(wechatCardPic, "请上传企业微信名片！");
        //用于校验分销账号是否存在
        if(mobile!=null||!"".equals(mobile)) {
        	int count=sysUserDao.mlsUseCount(mobile);
            if(count>0) {
            	return R.error("手机号已被注册");
            }
        }
        //校验微信openId在用户表是否已经存在（后台登陆的唯一性校验，使用微信的openId。分销账号的校验使用手机号）
        Date nowTime = new Date();
        SysUserVo sysUserVo = sysUserService.queryByOpenId(weixinOpenid);
        Map<String, Object> resultObj = new HashMap<String, Object>();
        resultObj.put("sysUserVo",sysUserVo);
        
        // 判断是否第一次登录， 保存到用户表
        if (null == sysUserVo) {
        	// 第一次登录
        	sysUserVo = new SysUserVo();
            sysUserVo.setUsername(username);
//            sysUserVo.setPassword(DigestUtils.sha256Hex(password));
            //sha256加密
            sysUserVo.setPassword(new Sha256Hash(Constant.DEFAULT_PASS_WORD).toHex());
            sysUserVo.setMobile(mobile);
//            sysUserVo.setEmail(email);
            sysUserVo.setMerchantIntroduction(shopIntroduction);
            sysUserVo.setWeixinOpenid(weixinOpenid);
            sysUserVo.setMerchantName(merchantName);
            sysUserVo.setOrigin("微商城申请");
            sysUserVo.setCreateUserId(getUserId());
            sysUserVo.setCreateTime(nowTime);
            sysUserVo.setStatus(2);
            sysUserVo.setDeptId((long)8);
            sysUserVo.setWechatCardPic(wechatCardPic);
            sysUserVo.setGonghao(gonghao);
            sysUserService.save(sysUserVo);
            SysUserEntity  sysUserEntity= new SysUserEntity();
            sysUserEntity.setUserId(sysUserVo.getUserId());
            sysUserDao.updateMerchantId(sysUserEntity);
            // 保存用户与角色关系，默认为商户角色，值为6
            List<Long> roleIdList = new ArrayList<Long>();
            roleIdList.add((long)6);
            sysUserRoleService.saveOrUpdate(sysUserVo.getUserId(), roleIdList);
        }else {
        	// 判断注册是否已经审核
        	// 待审核
        	if(sysUserVo.getStatus()==2) {
        		return R.ok("等待管理员审核");
        	}
        	if(sysUserVo.getStatus()==3) {
        		return R.ok("已退回修改");
        	}
        	// 审核不通过
        	if(sysUserVo.getStatus()==4) {
        		if(sysUserVo.getRejectInfo()!=null||!"".equals(sysUserVo.getRejectInfo())) {
        			return R.ok("审核不通过！审核意见："+sysUserVo.getRejectInfo()+"如有需要，请联系管理员！");
        		}else {
        			return R.ok("审核不通过！未填写审核意见，请联系管理员！");
        		}
        		
        	}
        }
        return R.ok("注册成功，请等待审核！");
    }
    /**
     * 微信注册商铺---修改
     */
    @ApiOperation(value = "通过用户id，修改注册申请")
    @IgnoreAuth
    @PostMapping("ShopkeeperUpdate")
    public R ShopkeeperUpdate(String userId,String username, String weixinOpenid
    		, String mobile,String merchantName ,String shopIntroduction,String wechatCardPic,String gonghao) {
    	 Assert.isBlank(userId, "userId不能为空");
    	 Assert.isBlank(weixinOpenid, "微信openId不能为空");
         Assert.isBlank(username, "姓名不能为空");
         Assert.isBlank(merchantName, "店铺名称不能为空");
         Assert.isBlank(wechatCardPic, "请上传企业微信名片！");
         Assert.isBlank(gonghao, "工号不能为空");
         SysUserVo  sysUserVo = new SysUserVo();
         sysUserVo.setUserId(Long.parseLong(userId));
         sysUserVo.setUsername(username);
//         sysUserVo.setPassword(new Sha256Hash(Constant.DEFAULT_PASS_WORD).toHex());
         sysUserVo.setMobile(mobile);
         sysUserVo.setWeixinOpenid(weixinOpenid);
         sysUserVo.setMerchantName(merchantName);
//         sysUserVo.setOrigin("微商城申请");
//         sysUserVo.setCreateUserId(getUserId());
         sysUserVo.setGonghao(gonghao);
         sysUserVo.setStatus(1);
         sysUserVo.setDeptId((long)8);
         sysUserVo.setWechatCardPic(wechatCardPic);
         sysUserVo.setMerchantIntroduction(shopIntroduction);
         sysUserService.update(sysUserVo);
    	return R.ok("修改成功！");
       
    }
    /**
     * 上传文件
     *
     * @param file 文件
     * @return R
     * @throws Exception 异常
     */
    @IgnoreAuth
    @ApiOperation(value = "上传图片")
    @PostMapping("uploadPic")
    public R upload(HttpServletRequest request,@RequestParam("file") MultipartFile file) throws Exception {
        if (file.isEmpty()) {
            throw new RRException("上传文件不能为空");
        }
        //上传文件
        String url = OSSFactory.build().upload(request,file);

        //保存文件信息
        OssEntityVo ossEntity = new OssEntityVo();
        ossEntity.setUrl(url);
        ossEntity.setCreateDate(new Date());
        sysOssService.save(ossEntity);
        return 	R.ok(url);
    }

    
    
    /**
     * 校验当前登陆人是否是店主
     */
    @ApiOperation(value = "当前登陆人是否是店主")
    @IgnoreAuth
    @PostMapping("isShopkeeper")
    public R isShopkeeper(String weixinOpenid) {
        Assert.isBlank(weixinOpenid, "Openid不能为空");
        SysUserVo sysUserVo = sysUserService.queryByOpenId(weixinOpenid);
        Map<String, Object> resultObj = new HashMap<String, Object>();
        if(sysUserVo==null) {
        	resultObj.put("isShopkeeper","-1");
        	resultObj.put("msg","当前用户不是店主");
        }else {
        	if(sysUserVo.getStatus()==0) {
        		resultObj.put("isShopkeeper","0");
        		resultObj.put("msg","当前用户已被禁用");
                resultObj.put("sysUserVo",sysUserVo);
        	}else if(sysUserVo.getStatus()==1) {
        		resultObj.put("isShopkeeper","1");
        		resultObj.put("msg","当前用户是店主");
                resultObj.put("sysUserVo",sysUserVo);
        	}else if(sysUserVo.getStatus()==2) {
            	resultObj.put("isShopkeeper","2");
            	resultObj.put("msg","等待管理员审核");
                resultObj.put("sysUserVo",null);
        	}else if(sysUserVo.getStatus()==3) {
        		resultObj.put("isShopkeeper","3");
            	resultObj.put("msg","当前用户申请已被退回修改！");
                resultObj.put("sysUserVo",sysUserVo);
        	}else if(sysUserVo.getStatus()==4) {
        		resultObj.put("isShopkeeper","4");
            	resultObj.put("msg","当前用户申请开店被拒绝！");
                resultObj.put("sysUserVo",sysUserVo);
        	}else {
        		resultObj.put("isShopkeeper","-1");
            	resultObj.put("msg","当前用户不是店主");
        	}
        }
       
    	 return R.ok(resultObj);
       
    }

    /**
     * 通过店铺id，获取店铺二维码名片
     */
    @ApiOperation(value = "通过店铺id，获取店铺二维码名片")
    @IgnoreAuth
    @PostMapping("ShopkeeperWechatCardPic")
    public R ShopkeeperWechatCardPic(String merchantId) {
        Assert.isBlank(merchantId, "店铺id不能为空");
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("merchantId", merchantId);
        SysUserVo sysUserVo = sysUserService.queryObjectByparam(map);
        Map<String, Object> resultObj = new HashMap<String, Object>();
         resultObj.put("sysUserVo",sysUserVo);
    	 return R.ok(resultObj);
       
    }
}
