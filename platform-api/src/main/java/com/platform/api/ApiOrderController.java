package com.platform.api;

import com.platform.annotation.IgnoreAuth;
import com.platform.annotation.LoginUser;
import com.platform.entity.CommentVo;
import com.platform.entity.OrderGoodsVo;
import com.platform.entity.OrderVo;
import com.platform.entity.ShippingVo;
import com.platform.entity.UserCouponVo;
import com.platform.entity.UserVo;
import com.platform.service.*;
import com.platform.util.ApiBaseAction;
import com.platform.util.ApiPageUtils;
import com.platform.util.wechat.APIWSDLUtilsForShop;
import com.platform.util.wechat.WechatRefundApiResult;
import com.platform.util.wechat.WechatUtil;
import com.platform.utils.Base64;
import com.platform.utils.Query;
import com.platform.utils.R;
import com.platform.utils.RRException;
import com.platform.utils.ResourceUtil;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 作者: @author Harmon <br>
 * 时间: 2017-08-11 08:32<br>
 * 描述: ApiIndexController <br>
 */
@Api(tags = "订单相关")
@RestController
@RequestMapping("/api/order")
public class ApiOrderController extends ApiBaseAction {
    @Autowired
    private ApiOrderService orderService;
    @Autowired
    private ApiOrderGoodsService orderGoodsService;
    @Autowired
    private ApiUserCouponService userCouponService;
	@Autowired
	private ApiProductService productService;
	@Autowired
	private ApiCommentService commentService;
	@Autowired
	private ApiShippingService apiShippingService;
	@Autowired
	private ApiUserService userService;
    /**
     */
    @ApiOperation(value = "订单首页")
    @IgnoreAuth
    @GetMapping("index")
    public Object index() {
        //
        return toResponsSuccess("");
    }

    /**
     * 获取订单列表
     */
    @ApiOperation(value = "获取订单列表")
    @RequestMapping("list")
    public Object list(@LoginUser UserVo loginUser, Integer order_status,
                       @RequestParam(value = "page", defaultValue = "1") Integer page,
                       @RequestParam(value = "size", defaultValue = "10") Integer size 
    				) {
        //
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("user_id", loginUser.getUserId());
        params.put("page", page);
        params.put("limit", size);
        params.put("sidx", "id");
        params.put("order", "desc");
        params.put("order_status", order_status);
        //查询列表数据
        Query query = new Query(params);
        List<OrderVo> orderEntityList = orderService.queryListForOnlyOrder(query);
        int total = orderService.queryTotalForOnlyOrder(query);
        
        
        ApiPageUtils pageUtil = new ApiPageUtils(orderEntityList, total, query.getLimit(), query.getPage());
        //
        for (OrderVo item : orderEntityList) {
            Map<String, Object> orderGoodsParam = new HashMap<String, Object>();
            orderGoodsParam.put("order_id", item.getId());
            //订单的商品
            List<OrderGoodsVo> goodsList = orderGoodsService.queryList(orderGoodsParam);
            Integer goodsCount = 0;
            for (OrderGoodsVo orderGoodsEntity : goodsList) {
            	orderGoodsEntity.setIsAlreadyComment("false");
                goodsCount += orderGoodsEntity.getNumber();
                item.setGoodsCount(goodsCount);
                //查询是否已经评价。
                if(item.getOrder_status() ==301 ) {
                	Map<String, Object> map = new HashMap<String, Object>();
//               	   	map.put("value_id", orderGoodsEntity.getProduct_id());
               	   	map.put("ordergoodsId", orderGoodsEntity.getId());
               		List<CommentVo> commentList = commentService.queryList(map);
               		if(commentList.size()>0) {
               			orderGoodsEntity.setIsAlreadyComment("true");
               		}
                }
            }
            item.setGoodsVoList(goodsList);
        }
        return toResponsSuccess(pageUtil);
    }
    /**
     	* 获取当前店铺订单列表
     */
    @ApiOperation(value = "获取当前店铺订单列表")
    @RequestMapping("MyMerchantlist")
    public Object MyMerchant(Integer order_status, String merchantId,
                       @RequestParam(value = "page", defaultValue = "1") Integer page,
                       @RequestParam(value = "size", defaultValue = "10") Integer size 
    				) {
        Map<String, Object> params = new HashMap<String, Object>();
        //判断是否有店铺id
    	if(merchantId!=null&&!"".equals(merchantId)) {
    		params.put("merchant_id", merchantId);	
    	}else {
    		toResponsFail("未能获取当前店铺id，无法请求数据");
    	}
//        params.put("user_id", loginUser.getUserId());
        params.put("page", page);
        params.put("limit", size);
        params.put("sidx", "id");
        params.put("order", "desc");
        params.put("order_status", order_status);
        //查询列表数据
        Query query = new Query(params);
        List<OrderVo> orderEntityList = orderService.queryOrderForMyMerchantlist(query);
        int total = orderService.queryTotalForOnlyOrder(query);
        
        
        ApiPageUtils pageUtil = new ApiPageUtils(orderEntityList, total, query.getLimit(), query.getPage());
        //
        for (OrderVo item : orderEntityList) {
        	item.setUserName(Base64.decode(item.getUserName()));
            Map<String, Object> orderGoodsParam = new HashMap<String, Object>();
            orderGoodsParam.put("order_id", item.getId());
            //订单的商品
            List<OrderGoodsVo> goodsList = orderGoodsService.queryList(orderGoodsParam);
            Integer goodsCount = 0;
            for (OrderGoodsVo orderGoodsEntity : goodsList) {
            	orderGoodsEntity.setIsAlreadyComment("false");
                goodsCount += orderGoodsEntity.getNumber();
                item.setGoodsCount(goodsCount);
                //查询是否已经评价。
                if(item.getOrder_status() ==301 ) {
                	Map<String, Object> map = new HashMap<String, Object>();
//               	   	map.put("value_id", orderGoodsEntity.getProduct_id());
               	   	map.put("ordergoodsId", orderGoodsEntity.getId());
               		List<CommentVo> commentList = commentService.queryList(map);
               		if(commentList.size()>0) {
               			orderGoodsEntity.setIsAlreadyComment("true");
               		}
                }
            }
            item.setGoodsVoList(goodsList);
        }
        return toResponsSuccess(pageUtil);
    }

    /**
     * 获取订单详情
     */
    @ApiOperation(value = "获取订单详情")
    @GetMapping("detail")
    public Object detail(Integer orderId) {
        Map<String, Object> resultObj = new HashMap<String, Object>();
        //
        OrderVo orderInfo = orderService.queryObject(orderId);
        if (null == orderInfo) {
            return toResponsObject(400, "订单不存在", "");
        }
        Map<String, Object> orderGoodsParam = new HashMap<String, Object>();
        orderGoodsParam.put("order_id", orderId);
        //订单的商品
        List<OrderGoodsVo> orderGoods = orderGoodsService.queryList(orderGoodsParam);
        //订单最后支付时间
        if (orderInfo.getOrder_status() == 0) {
            // if (moment().subtract(60, 'minutes') < moment(orderInfo.add_time)) {
//            orderInfo.final_pay_time = moment("001234", "Hmmss").format("mm:ss")
            // } else {
            //     //超过时间不支付，更新订单状态为取消
            // }
        }

        //订单可操作的选择,删除，支付，收货，评论，退换货
        Map<String, Object> handleOption = orderInfo.getHandleOption();
        //
        resultObj.put("orderInfo", orderInfo);
        resultObj.put("orderGoods", orderGoods);
        resultObj.put("handleOption", handleOption);
        if (!StringUtils.isEmpty(orderInfo.getShipping_code()) && !StringUtils.isEmpty(orderInfo.getShipping_no())) {
            resultObj.put("shippingList", null);
        }
        return toResponsSuccess(resultObj);
    }

    @ApiOperation(value = "修改订单")
    @PostMapping("updateSuccess")
    public Object updateSuccess(Integer orderId) {
        OrderVo orderInfo = orderService.queryObject(orderId);
        if (orderInfo == null) {
            return toResponsFail("订单不存在");
        } else if (orderInfo.getOrder_status() != 0) {
            return toResponsFail("订单状态不正确orderStatus" + orderInfo.getOrder_status() + "payStatus" + orderInfo.getPay_status());
        }

        orderInfo.setId(orderId);
        orderInfo.setPay_status(2);
        orderInfo.setOrder_status(201);
        orderInfo.setShipping_status(0);
        orderInfo.setPay_time(new Date());
        int num = orderService.update(orderInfo);
        if (num > 0) {
            return toResponsMsgSuccess("修改订单成功");
        } else {
            return toResponsFail("修改订单失败");
        }
    }

    /**
               * 订单提交
     */
    @ApiOperation(value = "订单提交")
    @PostMapping("submit")
    public Object submit(@LoginUser UserVo loginUser) {
    	String personalGoodsMyMerchantId =  request.getHeader("personalGoods_myMerchantId");
        Map<String, Object> resultObj = null;
        try {
            resultObj = orderService.submit(getJsonRequest(), loginUser, personalGoodsMyMerchantId);
            if (null != resultObj) {
                return toResponsObject(MapUtils.getInteger(resultObj, "errno"), MapUtils.getString(resultObj, "errmsg"), resultObj.get("data"));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return toResponsFail("提交失败");
    }

    /**
     	* 取消订单
     */
    @ApiOperation(value = "取消订单")
    @RequestMapping("cancelOrder")
    public Object cancelOrder(Integer orderId) {
        try {
            OrderVo orderVo = orderService.queryObject(orderId);
            
            List<OrderVo> orders = orderService.queryByAllOrderId(orderVo.getAll_order_id());
            
            BigDecimal allPrice = BigDecimal.ZERO;
            for(OrderVo o : orders) {
            	allPrice = allPrice.add(o.getAll_price());
            }
            
            if (orderVo.getOrder_status() == 300) {
                return toResponsFail("已发货，不能取消");
            } else if (orderVo.getOrder_status() == 301) {
                return toResponsFail("已收货，不能取消");
            }
            // 需要退款
            if (orderVo.getPay_status() == 2) {
            	
                WechatRefundApiResult result = WechatUtil.wxRefund(orderVo.getAll_order_id().toString(),
                		allPrice.doubleValue(), orderVo.getAll_price().doubleValue());
                //测试修改金额
//                WechatRefundApiResult result = WechatUtil.wxRefund(orderVo.getId().toString(), 0.01d, 0.01d);
                if (result.getResult_code().equals("SUCCESS")) {
                    if (orderVo.getOrder_status() == 201) {
                        orderVo.setOrder_status(401);
					} /*
						*订单已发货；不能进行退款
						 * else if (orderVo.getOrder_status() == 300) { orderVo.setOrder_status(402); }
						 */
                    
                    orderVo.setPay_status(4);
                    orderVo.setRefund_time(new Date());
                    orderService.update(orderVo);
                    
                    //更新优惠券状态和实际
                    UserCouponVo uc = new UserCouponVo();
        			uc.setId(orderVo.getCoupon_id());
        			uc.setCoupon_status(1);
        			uc.setUsed_time(null);
        			userCouponService.updateCouponStatus(uc);
                    
                    //去掉订单成功成立分润退还
                    try {
//                    	orderService.cancelFx(orderVo.getId(), orderVo.getPay_time(), orderVo.getAll_price().multiply(new BigDecimal("100")).intValue());
                    }catch(Exception e) {
                    	System.out.println("================取消订单返还分润开始================");
                    	e.printStackTrace();
                    	System.out.println("================取消订单返还分润开始================");
                    }
                    //还原库存
                    Map<String, Object> orderGoodsParam = new HashMap<String, Object>();
                    orderGoodsParam.put("order_id", orderVo.getId());
                    //订单的商品
                    List<OrderGoodsVo> orderGoodsList = orderGoodsService.queryList(orderGoodsParam);
                    for(OrderGoodsVo orderGoodVo:orderGoodsList) {
                        //还原产品的库存信息
    					productService.updateProductNumber_add(orderGoodVo.getProduct_id(), orderGoodVo.getNumber());
    					//更新商品的库存以及销量
    					productService.updateGoodsNumber_add(orderGoodVo.getGoods_id(), orderGoodVo.getNumber());
    					//校验库存，根据库存修改上下架情况。
    					orderService.CheckStock(orderGoodVo.getGoods_id());
                    }
                    return toResponsSuccess("取消成功");
                } else {
                    return toResponsObject(400, "取消成失败", "取消成失败");
                }
            } else {
                orderVo.setOrder_status(101);
                orderService.update(orderVo);
                //更新优惠券状态和实际
                UserCouponVo uc = new UserCouponVo();
    			uc.setId(orderVo.getCoupon_id());
    			uc.setCoupon_status(1);
    			uc.setUsed_time(null);
    			userCouponService.updateCouponStatus(uc);
                
                //去掉订单成功成立分润退还
                try {
                	orderService.cancelFx(orderVo.getId(), orderVo.getPay_time(), orderVo.getAll_price().multiply(new BigDecimal("100")).intValue());
                }catch(Exception e) {
                	System.out.println("================取消订单返还分润开始================");
                	e.printStackTrace();
                	System.out.println("================取消订单返还分润开始================");
                }
                //还原库存
                Map<String, Object> orderGoodsParam = new HashMap<String, Object>();
                orderGoodsParam.put("order_id", orderVo.getId());
                //订单的商品
                List<OrderGoodsVo> orderGoodsList = orderGoodsService.queryList(orderGoodsParam);
                for(OrderGoodsVo orderGoodVo:orderGoodsList) {
                    //还原产品的库存信息
					productService.updateProductNumber_add(orderGoodVo.getProduct_id(), orderGoodVo.getNumber());
					//更新商品的库存以及销量
					productService.updateGoodsNumber_add(orderGoodVo.getGoods_id(), orderGoodVo.getNumber());
					//校验库存，根据库存修改上下架情况。
					orderService.CheckStock(orderGoodVo.getGoods_id());
                }
                return toResponsSuccess("取消成功");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return toResponsSuccess("提交失败");
    }
    


    /**
     	* 确认收货
     */
    @ApiOperation(value = "确认收货")
    @RequestMapping("confirmOrder")
    public Object confirmOrder(Integer orderId) {
        try {
            OrderVo orderVo = orderService.queryObject(orderId);
            orderVo.setOrder_status(301);
            orderVo.setShipping_status(2);
            orderVo.setConfirm_time(new Date());
            orderService.update(orderVo);
            return toResponsSuccess("确认收货成功");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return toResponsFail("提交失败");
    }
    
    /**
 	* 查询快递公司信息
 */
@ApiOperation(value = "快递公司信息")
@RequestMapping("queryShippingList")
public Object queryShippingList() {
    	Map<String, Object> resultObj = new HashMap<String, Object>();
        List<ShippingVo> shippingVos = apiShippingService.queryAll(null);
        resultObj.put("shippingVos",shippingVos);
        return toResponsSuccess(resultObj);
}
/**
 * 发货
 *
 * @param order
 * @return
 */
@ApiOperation(value = "确认发货")
@RequestMapping("sendGoods")
public Object sendGoods(Integer orderId,Integer shippingId,String shippingName,String shippingNo) {
      OrderVo orderInfo = orderService.queryObject(orderId);
      if (null == orderInfo) {
          return toResponsFail("订单不存在");
      }
      if (2 != orderInfo.getPay_status()) {
    	  return  toResponsFail("此订单未付款！");
      }
      orderInfo.setShippingChannel("1");
      orderInfo.setShipping_id(shippingId);
      orderInfo.setShipping_name(shippingName);
      orderInfo.setShipping_no(shippingNo);
      orderInfo.setAddress(null);
      orderInfo.setOrder_status(300);//订单已发货
      orderInfo.setShipping_status(1);//已发货
      orderService.update(orderInfo);
		//由于接口返回速度较慢，导致小程序接口调用失败，故创建线程去执行。
		  UserVo userVo = userService.queryObject(orderInfo.getUser_id()); Map<String,
		  Object> params =new HashMap<String, Object>();
		  params.put("order_id", orderInfo.getId()); 
		  List<OrderGoodsVo> list= orderGoodsService.queryList(params);
		  if(list.size()<20) {//对查下的list条数做限制。防止意外情况，导致查询出所有的商品
			  String type ="Shipped";
			  if(ResourceUtil.getConfigByName("sys.demo").equals("1")){ 
				  //演示环境不需要注册账号 
			  }else{ 
				APIWSDLUtilsForShop.OrderSynchronization(orderInfo,list,userVo,type); 
			  }
		  }
		 
    return toResponsSuccess("发货成功！");
}
/**
 * 修改物流信息
 *
 * @param order
 * @return
 */
@ApiOperation(value = "修改物流信息")
@RequestMapping("editShippingMsg")
public Object editShippingMsg(Integer orderId,Integer shippingId,String shippingName,String shippingNo) {
      OrderVo orderInfo = orderService.queryObject(orderId);
      if (null == orderInfo) {
          return toResponsFail("订单不存在");
      }
      if (2 != orderInfo.getPay_status()) {
    	  return  toResponsFail("此订单未付款！");
      }
      orderInfo.setShippingChannel("1");
      orderInfo.setShipping_id(shippingId);
      orderInfo.setShipping_name(shippingName);
      orderInfo.setShipping_no(shippingNo);
      orderInfo.setAddress(null);
      orderInfo.setOrder_status(300);//订单已发货
      orderInfo.setShipping_status(1);//已发货
      orderService.update(orderInfo);
   
    return toResponsSuccess("修改发货信息--成功");
}
}