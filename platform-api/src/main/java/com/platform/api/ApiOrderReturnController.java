package com.platform.api;


import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.platform.entity.OrderGoodsVo;
import com.platform.entity.OrderReturnVo;
import com.platform.entity.OrderVo;
import com.platform.entity.ShippingVo;
import com.platform.service.ApiOrderGoodsService;
import com.platform.service.ApiOrderReturnService;
import com.platform.service.ApiOrderService;
import com.platform.utils.R;
import com.platform.validator.Assert;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * Controller
 *
 * @email 939961241@qq.com
 * @date 2019-07-31 10:44:54
 */
@Api(tags = "申请退款相关")
@RestController
@RequestMapping("/api/orderReturn")
public class ApiOrderReturnController {
    @Autowired
    private ApiOrderReturnService orderReturnService;
    @Autowired
    private ApiOrderService orderService;
    @Autowired
    private ApiOrderGoodsService apiOrderGoodsService;
    /**
     * 申请退款
     */
    @ApiOperation(value = "提交退货申请")
    @PostMapping("Refund")
    public R Refund(String orderId,String orderGoodsId,String orderProductId,String number, String returnReason
    		, String userName,String mobileNumber ,String returnType) {
    	 Assert.isBlank(returnType, "退货退款类型不能为空");
    	 Assert.isBlank(orderId, "订单id不能为空");
         Assert.isBlank(returnReason, "退货原因不能为空");
         Assert.isBlank(userName, "联系人不能为空");
         Assert.isBlank(mobileNumber, "联系方式不能为空");
         if(orderGoodsId!=null&&!"".equals(orderGoodsId)) {
        	 Assert.isBlank(number, "退货商品数量不能为空");
         }
         OrderVo orderVo = orderService.queryObject(Integer.parseInt(orderId));
         Integer status = orderVo.getOrder_status();
         if(status!=301&&status!=403) {//301：订单已确认收货。403：此订单为部分退货
        	 return R.ok("当前订单未确认收货，无法提交退货申请！");
         }
         OrderReturnVo  orderReturn = new OrderReturnVo();
         orderReturn.setOrderId(Integer.parseInt(orderId));
         if(orderGoodsId!=null&&!"".equals(orderGoodsId)) {
             orderReturn.setOrderGoodsId(Integer.parseInt(orderGoodsId));
         }
         if(orderProductId!=null&&!"".equals(orderProductId)) {
        	 orderReturn.setOrderProductId(Integer.parseInt(orderProductId));
         }
         if(number!=null&&!"".equals(number)) {
        	 orderReturn.setNumber(Integer.parseInt(number));
         }
         orderReturn.setReturnReason(returnReason);
         orderReturn.setUserName(userName);
         orderReturn.setMobileNumber(mobileNumber);
         orderReturn.setStatus(1);
         orderReturn.setReturnType(returnType);
         SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
         orderReturn.setCreatTime(sdf.format(new Date()));
         orderReturnService.save(orderReturn);
         //更新订单状态
         orderVo.setOrder_status(501);//退货:申请待审核
         orderService.update(orderVo);
         if(orderGoodsId!=null&&!"".equals(orderGoodsId)) {
        	 OrderGoodsVo orderGoodsVo= apiOrderGoodsService.queryObject(Integer.parseInt(orderGoodsId));
        	 orderGoodsVo.setIsReturn("1");
        	 apiOrderGoodsService.update(orderGoodsVo);
         }
        
    	return R.ok("已提交退货申请，请等待审核！");
       
    }
    /**
     * 校验当前商品是否已提交过退货申请
     */
    @ApiOperation(value = "校验当前商品是否已提交过退货申请")
    @PostMapping("queryRefund")
    public R queryRefund(String orderId,String orderGoodsId) {
    	 Assert.isBlank(orderId, "订单id不能为空");
    	 Assert.isBlank(orderGoodsId, "订单的商品id不能为空");
    	 Map<String, Object> resultObj = new HashMap<String, Object>();
         OrderReturnVo  orderReturn = new OrderReturnVo();
         orderReturn.setOrderId(Integer.parseInt(orderId));
         orderReturn.setOrderGoodsId(Integer.parseInt(orderGoodsId));
         OrderReturnVo orderReturnVo  =  orderReturnService.queryObjectByVo(orderReturn);
         if(orderReturnVo==null) {
         	resultObj.put("isRefund","-1");
         	resultObj.put("msg","未提交退货申请");
         }else{
        	 if(orderReturnVo.getStatus()==1) {
        		 resultObj.put("isRefund","1");
         		 resultObj.put("msg","已经提交退货申请");
                 resultObj.put("orderReturnVo",orderReturnVo);
        	 }else if(orderReturnVo.getStatus()==2){
        		 resultObj.put("isRefund","2");
         		 resultObj.put("msg","同意申请，待买家发货");
                 resultObj.put("orderReturnVo",orderReturnVo);
        	 }else if(orderReturnVo.getStatus()==3){
        		 resultObj.put("isRefund","3");
         		 resultObj.put("msg","待卖家收货");
                 resultObj.put("orderReturnVo",orderReturnVo);
        	 }else if(orderReturnVo.getStatus()==4){
        		 resultObj.put("isRefund","4");
         		 resultObj.put("msg","待卖家退款");
                 resultObj.put("orderReturnVo",orderReturnVo);
        	 }else if(orderReturnVo.getStatus()==5){
        		 resultObj.put("isRefund","5");
         		 resultObj.put("msg","已完成");
                 resultObj.put("orderReturnVo",orderReturnVo);
        	 }
        	 
         }
    	return R.ok(resultObj);
    }
    
    /**
     * 修改退换货申请--提交发货信息
     */
    @ApiOperation(value = "修改注册申请")
    @PostMapping("update")
    public R update(String orderId, String shippingId,String shippingName,String shippingNo) {
    	 Assert.isBlank(orderId, "订单id不能为空");
    	 Assert.isBlank(shippingId, "快递id不能为空");
         Assert.isBlank(shippingName, "快递名称不能为空");
         Assert.isBlank(shippingNo, "快递编号不能为空");
         OrderReturnVo  orderReturn = new OrderReturnVo();
         orderReturn.setOrderId(Integer.parseInt(orderId));
         orderReturn.setShippingId(Integer.parseInt(shippingId));
         orderReturn.setShippingName(shippingName);
         orderReturn.setShippingNo(shippingNo);
         orderReturn.setStatus(3);
         orderReturnService.update(orderReturn);
         OrderVo orderVo = orderService.queryObject(Integer.parseInt(orderId));
//         //更新订单状态
         orderVo.setOrder_status(503);
         orderService.update(orderVo);
    	return R.ok("已提交退货申请，请等待审核！");
       
    }
    /**
     * 查询快递公司信息
     */
    @ApiOperation(value = "查询快递公司信息")
    @PostMapping("queryShipping")
    public R queryShipping() {
    	List<ShippingVo> list =orderReturnService.queryShipping();
   	 	Map<String, Object> resultObj = new HashMap<String, Object>();
   	 	resultObj.put("ShippingVoList", list);
    	return R.ok(resultObj);
       
    }
    /**
     	* 同意退货申请
     */
    @ApiOperation(value = "同意退货申请")
    @PostMapping("agreeRefund")
    public R agreeRefund(String orderId) {
   	 	Assert.isBlank(orderId, "订单id不能为空");
   	  OrderReturnVo  orderReturn = new OrderReturnVo();
   	  orderReturn.setOrderId(Integer.parseInt(orderId));
      orderReturn.setStatus(2);
      orderReturnService.update(orderReturn);
      OrderVo orderVo = orderService.queryObject(Integer.parseInt(orderId));
      //更新订单状态
      orderVo.setOrder_status(502);
      orderService.update(orderVo);
    	return R.ok("同意退货申请，待卖家发货！");
       
    }
    
    /**
 	* 拒绝退货申请
 */
@ApiOperation(value = "拒绝退货申请")
@PostMapping("rejuectRefund")
public R rejuectRefund(String orderId) {
	 	Assert.isBlank(orderId, "订单id不能为空");
	  OrderReturnVo  orderReturn = new OrderReturnVo();
	  orderReturn.setOrderId(Integer.parseInt(orderId));
  orderReturn.setStatus(0);
  orderReturnService.update(orderReturn);
  OrderVo orderVo = orderService.queryObject(Integer.parseInt(orderId));
//  //更新订单状态
  orderVo.setOrder_status(403);
  orderService.update(orderVo);
	return R.ok("已拒绝退货申请！");
   
}
/**
	* 撤回退货申请
*/
@ApiOperation(value = "撤回退货申请")
@PostMapping("recallRefund")
public R recallRefund(String orderId) {
 	Assert.isBlank(orderId, "订单id不能为空");
  OrderReturnVo  orderReturn = new OrderReturnVo();
  orderReturn.setOrderId(Integer.parseInt(orderId));
orderReturn.setStatus(-1);
orderReturnService.update(orderReturn);
OrderVo orderVo = orderService.queryObject(Integer.parseInt(orderId));
////更新订单状态
orderVo.setOrder_status(301);
orderService.update(orderVo);
return R.ok("已拒撤回退货申请！");

} 
}
