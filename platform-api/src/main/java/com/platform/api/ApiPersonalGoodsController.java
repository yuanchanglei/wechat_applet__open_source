package com.platform.api;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.platform.annotation.IgnoreAuth;
import com.platform.entity.GoodsVo;
import com.platform.entity.PersonalGoodsVo;
import com.platform.service.ApiPersonalGoodsService;
import com.platform.utils.PageUtils;
import com.platform.utils.Query;
import com.platform.utils.R;
import com.platform.utils.ShiroUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * Controller
 *
 * @email 939961241@qq.com
 * @date 2019-07-31 10:44:54
 */
@Api(tags = "个人商铺相关")
@RestController
@RequestMapping("/api/personalGoods")
public class ApiPersonalGoodsController {
    @Autowired
    private ApiPersonalGoodsService personalGoodsService;

    /**
     * 查看列表
     */
    @ApiOperation(value = "查看列表")
    @RequestMapping("list")
    public R list(@RequestParam Map<String, Object> params) {
        //查询列表数据
        Query query = new Query(params);

        List<PersonalGoodsVo> personalGoodsList = personalGoodsService.queryList(query);
        int total = personalGoodsService.queryTotal(query);

        PageUtils pageUtil = new PageUtils(personalGoodsList, total, query.getLimit(), query.getPage());

        return R.ok().put("page", pageUtil);
    }
    
    /**
     * 查看个人商铺商品列表
     */
    @ApiOperation(value = "查看个人商铺商品列表")
    @RequestMapping("queryListOfGoods")
    public R queryListOfGoods(@RequestParam Map<String, Object> params) {
        //查询列表数据
    	if(params.get("page")==null) {
    		params.put("page", 1);
    	}
    	if(params.get("limit")==null) {
    		params.put("limit", 10);
    	}
    	if(params.get("sidx")==null) {
    		params.put("sidx", "add_time");
    	}
    	if(params.get("order")==null) {
    		params.put("order", "asc");
    	}
        Query query = new Query(params);
        // 此处merchantId和personalGoods类关联
        if("Y".equals(query.get("isPersonalGoods"))) {
        	query.put("personalGoods_merchantId",query.get("merchantId"));
        	query.remove("merchantId");
        }
        // 商品库列表——未被个人商铺（当前用户）选择的商品
        if("Y".equals(query.get("isPersonalGoodsSelect"))) {
        	query.put("personalGoods_myMerchantId",query.get("merchantId"));
        	query.remove("merchantId");
        	query.put("isOnSale","1");
        }
        query.put("isDelete", 0);
        List<GoodsVo> personalGoodsList = personalGoodsService.queryListOfGoods(query);
        int total = personalGoodsService.queryTotalOfGoods(query);

        PageUtils pageUtil = new PageUtils(personalGoodsList, total, query.getLimit(), query.getPage());

        return R.ok().put("page", pageUtil);
    }
    

    /**
     * 查看信息
     */
    @ApiOperation(value = "获取详情")
    @GetMapping("info")
    public R info(@PathVariable("id") Long id) {
        PersonalGoodsVo personalGoods = personalGoodsService.queryObject(id);

        return R.ok().put("personalGoods", personalGoods);
    }

    /**
     * 保存
     */
    @ApiOperation(value = "保存")
    @PostMapping("save")
    public R save(@RequestBody PersonalGoodsVo personalGoods) {
//    	SysUserEntity sysUserEntity= ShiroUtils.getUserEntity();
//    	personalGoods.setSysUserId(sysUserEntity.getUserId());
//		personalGoods.setMerchantId(sysUserEntity.getMerchantId());
		personalGoods.setAddTime(new Date());
		personalGoodsService.save(personalGoods);
		return R.ok("添加成功");	
    }
    /**
     * 根据goodsIds批量保存
     */
    @ApiOperation(value = "根据goodsIds批量保存")
    @PostMapping("saveBatch")
    public R saveBatch(@RequestParam Long[] goodsIds,@RequestParam Long userId,@RequestParam Long merchantId) {
    	if(goodsIds==null||userId == null ||merchantId ==null) {
    		return R.error("参数错误");
    	}
		personalGoodsService.saveBatch(goodsIds, userId, merchantId);
		return R.ok("添加成功");	
    }
    
    

    /**
     * 修改
     */
    @ApiOperation(value = "修改")
    @PostMapping("update")
    public R update(@RequestBody PersonalGoodsVo personalGoods) {
        personalGoodsService.update(personalGoods);
        return R.ok();
    }

    /**
     * 删除
     */
    @ApiOperation(value = "删除")
    @PostMapping("delete")
    public R delete(@RequestParam Long[] ids,@RequestParam Long userId,@RequestParam Long merchantId) {
    	if(ids==null||userId == null ||merchantId ==null) {
    		return R.error("参数错误");
    	}
    	for(Long goods_id:ids) {
    		Map<String, Object> params = new HashMap<String, Object>();
    		params.put("goods_id", goods_id+"");
    		params.put("userId", userId+"");
    		params.put("merchantId", merchantId+"");
            personalGoodsService.deleteBatch(params);

    	}
    	
        return R.ok("删除成功");
    }
    

    /**
     * 查看所有列表
     */
    @ApiOperation(value = "查看所有列表")
    @PostMapping("queryAll")
    public R queryAll(@RequestParam Map<String, Object> params) {

        List<PersonalGoodsVo> list = personalGoodsService.queryList(params);

        return R.ok().put("list", list);
    }
}
