package com.platform.task;

import com.platform.entity.*;
import com.platform.service.*;
import com.platform.util.Kuai100Util;
import com.platform.util.wechat.WechatRefundApiResult;
import com.platform.util.wechat.WechatUtil;
import com.platform.utils.DateUtils;
import com.platform.utils.ResourceUtil;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import java.util.List;

/**
 * 系统定时任务
 * <p>
 * @author xudong
 * @date 2020年02月15日 下午1:34:24
 */
@Component("PlatformTask")
public class PlatformTask {
    private Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    private ApiOrderService orderService;
    @Autowired
    private ApiOrderGoodsService orderGoodsService;
    @Autowired
    private ApiProductService productService;
    @Autowired
	private ApiUserService userService;
    public void invalidOrderTask() {
        logger.info("PlatformTask_invalidOrderTask======定时取消未付款订单");
        List<OrderGoodsVo> ordergoods = orderGoodsService.queryInvalidOrder();
        for(OrderGoodsVo o : ordergoods) {
        	
        	ProductVo product = productService.queryObject(o.getProduct_id());
        	if(product!=null) {
        		 //还原产品的库存信息
				productService.updateProductNumber_add(product.getId(), o.getNumber());
				//更新商品的库存以及销量
				productService.updateGoodsNumber_add(product.getId(), o.getNumber());
				//校验库存，根据库存修改上下架情况。
				orderService.CheckStock(product.getGoods_id());
        	}
        	
        	OrderVo order = orderService.queryObject(o.getOrder_id());
        	if(order!=null) {
        		order.setOrder_status(103);
            	orderService.update(order);
        	}
        	
        }
    }
    
}
