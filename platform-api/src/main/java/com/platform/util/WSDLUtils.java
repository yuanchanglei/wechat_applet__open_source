package com.platform.util;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.xml.ws.Holder;

import com.platform.entity.UserVo;
import com.platform.utils.Base64;
import com.platform.wsdlClint.E3SynOderInfo.EOrderEntry;
import com.platform.wsdlClint.E3SynOderInfo.LPSpcOrderSpcSynchronizationSpcE3SpcWS;
import com.platform.wsdlClint.E3SynOderInfo.LPSpcOrderSpcSynchronizationSpcE3SpcWS_Service;
import com.platform.wsdlClint.E3SynOderInfo.ListOfEOrderInfo;
import com.platform.wsdlClint.E3SynOderInfo.ListOfEOrderInfoTopElmt;
import com.platform.wsdlClint.LPAPPMemberRegister.LPSpcAPPSpcMemberSpcRegisterSpcBS;
import com.platform.wsdlClint.LPAPPMemberRegister.LPSpcAPPSpcMemberSpcRegisterSpcBS_Service;
import com.platform.wsdlClint.LPAPPQueryMemberInfo.AppMemberInfo;
import com.platform.wsdlClint.LPAPPQueryMemberInfo.LPSpcAPPSpcMemberSpcQuerySpcBS;
import com.platform.wsdlClint.LPAPPQueryMemberInfo.LPSpcAPPSpcMemberSpcQuerySpcBS_Service;
import com.platform.wsdlClint.LPAPPQueryMemberInfo.ListOfAppMemberInfoTopElmt;
import com.platform.wsdlClint.LPQueryMemTier.LPSpcAPPSpcMemTierSpcQuerySpcBS;
import com.platform.wsdlClint.LPQueryMemTier.LPSpcAPPSpcMemTierSpcQuerySpcBS_Service;
import com.platform.wsdlClint.LPQueryMemTier.ListOfMemberTierInfoTopElmt;
import com.platform.wsdlClint.LPQueryMemTier.MemberInfo;

public class WSDLUtils {

	
	/**
	 * 	获取会员等级
	 * @param phone-手机号码
	 * @return
	 */
	public static MemberInfo getMemTier(String phoneTemp) {
	   //	   String phone = new String("18921425435");
	   String parameter2 = new String("");
	   String parameter3 = new String("");
	   String parameter5 = new String("");
	   String parameter4 = new String("");
	   String parameter1 = new String("");
	   String phone = new String(phoneTemp);
	   Holder<String> code = new Holder<String>("");
	   Holder<String> msg = new Holder<String>("");
	   Holder<ListOfMemberTierInfoTopElmt> outmessage = new Holder<ListOfMemberTierInfoTopElmt>();
	   Holder<String> status = new Holder<String>("");
	   LPSpcAPPSpcMemTierSpcQuerySpcBS_Service bs_Service=new LPSpcAPPSpcMemTierSpcQuerySpcBS_Service();
	   LPSpcAPPSpcMemTierSpcQuerySpcBS bs= bs_Service.getLPSpcAPPSpcMemTierSpcQuerySpcBS();
	   bs.lpQueryMemTier(parameter2, parameter3, parameter5, phone, parameter4, parameter1, code, msg, outmessage, status);
	   List<MemberInfo>  memberInfoList= outmessage.value.listOfMemberTierInfo.getMemberInfo();
	   if(memberInfoList.size()!=0) {
		   return memberInfoList.get(0);
	   }else {
		   return null;
	   }
	  
	}
	/**
	 * 获取蓝豹用户信息
	 * @param phone
	 * @return
	 */
	public static AppMemberInfo getMemberInfo(String phoneTemp) {
		   //	   String phone = new String("18921425435");
		   String parameter2 = new String("");
		   String parameter3 = new String("");
		   String parameter5 = new String("");
		   String parameter4 = new String("");
		   String parameter1 = new String("");
		   String phone = new String(phoneTemp);
		   Holder<String> code = new Holder<String>("");
		   Holder<String> msg = new Holder<String>("");
		   Holder<ListOfAppMemberInfoTopElmt> outmessage = new Holder<ListOfAppMemberInfoTopElmt>();
		   Holder<String> status = new Holder<String>("");
		   LPSpcAPPSpcMemberSpcQuerySpcBS_Service bs_Service=new LPSpcAPPSpcMemberSpcQuerySpcBS_Service();
		   LPSpcAPPSpcMemberSpcQuerySpcBS bs= bs_Service.getLPSpcAPPSpcMemberSpcQuerySpcBS();
		   bs.lpappQueryMemberInfo(parameter2, parameter3, parameter5, phone, parameter4, parameter1, code, msg, outmessage, status);
		   if( outmessage.value==null) {
			   return null;
		   }else {
			   AppMemberInfo appMemberInfo= outmessage.value.getListOfAppMemberInfo().getAppMemberInfo();
			   if(appMemberInfo!=null) {
				   MemberInfo  memberInfo = getMemTier(phone);
				   String memberInfoName =memberInfo.getName();
				   String appMemberInfoDiscount ="";
						if("潜在客户".equals(memberInfoName)) {
							appMemberInfoDiscount = "1";
						}else if("普卡会员".equals(memberInfoName)) {
							appMemberInfoDiscount = "1";
						}else if("金卡会员".equals(memberInfoName)) {
							appMemberInfoDiscount = "0.9";
						}else if("白金卡会员".equals(memberInfoName)) {
							appMemberInfoDiscount = "0.8";
						}else if("贵宾会员".equals(memberInfoName)) {
							appMemberInfoDiscount = "1";
						}else {
							appMemberInfoDiscount = "1";
						}
				   appMemberInfo.setMemberInfo(memberInfo);
				   appMemberInfo.setDiscount(appMemberInfoDiscount);
			   }
			   return appMemberInfo;
		   } 
		  
		   
		  
		}
	/**
	 * 蓝豹系统用户注册
	 * @param userVo
	 * @return
	 */
	public static String MemberSpcRegister(UserVo userVo) {
		   //	   String phone = new String("18921425435");
		   Holder<String> parameter1 = new Holder<String>("");
		   Holder<String> parameter2 = new  Holder<String>("");
		   Holder<String> parameter3 = new Holder<String>("");
		   Holder<String> parameter4 = new Holder<String>("");
		   Holder<String> parameter5 = new Holder<String>("");
		   //姓名必填
		   String name = new String(Base64.decode(userVo.getUsername()));
		   String storeNum = new String("L00202");
		   //会员来源必填
		   String channel = new String("JD");
		   //手机号码必填
		   String phone = new String(userVo.getMobile());
		   String assistantId = new String("VJD");
		   SimpleDateFormat f = new SimpleDateFormat("MM/dd/YYYY");
		   Calendar c = Calendar.getInstance();
		   c.add(Calendar.YEAR, -20);
		   String birthday = new String(f.format(c.getTime()));
		   //性别必填
		   String genderTemp ="";
		   if(userVo.getGender()==1||userVo.getGender()==0) {
			   genderTemp ="先生";
		   }else {
			   genderTemp ="女士";
		   }
		   String gender = new String(genderTemp);
		   Holder<String> code = new Holder<String>("");
		   Holder<String> memNum = new Holder<String>("");
		   Holder<String> memPoint = new Holder<String>("");
		   Holder<String> memTier = new Holder<String>("");
		   Holder<String> status = new Holder<String>("");
		   Holder<String> msg = new Holder<String>("");
		   LPSpcAPPSpcMemberSpcRegisterSpcBS_Service bs_Service=new LPSpcAPPSpcMemberSpcRegisterSpcBS_Service();
		   LPSpcAPPSpcMemberSpcRegisterSpcBS bs= bs_Service.getLPSpcAPPSpcMemberSpcRegisterSpcBS();
		   bs.lpappMemberRegister(parameter2, name, storeNum, channel, parameter3, parameter5, phone, parameter4, assistantId, parameter1, birthday, gender, code, memNum, memPoint, memTier, msg, status);
		   return null;
		}
	/**
	 * 蓝豹系统订单同步
	 * @param 
	 * @return
	 */
	public static String OrderSynchronization() {
		   //	   String phone = new String("18921425435");
		/*
		 * Holder<String> parameter1 = new Holder<String>(""); Holder<String> parameter2
		 * = new Holder<String>(""); Holder<String> parameter3 = new Holder<String>("");
		 * Holder<String> parameter4 = new Holder<String>(""); Holder<String> parameter5
		 * = new Holder<String>("");
		 * 
		 * Holder<String> code = new Holder<String>(""); Holder<String> memNum = new
		 * Holder<String>(""); Holder<String> memPoint = new Holder<String>("");
		 * Holder<String> memTier = new Holder<String>(""); Holder<String> status = new
		 * Holder<String>(""); Holder<String> msg = new Holder<String>("");
		 */
		   LPSpcOrderSpcSynchronizationSpcE3SpcWS_Service bs_Service=new LPSpcOrderSpcSynchronizationSpcE3SpcWS_Service();
		   LPSpcOrderSpcSynchronizationSpcE3SpcWS bs= bs_Service.getLPSpcOrderSpcSynchronizationSpcE3SpcWS();
		   ListOfEOrderInfoTopElmt listOfEOrderInfoTopElmt = new ListOfEOrderInfoTopElmt();
		   ListOfEOrderInfo  listOfEOrderInfo= new ListOfEOrderInfo();
		   List<EOrderEntry> eOrderEntryList = new ArrayList<EOrderEntry>();
		   EOrderEntry eOrderEntry =new EOrderEntry();
		   eOrderEntryList.add(eOrderEntry);
		   listOfEOrderInfo.setEOrderEntry(eOrderEntryList);
		   listOfEOrderInfoTopElmt.setListOfEOrderInfo(listOfEOrderInfo);
		   bs.e3SynOderInfo(listOfEOrderInfoTopElmt);
		   return null;
		}

}
