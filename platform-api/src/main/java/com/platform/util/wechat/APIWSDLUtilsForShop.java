package com.platform.util.wechat;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.platform.entity.OrderGoodsVo;
import com.platform.entity.OrderVo;
import com.platform.entity.UserVo;
import com.platform.wsdlClint.E3SynOderInfo.EOrderEntry;
import com.platform.wsdlClint.E3SynOderInfo.EOrderLineItemEntry;
import com.platform.wsdlClint.E3SynOderInfo.EOrderPaymentEntry;
import com.platform.wsdlClint.E3SynOderInfo.LPSpcOrderSpcSynchronizationSpcE3SpcWS;
import com.platform.wsdlClint.E3SynOderInfo.LPSpcOrderSpcSynchronizationSpcE3SpcWS_Service;
import com.platform.wsdlClint.E3SynOderInfo.ListOfEOrderInfo;
import com.platform.wsdlClint.E3SynOderInfo.ListOfEOrderInfoTopElmt;
import com.platform.wsdlClint.E3SynOderInfo.ListOfEOrderLineItemEntry;
import com.platform.wsdlClint.E3SynOderInfo.ListOfEOrderOutputInfoTopElmt;
import com.platform.wsdlClint.E3SynOderInfo.ListOfEOrderPaymentEntry;

public class APIWSDLUtilsForShop {

	/**
	 * 蓝豹系统订单同步
	 * @param 
	 * @return
	 */
	public static String OrderSynchronization(OrderVo order,List<OrderGoodsVo> OrderGoodsEntityList,UserVo userEntity,String type) {
		   //	   String phone = new String("18921425435");
		/*
		 * Holder<String> parameter1 = new Holder<String>(""); Holder<String> parameter2
		 * = new Holder<String>(""); Holder<String> parameter3 = new Holder<String>("");
		 * Holder<String> parameter4 = new Holder<String>(""); Holder<String> parameter5
		 * = new Holder<String>("");
		 * 
		 * Holder<String> code = new Holder<String>(""); Holder<String> memNum = new
		 * Holder<String>(""); Holder<String> memPoint = new Holder<String>("");
		 * Holder<String> memTier = new Holder<String>(""); Holder<String> status = new
		 * Holder<String>(""); Holder<String> msg = new Holder<String>("");
		 */   
		   SimpleDateFormat f = new SimpleDateFormat("MM/dd/YYYY HH:mm:ss");
		   LPSpcOrderSpcSynchronizationSpcE3SpcWS_Service bs_Service=new LPSpcOrderSpcSynchronizationSpcE3SpcWS_Service();
		   LPSpcOrderSpcSynchronizationSpcE3SpcWS bs= bs_Service.getLPSpcOrderSpcSynchronizationSpcE3SpcWS();
		   ListOfEOrderInfoTopElmt listOfEOrderInfoTopElmt = new ListOfEOrderInfoTopElmt();
		   ListOfEOrderInfo  listOfEOrderInfo= new ListOfEOrderInfo();
		   List<EOrderEntry> eOrderEntryList = new ArrayList<EOrderEntry>();
		   EOrderEntry eOrderEntry =new EOrderEntry();
		   ListOfEOrderLineItemEntry listOfEOrderLineItemEntry = new ListOfEOrderLineItemEntry();
		   List<EOrderLineItemEntry> eOrderLineItemEntryList =new ArrayList<EOrderLineItemEntry>();
		   for(int i=0;i<OrderGoodsEntityList.size();i++) {
			   EOrderLineItemEntry eOrderLineItemEntry = new EOrderLineItemEntry();
			   //赋值
			   //产品编号
			   eOrderLineItemEntry.setItemProCode(OrderGoodsEntityList.get(i).getGoods_sn());
			   //数量
			   eOrderLineItemEntry.setItemQuantity(OrderGoodsEntityList.get(i).getNumber().toString());
			   //产品吊牌价
			   eOrderLineItemEntry.setItemProPrice(OrderGoodsEntityList.get(i).getMarket_price().toString());
			   //实付金额
			   eOrderLineItemEntry.setItemActAmount(OrderGoodsEntityList.get(i).getRetail_price().multiply(new BigDecimal(OrderGoodsEntityList.get(i).getNumber())).toString());
			   //外系统订单明细流水号
			   eOrderLineItemEntry.setItemOrderNum(order.getOrder_sn());
			   //应付金额
			   eOrderLineItemEntry.setItemPayable(OrderGoodsEntityList.get(i).getRetail_price().multiply(new BigDecimal(OrderGoodsEntityList.get(i).getNumber())).toString());
			   //均摊后金额
			   eOrderLineItemEntry.setEquallyAmount(OrderGoodsEntityList.get(i).getRetail_price().multiply(new BigDecimal(OrderGoodsEntityList.get(i).getNumber())).toString());
			   //折扣
			   eOrderLineItemEntry.setItemDiscount(order.getGoodsDiscount());
			   eOrderLineItemEntryList.add(eOrderLineItemEntry);
		   }
		   listOfEOrderLineItemEntry.setEOrderLineItemEntry(eOrderLineItemEntryList);
		   ListOfEOrderPaymentEntry listOfEOrderPaymentEntry =new ListOfEOrderPaymentEntry(); 
		   List<EOrderPaymentEntry> eOrderPaymentEntryList =new ArrayList<EOrderPaymentEntry>();
		   EOrderPaymentEntry eOrderPaymentEntry = new EOrderPaymentEntry();
		   //支付方式
		   eOrderPaymentEntry.setPayMethod("weixin");
		   //支付时间
		   eOrderPaymentEntry.setPayTime(f.format(order.getPay_time()));
		   //支付金额
		   eOrderPaymentEntry.setPayAmount(order.getActual_price().toString());
		   eOrderPaymentEntryList.add(eOrderPaymentEntry);
		   listOfEOrderPaymentEntry.setEOrderPaymentEntry(eOrderPaymentEntryList);
		   //赋值
		   //会员手机号码
		   if(userEntity.getMobile()!=null) {
			   eOrderEntry.setPhone(userEntity.getMobile());
		   }
		
		   //订单状态-已发货
		   if("Shipped".equals(type)) {
			   eOrderEntry.setStatus("Shipped");
			   //订单编号
			   eOrderEntry.setOrderNum(order.getOrder_sn());
		   }else {
			   eOrderEntry.setStatus("Return");
			   //订单编号
			   eOrderEntry.setOrderNum(order.getOrder_sn()+"_Return");
			   eOrderEntry.setOriginalOrderNum(order.getOrder_sn());
		   }
		 
		   //订单发货时间
		   eOrderEntry.setCompleted(f.format(new Date()));
		   //来源-默认京东
		   eOrderEntry.setOrderSource("JD");
		   //实付金额
		   if("Shipped".equals(type)) {
			   eOrderEntry.setActAmount(order.getOrder_price().toString());
		   }else {
			   eOrderEntry.setActAmount("-"+order.getOrder_price().toString());
		   }
		
		   //应付金额
		   eOrderEntry.setPayable(order.getActual_price().toString());
		   eOrderEntry.setDiscAmount(order.getActual_price().subtract(order.getActual_price()).toString());
		   eOrderEntry.setReceiver(order.getConsignee());
		   eOrderEntry.setReceiverPhone(order.getMobile());
		   eOrderEntry.setReceiverAddress(order.getFull_region());
		   eOrderEntry.setOrganizationNum("L00202");
		   eOrderEntry.setEmployeeNum("VJD");
		   eOrderEntry.setParameter1(f.format(order.getAdd_time()));
		   eOrderEntry.setListOfEOrderLineItemEntry(listOfEOrderLineItemEntry);
		   eOrderEntry.setListOfEOrderPaymentEntry(listOfEOrderPaymentEntry);
		   eOrderEntryList.add(eOrderEntry);
		   listOfEOrderInfo.setEOrderEntry(eOrderEntryList);
		   listOfEOrderInfoTopElmt.setListOfEOrderInfo(listOfEOrderInfo);
		   ListOfEOrderOutputInfoTopElmt out = bs.e3SynOderInfo(listOfEOrderInfoTopElmt);
		   System.out.println( "------------------------------------------");
		   System.out.println( out.getListOfEOrderOutputInfo().getEOrderOutputEntry().get(0).getMsg());
		  
		   return null;
		}

}
