package com.platform.entity;

/**
 * 首次登录对象
 */
public class LoginInfo  {
    private String code;
    private String avatarUrl;
    private int gender;
    private String nickName;
    private String language;
    private String city;
    private String province;
    private String country;
	private int promoterId;
	private Long merchantId;	//商户id
	//手机号码
	private String mobile;
    private String mobile_iv;
    private String mobile_encryptedData;
    private String mobile_Id;//当前手机号码获取的用户id
	public Long getMerchantId() {
		return merchantId;
	}
	public void setMerchantId(Long merchantId) {
		this.merchantId = merchantId;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getAvatarUrl() {
		return avatarUrl;
	}
	public void setAvatarUrl(String avatarUrl) {
		this.avatarUrl = avatarUrl;
	}
	public int getGender() {
		return gender;
	}
	public void setGender(int gender) {
		this.gender = gender;
	}
	public String getNickName() {
		return nickName;
	}
	public void setNickName(String nickName) {
		this.nickName = nickName;
	}
	public String getLanguage() {
		return language;
	}
	public void setLanguage(String language) {
		this.language = language;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getProvince() {
		return province;
	}
	public void setProvince(String province) {
		this.province = province;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}

	public int getPromoterId() {
		return promoterId;
	}

	public void setPromoterId(int promoterId) {
		this.promoterId = promoterId;
	}
	public String getMobile_iv() {
		return mobile_iv;
	}
	public void setMobile_iv(String mobile_iv) {
		this.mobile_iv = mobile_iv;
	}
	public String getMobile_encryptedData() {
		return mobile_encryptedData;
	}
	public void setMobile_encryptedData(String mobile_encryptedData) {
		this.mobile_encryptedData = mobile_encryptedData;
	}
	public String getMobile_Id() {
		return mobile_Id;
	}
	public void setMobile_Id(String mobile_Id) {
		this.mobile_Id = mobile_Id;
	}
	public String getMobile() {
		return mobile;
	}
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	
}
