package com.platform.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotBlank;

import com.platform.validator.group.AddGroup;
import com.platform.validator.group.UpdateGroup;


/**
 * @author lipengjun
 * @email 939961241@qq.com
 * @date 2017-08-15 08:03:41
 */
public class SysUserVo implements Serializable {
    private static final long serialVersionUID = 1L;

    //主键 用户ID
    private Long userId;
    // 用户名
    @NotBlank(message = "用户名不能为空", groups = {AddGroup.class, UpdateGroup.class})
    private String username;
    // 密码
    private transient String password;
    // 邮箱
    @NotBlank(message = "邮箱不能为空", groups = {AddGroup.class, UpdateGroup.class})
    @Email(message = "邮箱格式不正确", groups = {AddGroup.class, UpdateGroup.class})
    private String email;
    // 手机号
    private String mobile;
    
    // 状态  0：禁用   1：正常  2：待审核 3：审核不通过
    private Integer status;
    // 角色ID列表
    private List<Long> roleIdList;
    // 创建者ID
    private Long createUserId;
    // 创建时间
    private Date createTime;
    // 微信Openid
    private String weixinOpenid;
    // 微信名片地址
    private String wechatCardPic;

    // 审核人
    private Long shUserId;
    private String shUserName;
    // 审核时间
    private Date shTime;
    // 审核拒绝信息
    private String rejectInfo;
    
    //商户id（和UserId值相同）
    private Long merchantId;
    //商户名称
    private String merchantName;
    //商户简介
    private String merchantIntroduction;
    
    private String  gonghao;
    //数据来源
    private String origin;
    // 部门
    private Long deptId;
    // 部门名称
    private String deptName;
    
    //一级分佣
    private double fx1;
    //二级分佣
    private double fx2;
    //本人分佣
    private double fx;
    //平台分佣
    private double pfx;
    
	//是否显示其他商户 0:不显示，1:显示
  	private int allShow;

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public List<Long> getRoleIdList() {
		return roleIdList;
	}

	public void setRoleIdList(List<Long> roleIdList) {
		this.roleIdList = roleIdList;
	}

	public Long getCreateUserId() {
		return createUserId;
	}

	public void setCreateUserId(Long createUserId) {
		this.createUserId = createUserId;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public String getWeixinOpenid() {
		return weixinOpenid;
	}

	public void setWeixinOpenid(String weixinOpenid) {
		this.weixinOpenid = weixinOpenid;
	}

	public Long getShUserId() {
		return shUserId;
	}

	public void setShUserId(Long shUserId) {
		this.shUserId = shUserId;
	}

	public String getShUserName() {
		return shUserName;
	}

	public void setShUserName(String shUserName) {
		this.shUserName = shUserName;
	}

	public Date getShTime() {
		return shTime;
	}

	public void setShTime(Date shTime) {
		this.shTime = shTime;
	}

	public String getRejectInfo() {
		return rejectInfo;
	}

	public void setRejectInfo(String rejectInfo) {
		this.rejectInfo = rejectInfo;
	}

	public Long getMerchantId() {
		return merchantId;
	}

	public void setMerchantId(Long merchantId) {
		this.merchantId = merchantId;
	}

	public String getMerchantName() {
		return merchantName;
	}

	public void setMerchantName(String merchantName) {
		this.merchantName = merchantName;
	}

	public String getMerchantIntroduction() {
		return merchantIntroduction;
	}

	public void setMerchantIntroduction(String merchantIntroduction) {
		this.merchantIntroduction = merchantIntroduction;
	}

	public Long getDeptId() {
		return deptId;
	}

	public void setDeptId(Long deptId) {
		this.deptId = deptId;
	}

	public String getDeptName() {
		return deptName;
	}

	public void setDeptName(String deptName) {
		this.deptName = deptName;
	}

	public double getFx1() {
		return fx1;
	}

	public void setFx1(double fx1) {
		this.fx1 = fx1;
	}

	public double getFx2() {
		return fx2;
	}

	public void setFx2(double fx2) {
		this.fx2 = fx2;
	}

	public double getFx() {
		return fx;
	}

	public void setFx(double fx) {
		this.fx = fx;
	}

	public double getPfx() {
		return pfx;
	}

	public void setPfx(double pfx) {
		this.pfx = pfx;
	}

	public int getAllShow() {
		return allShow;
	}

	public void setAllShow(int allShow) {
		this.allShow = allShow;
	}

	public String getOrigin() {
		return origin;
	}

	public void setOrigin(String origin) {
		this.origin = origin;
	}

	public String getWechatCardPic() {
		return wechatCardPic;
	}

	public void setWechatCardPic(String wechatCardPic) {
		this.wechatCardPic = wechatCardPic;
	}

	public String getGonghao() {
		return gonghao;
	}

	public void setGonghao(String gonghao) {
		this.gonghao = gonghao;
	}

}
