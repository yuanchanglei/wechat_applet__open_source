package com.platform.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

/**
 * Created by zhouhaisheng on 2019/3/27.
 */
public class MerCartVo implements Serializable {
    private static final long serialVersionUID = 1L;
    public  Long merchantId;
    public String merchantName;
    public List<CartVo> cartVoList;
    public BigDecimal freightPrice;//运费
    public BigDecimal orderTotalPrice;//订单总金额
    public String memTier;//会员等级
    public String orderDiscount;//订单折扣
    public String orderDiscountText;//订单金额优惠信息
    public BigDecimal orderCampaignDiscount ;//订单参与店铺活动优惠的金额（包含会员折扣价格）
    public String orderCampaignDiscountText ;//订单参与店铺活动优惠的活动信息（包括会员折扣）
    public String orderCampaignDiscountID ;//订单参与店铺活动优惠的活动id（只保存活动的id，使用“,”隔开）
    public BigDecimal actualPrice;//实际支付金额
    public List<CouponVo> userCouponList;//用户可用优惠券列表
    public Long getMerchantId() {
        return merchantId;
    }

    public void setMerchantId(Long merchantId) {
        this.merchantId = merchantId;
    }

    public List<CartVo> getCartVoList() {
        return cartVoList;
    }

    public void setCartVoList(List<CartVo> cartVoList) {
        this.cartVoList = cartVoList;
    }

    public BigDecimal getOrderTotalPrice() {
        return orderTotalPrice;
    }

    public void setOrderTotalPrice(BigDecimal orderTotalPrice) {
        this.orderTotalPrice = orderTotalPrice;
    }

    public String getMemTier() {
		return memTier;
	}

	public void setMemTier(String memTier) {
		this.memTier = memTier;
	}

	public String getOrderDiscount() {
		return orderDiscount;
	}

	public void setOrderDiscount(String orderDiscount) {
		this.orderDiscount = orderDiscount;
	}

	public String getOrderDiscountText() {
		return orderDiscountText;
	}

	public void setOrderDiscountText(String orderDiscountText) {
		this.orderDiscountText = orderDiscountText;
	}

	public BigDecimal getActualPrice() {
        return actualPrice;
    }

    public void setActualPrice(BigDecimal actualPrice) {
        this.actualPrice = actualPrice;
    }

    public BigDecimal getFreightPrice() {
        return freightPrice;
    }

    public void setFreightPrice(BigDecimal freightPrice) {
        this.freightPrice = freightPrice;
    }

    public List<CouponVo> getUserCouponList() {
        return userCouponList;
    }

    public void setUserCouponList(List<CouponVo> userCouponList) {
        this.userCouponList = userCouponList;
    }

    public String getMerchantName() {
        return merchantName;
    }

    public void setMerchantName(String merchantName) {
        this.merchantName = merchantName;
    }

	public BigDecimal getOrderCampaignDiscount() {
		return orderCampaignDiscount;
	}

	public void setOrderCampaignDiscount(BigDecimal orderCampaignDiscount) {
		this.orderCampaignDiscount = orderCampaignDiscount;
	}

	public String getOrderCampaignDiscountText() {
		return orderCampaignDiscountText;
	}

	public void setOrderCampaignDiscountText(String orderCampaignDiscountText) {
		this.orderCampaignDiscountText = orderCampaignDiscountText;
	}

	public String getOrderCampaignDiscountID() {
		return orderCampaignDiscountID;
	}

	public void setOrderCampaignDiscountID(String orderCampaignDiscountID) {
		this.orderCampaignDiscountID = orderCampaignDiscountID;
	}
    
}
