package com.platform.entity;

import java.io.Serializable;
import java.util.Date;

/**
 * 实体
 * 表名 nideshop_personal_goods
 *
 * @author lipengjun
 * @email 939961241@qq.com
 * @date 2019-07-31 10:44:54
 */
public class PersonalGoodsVo implements Serializable {
    private static final long serialVersionUID = 1L;

    //
    private Long id;
    //商品Id
    private Long goodsId;
    //商品名称（VO显示用）
    private String goodsName;
    //系统用户id
    private Long sysUserId;
    //商户id
    private Long merchantId;
    //创建时间
    private Date addTime;

    /**
     * 设置：
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * 获取：
     */
    public Long getId() {
        return id;
    }
    /**
     * 设置：商品Id
     */
    public void setGoodsId(Long goodsId) {
        this.goodsId = goodsId;
    }

    /**
     * 获取：商品Id
     */
    public Long getGoodsId() {
        return goodsId;
    }
    /**
     * 设置：系统用户id
     */
    public void setSysUserId(Long sysUserId) {
        this.sysUserId = sysUserId;
    }

    /**
     * 获取：系统用户id
     */
    public Long getSysUserId() {
        return sysUserId;
    }
    /**
     * 设置：商户id
     */
    public void setMerchantId(Long merchantId) {
        this.merchantId = merchantId;
    }

    /**
     * 获取：商户id
     */
    public Long getMerchantId() {
        return merchantId;
    }
    /**
     * 设置：创建时间
     */
    public void setAddTime(Date addTime) {
        this.addTime = addTime;
    }

    /**
     * 获取：创建时间
     */
    public Date getAddTime() {
        return addTime;
    }

	public String getGoodsName() {
		return goodsName;
	}

	public void setGoodsName(String goodsName) {
		this.goodsName = goodsName;
	}
}
