package com.platform.dao;

import com.platform.entity.SysUserVo;

import java.util.Map;

import org.apache.ibatis.annotations.Param;

/**
 * 用户
 *
 * @author lipengjun
 * @email 939961241@qq.com
 * @date 2017-03-23 15:22:06
 */
public interface ApiSysUserMapper extends BaseDao<SysUserVo> {
    SysUserVo queryByMobile(String mobile);

    SysUserVo queryByOpenId(@Param("openId") String openId);
    SysUserVo queryObjectByparam(Map<String, Object> map);
    
    /**
     * 更新分销比例
     * @param vo
     * @return
     */
    int updatefx(SysUserVo vo);
    
}

