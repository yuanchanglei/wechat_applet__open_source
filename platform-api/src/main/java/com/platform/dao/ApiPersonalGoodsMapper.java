package com.platform.dao;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import com.platform.entity.GoodsVo;
import com.platform.entity.PersonalGoodsVo;

/**
 * Dao
 *
 * @author xuyang
 * @email 295640759@qq.com
 * @date 2019-07-31 10:44:54
 */
public interface ApiPersonalGoodsMapper extends BaseDao<PersonalGoodsVo> {
	
	List<GoodsVo> queryListOfGoods(Map<String, Object> map);

    int queryTotalOfGoods(Map<String, Object> map);
    
	int insertSelective(PersonalGoodsVo record);
	int deleteforBatch(Map<String, Object> map); 
}
