package com.platform.dao;

import java.util.List;

import com.platform.entity.OrderReturnVo;
import com.platform.entity.ShippingVo;

/**
 * Dao
 *
 * @author xuyang
 * @email 295640759@qq.com
 * @date 2019-07-31 10:44:54
 */
public interface ApiOrderReturnMapper extends BaseDao<OrderReturnVo> {
	 /**
     * 根据主键查询实体
     *
     * @param id 主键
     * @return 实体
     */
	OrderReturnVo queryObjectById(Long id);
	 /**
     * 根据条件查询实体
     *
     * @param id 主键
     * @return 实体
     */
	OrderReturnVo queryObjectByVo(OrderReturnVo orderReturn);
	/**
     * 保存实体
     *
     * @param orderReturn 实体
     * @return 保存条数
     */
    int save(OrderReturnVo orderReturn);
    
    //更新
    int updateByPrimaryKeySelective(OrderReturnVo orderReturn);
    
    List<ShippingVo> queryShipping();
}
