package com.platform.service;

import com.platform.dao.ApiSysUserMapper;
import com.platform.entity.SysUserVo;
import com.platform.utils.RRException;
import com.platform.validator.Assert;
import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Map;


@Service
public class ApiSysUserService {
    @Autowired
    private ApiSysUserMapper sysUserDao;

    public SysUserVo queryObject(Long userId) {
        return sysUserDao.queryObject(userId);
    }
    public SysUserVo queryObjectByparam(Map<String, Object> map) {
        return sysUserDao.queryObjectByparam(map);
    }
    public SysUserVo queryByOpenId(String openId) {
        return sysUserDao.queryByOpenId(openId);
    }

    public List<SysUserVo> queryList(Map<String, Object> map) {
        return sysUserDao.queryList(map);
    }

    public int queryTotal(Map<String, Object> map) {
        return sysUserDao.queryTotal(map);
    }

    public void save(String mobile, String password) {
        SysUserVo sysUser = new SysUserVo();
        sysUser.setMobile(mobile);
        sysUser.setUsername(mobile);
        sysUser.setPassword(DigestUtils.sha256Hex(password));
        sysUser.setCreateTime(new Date());
        sysUserDao.save(sysUser);
    }

    public void save(SysUserVo SysUserVo) {
        sysUserDao.save(SysUserVo);
    }

    public void update(SysUserVo user) {
        sysUserDao.update(user);
    }

    public void delete(Long userId) {
        sysUserDao.delete(userId);
    }

    public void deleteBatch(Long[] userIds) {
        sysUserDao.deleteBatch(userIds);
    }

    public SysUserVo queryByMobile(String mobile) {
        return sysUserDao.queryByMobile(mobile);
    }

    public long login(String mobile, String password) {
        SysUserVo user = queryByMobile(mobile);
        Assert.isNull(user, "手机号或密码错误");

        //密码错误
        if (!user.getPassword().equals(DigestUtils.sha256Hex(password))) {
            throw new RRException("手机号或密码错误");
        }

        return user.getUserId();
    }

    public void isRealValidate(SysUserVo SysUserVo){
        SysUserVo userVo1=new SysUserVo();
        userVo1.setUserId(SysUserVo.getUserId());
        userVo1.setMobile(SysUserVo.getMobile());
        sysUserDao.update(userVo1);
    }
    
    public int updatefx(SysUserVo vo) {
    	return sysUserDao.updatefx(vo);
    }
}
