package com.platform.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import com.platform.dao.ApiOrderReturnMapper;
import com.platform.entity.OrderReturnVo;
import com.platform.entity.ShippingVo;

/**
 * Service实现类
 *
 * @author lipengjun
 * @email 939961241@qq.com
 * @date 2019-07-31 10:44:54
 */
@Service
public class ApiOrderReturnService {
    @Autowired
    private ApiOrderReturnMapper orderReturnMapper;

    public OrderReturnVo queryObject(Long id) {
        return orderReturnMapper.queryObjectById(id);
    }
    public OrderReturnVo queryObjectByVo(OrderReturnVo orderReturn) {
        return orderReturnMapper.queryObjectByVo(orderReturn);
    }
    public int save(OrderReturnVo orderReturn) {
        return orderReturnMapper.save(orderReturn);
    }
    public int update(OrderReturnVo orderReturn) {
        return orderReturnMapper.updateByPrimaryKeySelective(orderReturn);
    }
    public List<ShippingVo> queryShipping() {
        return orderReturnMapper.queryShipping();
    }
}
