package com.platform.service;

import com.platform.dao.ApiProductMapper;
import com.platform.entity.ProductVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;


@Service
public class ApiProductService {
    @Autowired
    private ApiProductMapper productDao;


    public ProductVo queryObject(Integer id) {
        return productDao.queryObject(id);
    }


    public List<ProductVo> queryList(Map<String, Object> map) {
        return productDao.queryList(map);
    }


    public int queryTotal(Map<String, Object> map) {
        return productDao.queryTotal(map);
    }


    public void save(ProductVo goods) {
        productDao.save(goods);
    }


    public void update(ProductVo goods) {
        productDao.update(goods);
    }


    public void delete(Integer id) {
        productDao.delete(id);
    }


    public void deleteBatch(Integer[] ids) {
        productDao.deleteBatch(ids);
    }
    //减掉购买的产品库存
    public void updateProductNumber_cut(int productId,Integer cartNumber) {
        productDao.updateProductNumber_cut(productId,cartNumber);
    }
    //还原购买的产品库存
    public void updateProductNumber_add(int productId,Integer cartNumber) {
        productDao.updateProductNumber_add(productId,cartNumber);
    }
    /**
     * 根据参数修改商品库存，和销量_cut 为减库存
     * @param goodsId  商品id
     * @param cartNumber 购物车本次购买的数量
     */
    public void updateGoodsNumber_cut(int goodsId,Integer cartNumber) {
        productDao.updateGoodsNumber_cut(goodsId,cartNumber);
    }
    /**
     * 根据参数修改商品库存，和销量_add 为还原库存
* @param goodsId  商品id
* @param cartNumber 购物车本次购买的数量
*/
public void updateGoodsNumber_add(int goodsId,Integer cartNumber) {
productDao.updateGoodsNumber_add(goodsId,cartNumber);
}

}
