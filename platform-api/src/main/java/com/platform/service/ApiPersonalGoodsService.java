package com.platform.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Date;
import java.util.List;
import java.util.Map;

import com.platform.dao.ApiPersonalGoodsMapper;
import com.platform.entity.GoodsVo;
import com.platform.entity.PersonalGoodsVo;

/**
 * Service实现类
 *
 * @author lipengjun
 * @email 939961241@qq.com
 * @date 2019-07-31 10:44:54
 */
@Service
public class ApiPersonalGoodsService {
    @Autowired
    private ApiPersonalGoodsMapper personalGoodsMapper;

    public List<PersonalGoodsVo> queryList(Map<String, Object> map) {
        return personalGoodsMapper.queryList(map);
    }

    public int queryTotal(Map<String, Object> map) {
        return personalGoodsMapper.queryTotal(map);
    }
    
    public List<GoodsVo> queryListOfGoods(Map<String, Object> map) {
        return personalGoodsMapper.queryListOfGoods(map);
    }

    public int queryTotalOfGoods(Map<String, Object> map) {
        return personalGoodsMapper.queryTotalOfGoods(map);
    }
    
    public PersonalGoodsVo queryObject(Object id) {
    	return personalGoodsMapper.queryObject(id);
    }

    public int save(PersonalGoodsVo personalGoods) {
        return personalGoodsMapper.save(personalGoods);
    }
    
    public int saveBatch(Long[] goodsIds,Long userId,Long merchantId) {
//    	 int result = 0;
         
         int result = 0;
         Date addTime = new Date();
    	 PersonalGoodsVo personalGoods = new PersonalGoodsVo();
    	 personalGoods.setSysUserId(userId);
    	 personalGoods.setMerchantId(merchantId);
    	 personalGoods.setAddTime(addTime);
         for (Long goodsId : goodsIds) {
     		 personalGoods.setGoodsId(goodsId);
             result +=personalGoodsMapper.insertSelective(personalGoods);
         }
         return result;
    }

    public int update(PersonalGoodsVo personalGoods) {
        return personalGoodsMapper.update(personalGoods);
    }

    public int deleteBatch(Map<String, Object> map) {
        return personalGoodsMapper.deleteforBatch(map);
    }
    
}
