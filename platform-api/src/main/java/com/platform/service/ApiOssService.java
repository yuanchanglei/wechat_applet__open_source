package com.platform.service;

import com.platform.dao.ApiOssMapper;
import com.platform.entity.OssEntityVo;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 文件上传Service
 *
 * @author lipengjun
 * @email 939961241@qq.com
 * @date 2017-03-25 12:13:26
 */
@Service
public class ApiOssService {

	@Autowired
	private ApiOssMapper OssDao;
    /**
     * 保存实体
     *
     * @param sysOss 实体
     * @return 保存条数
     */
    public void save(OssEntityVo sysOss){
		OssDao.save(sysOss);
	}
  
}
