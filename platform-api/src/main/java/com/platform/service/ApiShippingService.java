package com.platform.service;


import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.platform.dao.ApiShippingMapper;
import com.platform.entity.ShippingVo;

/**
 * Service接口
 *
 * @author lipengjun
 * @email 939961241@qq.com
 * @date 2017-09-04 21:42:24
 */
@Service
public class ApiShippingService {
	 @Autowired
	 private ApiShippingMapper apiShippingMapper;
	 

    public List<ShippingVo> queryAll(Map<String, Object> map) {
		return apiShippingMapper.queryList(map);
	 };
  
}
