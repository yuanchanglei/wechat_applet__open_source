
package com.platform.wsdlClint.E3SynOderInfo;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>ListOfEOrderInfo complex type�� Java �ࡣ
 * 
 * <p>����ģʽƬ��ָ�������ڴ����е�Ԥ�����ݡ�
 * 
 * <pre>
 * &lt;complexType name="ListOfEOrderInfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="EOrderEntry" type="{http://www.siebel.com/xml/LP%20Order%20Syn%20E3%20IO}EOrderEntry" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ListOfEOrderInfo", propOrder = {
    "eOrderEntry"
})
public class ListOfEOrderInfo {

    @XmlElement(name = "EOrderEntry", required = true)
    protected List<EOrderEntry> eOrderEntry;

    /**
     * Gets the value of the eOrderEntry property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the eOrderEntry property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getEOrderEntry().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link EOrderEntry }
     * 
     * 
     */
    public List<EOrderEntry> getEOrderEntry() {
        if (eOrderEntry == null) {
            eOrderEntry = new ArrayList<EOrderEntry>();
        }
        return this.eOrderEntry;
    }
    public void setEOrderEntry(List<EOrderEntry> eOrderEntry) {
    	this.eOrderEntry = eOrderEntry;
    }
}
