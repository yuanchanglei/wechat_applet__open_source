
package com.platform.wsdlClint.E3SynOderInfo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>ListOfEOrderOutputInfoTopElmt complex type的 Java 类。
 * 
 * <p>以下模式片段指定包含在此类中的预期内容。
 * 
 * <pre>
 * &lt;complexType name="ListOfEOrderOutputInfoTopElmt">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ListOfEOrderOutputInfo" type="{http://www.siebel.com/xml/LP%20Order%20E3%20Outputs%20IO}ListOfEOrderOutputInfo"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ListOfEOrderOutputInfoTopElmt", namespace = "http://www.siebel.com/xml/LP%20Order%20E3%20Outputs%20IO", propOrder = {
    "listOfEOrderOutputInfo"
})
public class ListOfEOrderOutputInfoTopElmt {

    @XmlElement(name = "ListOfEOrderOutputInfo", required = true)
    protected ListOfEOrderOutputInfo listOfEOrderOutputInfo;

    /**
     * 获取listOfEOrderOutputInfo属性的值。
     * 
     * @return
     *     possible object is
     *     {@link ListOfEOrderOutputInfo }
     *     
     */
    public ListOfEOrderOutputInfo getListOfEOrderOutputInfo() {
        return listOfEOrderOutputInfo;
    }

    /**
     * 设置listOfEOrderOutputInfo属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link ListOfEOrderOutputInfo }
     *     
     */
    public void setListOfEOrderOutputInfo(ListOfEOrderOutputInfo value) {
        this.listOfEOrderOutputInfo = value;
    }

}
