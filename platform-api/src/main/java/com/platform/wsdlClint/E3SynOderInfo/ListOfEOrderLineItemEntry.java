
package com.platform.wsdlClint.E3SynOderInfo;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>ListOfEOrderLineItemEntry complex type�� Java �ࡣ
 * 
 * <p>����ģʽƬ��ָ�������ڴ����е�Ԥ�����ݡ�
 * 
 * <pre>
 * &lt;complexType name="ListOfEOrderLineItemEntry">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="EOrderLineItemEntry" type="{http://www.siebel.com/xml/LP%20Order%20Syn%20E3%20IO}EOrderLineItemEntry" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ListOfEOrderLineItemEntry", propOrder = {
    "eOrderLineItemEntry"
})
public class ListOfEOrderLineItemEntry {

    @XmlElement(name = "EOrderLineItemEntry", required = true)
    protected List<EOrderLineItemEntry> eOrderLineItemEntry;

    /**
     * Gets the value of the eOrderLineItemEntry property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the eOrderLineItemEntry property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getEOrderLineItemEntry().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link EOrderLineItemEntry }
     * 
     * 
     */
    public List<EOrderLineItemEntry> getEOrderLineItemEntry() {
        if (eOrderLineItemEntry == null) {
            eOrderLineItemEntry = new ArrayList<EOrderLineItemEntry>();
        }
        return this.eOrderLineItemEntry;
    }

	public void setEOrderLineItemEntry(List<EOrderLineItemEntry> eOrderLineItemEntry) {
		this.eOrderLineItemEntry = eOrderLineItemEntry;
		
	}

}
