package com.platform.wsdlClint;

import javax.xml.ws.Holder;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.platform.entity.SysUserEntity;
import com.platform.service.SysUserService;
import com.platform.util.WSDLUtils;
import com.platform.wsdlClint.LPAPPQueryMemberInfo.AppMemberInfo;
import com.platform.wsdlClint.LPQueryMemTier.LPSpcAPPSpcMemTierSpcQuerySpcBS;
import com.platform.wsdlClint.LPQueryMemTier.LPSpcAPPSpcMemTierSpcQuerySpcBS_Service;
import com.platform.wsdlClint.LPQueryMemTier.ListOfMemberTierInfoTopElmt;
import com.platform.wsdlClint.LPQueryMemTier.MemberInfo;

/**
 * 测试定时任务(演示Demo，可删除)
 * <p>
 * testTask为spring bean的名称
 *
 * @author lipengjun
 * @email 939961241@qq.com
 * @date 2016年11月30日 下午1:34:24
 */
@Component("testTask4")
public class TestTask4 {
    private Logger logger = LoggerFactory.getLogger(getClass());
    private   String parameter2 = new String("");
	private   String parameter3 = new String("");
	private   String parameter5 = new String("");
	private   String phone = new String("18921425435");
	private   String parameter4 = new String("");
	private   String parameter1 = new String("");
	private   Holder<String> code = new Holder<String>("");
	private   Holder<String> msg = new Holder<String>("");
	private   Holder<ListOfMemberTierInfoTopElmt> outmessage = new Holder<ListOfMemberTierInfoTopElmt>();
	private   Holder<String> status = new Holder<String>("");
	public  void test3() {
		// TODO Auto-generated method stub
//		LPSpcAPPSpcMemTierSpcQuerySpcBS_Service bs_Service=new LPSpcAPPSpcMemTierSpcQuerySpcBS_Service();
//		LPSpcAPPSpcMemTierSpcQuerySpcBS bs= bs_Service.getLPSpcAPPSpcMemTierSpcQuerySpcBS();
//		bs.lpQueryMemTier(parameter2, parameter3, parameter5, phone, parameter4, parameter1, code, msg, outmessage, status);
//		System.out.println("-------------------");
//		System.out.println(outmessage.value.listOfMemberTierInfo.getMemberInfo().get(0).getName());
		MemberInfo  m=WSDLUtils.getMemTier(phone);
		AppMemberInfo  member=WSDLUtils.getMemberInfo(phone);
		System.out.println(m.getName());
	}

}
