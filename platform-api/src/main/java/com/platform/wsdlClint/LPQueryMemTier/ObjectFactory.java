
package com.platform.wsdlClint.LPQueryMemTier;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.platform.wsdlClint.LPQueryMemTier package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _ListOfMemberTierInfo_QNAME = new QName("http://www.siebel.com/xml/LP%20Member%20TierInfo%20IO", "ListOfMemberTierInfo");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.platform.wsdlClint.LPQueryMemTier
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link ListOfMemberTierInfo }
     * 
     */
    public ListOfMemberTierInfo createListOfMemberTierInfo() {
        return new ListOfMemberTierInfo();
    }

    /**
     * Create an instance of {@link MemberInfo }
     * 
     */
    public MemberInfo createMemberInfo() {
        return new MemberInfo();
    }

    /**
     * Create an instance of {@link ListOfMemberTierInfoTopElmt }
     * 
     */
    public ListOfMemberTierInfoTopElmt createListOfMemberTierInfoTopElmt() {
        return new ListOfMemberTierInfoTopElmt();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ListOfMemberTierInfo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.siebel.com/xml/LP%20Member%20TierInfo%20IO", name = "ListOfMemberTierInfo")
    public JAXBElement<ListOfMemberTierInfo> createListOfMemberTierInfo(ListOfMemberTierInfo value) {
        return new JAXBElement<ListOfMemberTierInfo>(_ListOfMemberTierInfo_QNAME, ListOfMemberTierInfo.class, null, value);
    }

}
