
package com.platform.wsdlClint.LPQueryMemTier;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>ListOfMemberTierInfoTopElmt complex type�� Java �ࡣ
 * 
 * <p>����ģʽƬ��ָ�������ڴ����е�Ԥ�����ݡ�
 * 
 * <pre>
 * &lt;complexType name="ListOfMemberTierInfoTopElmt">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ListOfMemberTierInfo" type="{http://www.siebel.com/xml/LP%20Member%20TierInfo%20IO}ListOfMemberTierInfo"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ListOfMemberTierInfoTopElmt", propOrder = {
    "listOfMemberTierInfo"
})
public class ListOfMemberTierInfoTopElmt {

    @XmlElement(name = "ListOfMemberTierInfo", required = true)
	public ListOfMemberTierInfo listOfMemberTierInfo;

    /**
     * ��ȡlistOfMemberTierInfo���Ե�ֵ��
     * 
     * @return
     *     possible object is
     *     {@link ListOfMemberTierInfo }
     *     
     */
    public ListOfMemberTierInfo getListOfMemberTierInfo() {
        return listOfMemberTierInfo;
    }

    /**
     * ����listOfMemberTierInfo���Ե�ֵ��
     * 
     * @param value
     *     allowed object is
     *     {@link ListOfMemberTierInfo }
     *     
     */
    public void setListOfMemberTierInfo(ListOfMemberTierInfo value) {
        this.listOfMemberTierInfo = value;
    }

}
