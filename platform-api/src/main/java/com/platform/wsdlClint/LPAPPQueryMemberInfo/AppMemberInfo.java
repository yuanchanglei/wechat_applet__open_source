
package com.platform.wsdlClint.LPAPPQueryMemberInfo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import com.platform.wsdlClint.LPQueryMemTier.MemberInfo;


/**
 * <p>AppMemberInfo complex type的 Java 类。
 * 
 * <p>以下模式片段指定包含在此类中的预期内容。
 * 
 * <pre>
 * &lt;complexType name="AppMemberInfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="name" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="gender" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ListOfMemVoucherInfo" type="{http://www.siebel.com/xml/LP%20APP%20Member%20Info%20IO}ListOfMemVoucherInfo" minOccurs="0"/>
 *         &lt;element name="phone" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="availablePoints" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="birthday" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="neverEmail" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="neverSMS" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="neverCall" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="neverWechat" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="memValClass" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="yearShoppingAmount" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="average" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="lastBuyDate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="judgement" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="dressingStyle" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="totalBuyNum" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="buyReason" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="parameter1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="parameter2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="parameter3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="parameter4" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="parameter5" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="zodiac" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="email" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="job" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="educationalLevel" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="region" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="store" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="guide" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="memberType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="height" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="colorPrefer" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="bodyShape" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="weight" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="waistline" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="outsteam" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="neck" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="hipline" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="bust" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="shoulder" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="belly" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="sleeve" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="skirtlength" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="province" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="city" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="area" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="address" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="description" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="hobby" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="attend" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="joinDate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="parameter6" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="parameter7" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="parameter8" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="parameter9" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AppMemberInfo", propOrder = {
    "name",
    "gender",
    "listOfMemVoucherInfo",
    "phone",
    "availablePoints",
    "birthday",
    "neverEmail",
    "neverSMS",
    "neverCall",
    "neverWechat",
    "memValClass",
    "yearShoppingAmount",
    "average",
    "lastBuyDate",
    "judgement",
    "dressingStyle",
    "totalBuyNum",
    "buyReason",
    "parameter1",
    "parameter2",
    "parameter3",
    "parameter4",
    "parameter5",
    "zodiac",
    "email",
    "job",
    "educationalLevel",
    "region",
    "store",
    "guide",
    "memberType",
    "height",
    "colorPrefer",
    "bodyShape",
    "weight",
    "waistline",
    "outsteam",
    "neck",
    "hipline",
    "bust",
    "shoulder",
    "belly",
    "sleeve",
    "skirtlength",
    "province",
    "city",
    "area",
    "address",
    "description",
    "hobby",
    "attend",
    "joinDate",
    "parameter6",
    "parameter7",
    "parameter8",
    "parameter9",
    "memberInfo",
    "discount"
})
public class AppMemberInfo {

    protected String name;
    protected String gender;
    @XmlElement(name = "ListOfMemVoucherInfo")
    protected ListOfMemVoucherInfo listOfMemVoucherInfo;
    protected String phone;
    protected String availablePoints;
    protected String birthday;
    protected String neverEmail;
    protected String neverSMS;
    protected String neverCall;
    protected String neverWechat;
    protected String memValClass;
    protected String yearShoppingAmount;
    protected String average;
    protected String lastBuyDate;
    protected String judgement;
    protected String dressingStyle;
    protected String totalBuyNum;
    protected String buyReason;
    protected String parameter1;
    protected String parameter2;
    protected String parameter3;
    protected String parameter4;
    protected String parameter5;
    protected String zodiac;
    protected String email;
    protected String job;
    protected String educationalLevel;
    protected String region;
    protected String store;
    protected String guide;
    protected String memberType;
    protected String height;
    protected String colorPrefer;
    protected String bodyShape;
    protected String weight;
    protected String waistline;
    protected String outsteam;
    protected String neck;
    protected String hipline;
    protected String bust;
    protected String shoulder;
    protected String belly;
    protected String sleeve;
    protected String skirtlength;
    protected String province;
    protected String city;
    protected String area;
    protected String address;
    protected String description;
    protected String hobby;
    protected String attend;
    protected String joinDate;
    protected String parameter6;
    protected String parameter7;
    protected String parameter8;
    protected String parameter9;
    protected MemberInfo memberInfo;
    protected String discount;

    /**
     * 获取name属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * 设置name属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }

    /**
     * 获取gender属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGender() {
        return gender;
    }

    /**
     * 设置gender属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGender(String value) {
        this.gender = value;
    }

    /**
     * 获取listOfMemVoucherInfo属性的值。
     * 
     * @return
     *     possible object is
     *     {@link ListOfMemVoucherInfo }
     *     
     */
    public ListOfMemVoucherInfo getListOfMemVoucherInfo() {
        return listOfMemVoucherInfo;
    }

    /**
     * 设置listOfMemVoucherInfo属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link ListOfMemVoucherInfo }
     *     
     */
    public void setListOfMemVoucherInfo(ListOfMemVoucherInfo value) {
        this.listOfMemVoucherInfo = value;
    }

    /**
     * 获取phone属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPhone() {
        return phone;
    }

    /**
     * 设置phone属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPhone(String value) {
        this.phone = value;
    }

    /**
     * 获取availablePoints属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAvailablePoints() {
        return availablePoints;
    }

    /**
     * 设置availablePoints属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAvailablePoints(String value) {
        this.availablePoints = value;
    }

    /**
     * 获取birthday属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBirthday() {
        return birthday;
    }

    /**
     * 设置birthday属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBirthday(String value) {
        this.birthday = value;
    }

    /**
     * 获取neverEmail属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNeverEmail() {
        return neverEmail;
    }

    /**
     * 设置neverEmail属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNeverEmail(String value) {
        this.neverEmail = value;
    }

    /**
     * 获取neverSMS属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNeverSMS() {
        return neverSMS;
    }

    /**
     * 设置neverSMS属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNeverSMS(String value) {
        this.neverSMS = value;
    }

    /**
     * 获取neverCall属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNeverCall() {
        return neverCall;
    }

    /**
     * 设置neverCall属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNeverCall(String value) {
        this.neverCall = value;
    }

    /**
     * 获取neverWechat属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNeverWechat() {
        return neverWechat;
    }

    /**
     * 设置neverWechat属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNeverWechat(String value) {
        this.neverWechat = value;
    }

    /**
     * 获取memValClass属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMemValClass() {
        return memValClass;
    }

    /**
     * 设置memValClass属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMemValClass(String value) {
        this.memValClass = value;
    }

    /**
     * 获取yearShoppingAmount属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getYearShoppingAmount() {
        return yearShoppingAmount;
    }

    /**
     * 设置yearShoppingAmount属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setYearShoppingAmount(String value) {
        this.yearShoppingAmount = value;
    }

    /**
     * 获取average属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAverage() {
        return average;
    }

    /**
     * 设置average属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAverage(String value) {
        this.average = value;
    }

    /**
     * 获取lastBuyDate属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLastBuyDate() {
        return lastBuyDate;
    }

    /**
     * 设置lastBuyDate属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLastBuyDate(String value) {
        this.lastBuyDate = value;
    }

    /**
     * 获取judgement属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getJudgement() {
        return judgement;
    }

    /**
     * 设置judgement属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setJudgement(String value) {
        this.judgement = value;
    }

    /**
     * 获取dressingStyle属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDressingStyle() {
        return dressingStyle;
    }

    /**
     * 设置dressingStyle属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDressingStyle(String value) {
        this.dressingStyle = value;
    }

    /**
     * 获取totalBuyNum属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTotalBuyNum() {
        return totalBuyNum;
    }

    /**
     * 设置totalBuyNum属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTotalBuyNum(String value) {
        this.totalBuyNum = value;
    }

    /**
     * 获取buyReason属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBuyReason() {
        return buyReason;
    }

    /**
     * 设置buyReason属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBuyReason(String value) {
        this.buyReason = value;
    }

    /**
     * 获取parameter1属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getParameter1() {
        return parameter1;
    }

    /**
     * 设置parameter1属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setParameter1(String value) {
        this.parameter1 = value;
    }

    /**
     * 获取parameter2属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getParameter2() {
        return parameter2;
    }

    /**
     * 设置parameter2属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setParameter2(String value) {
        this.parameter2 = value;
    }

    /**
     * 获取parameter3属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getParameter3() {
        return parameter3;
    }

    /**
     * 设置parameter3属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setParameter3(String value) {
        this.parameter3 = value;
    }

    /**
     * 获取parameter4属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getParameter4() {
        return parameter4;
    }

    /**
     * 设置parameter4属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setParameter4(String value) {
        this.parameter4 = value;
    }

    /**
     * 获取parameter5属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getParameter5() {
        return parameter5;
    }

    /**
     * 设置parameter5属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setParameter5(String value) {
        this.parameter5 = value;
    }

    /**
     * 获取zodiac属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getZodiac() {
        return zodiac;
    }

    /**
     * 设置zodiac属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setZodiac(String value) {
        this.zodiac = value;
    }

    /**
     * 获取email属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEmail() {
        return email;
    }

    /**
     * 设置email属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEmail(String value) {
        this.email = value;
    }

    /**
     * 获取job属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getJob() {
        return job;
    }

    /**
     * 设置job属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setJob(String value) {
        this.job = value;
    }

    /**
     * 获取educationalLevel属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEducationalLevel() {
        return educationalLevel;
    }

    /**
     * 设置educationalLevel属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEducationalLevel(String value) {
        this.educationalLevel = value;
    }

    /**
     * 获取region属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRegion() {
        return region;
    }

    /**
     * 设置region属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRegion(String value) {
        this.region = value;
    }

    /**
     * 获取store属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStore() {
        return store;
    }

    /**
     * 设置store属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStore(String value) {
        this.store = value;
    }

    /**
     * 获取guide属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGuide() {
        return guide;
    }

    /**
     * 设置guide属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGuide(String value) {
        this.guide = value;
    }

    /**
     * 获取memberType属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMemberType() {
        return memberType;
    }

    /**
     * 设置memberType属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMemberType(String value) {
        this.memberType = value;
    }

    /**
     * 获取height属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHeight() {
        return height;
    }

    /**
     * 设置height属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHeight(String value) {
        this.height = value;
    }

    /**
     * 获取colorPrefer属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getColorPrefer() {
        return colorPrefer;
    }

    /**
     * 设置colorPrefer属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setColorPrefer(String value) {
        this.colorPrefer = value;
    }

    /**
     * 获取bodyShape属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBodyShape() {
        return bodyShape;
    }

    /**
     * 设置bodyShape属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBodyShape(String value) {
        this.bodyShape = value;
    }

    /**
     * 获取weight属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWeight() {
        return weight;
    }

    /**
     * 设置weight属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWeight(String value) {
        this.weight = value;
    }

    /**
     * 获取waistline属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWaistline() {
        return waistline;
    }

    /**
     * 设置waistline属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWaistline(String value) {
        this.waistline = value;
    }

    /**
     * 获取outsteam属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOutsteam() {
        return outsteam;
    }

    /**
     * 设置outsteam属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOutsteam(String value) {
        this.outsteam = value;
    }

    /**
     * 获取neck属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNeck() {
        return neck;
    }

    /**
     * 设置neck属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNeck(String value) {
        this.neck = value;
    }

    /**
     * 获取hipline属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHipline() {
        return hipline;
    }

    /**
     * 设置hipline属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHipline(String value) {
        this.hipline = value;
    }

    /**
     * 获取bust属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBust() {
        return bust;
    }

    /**
     * 设置bust属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBust(String value) {
        this.bust = value;
    }

    /**
     * 获取shoulder属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getShoulder() {
        return shoulder;
    }

    /**
     * 设置shoulder属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setShoulder(String value) {
        this.shoulder = value;
    }

    /**
     * 获取belly属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBelly() {
        return belly;
    }

    /**
     * 设置belly属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBelly(String value) {
        this.belly = value;
    }

    /**
     * 获取sleeve属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSleeve() {
        return sleeve;
    }

    /**
     * 设置sleeve属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSleeve(String value) {
        this.sleeve = value;
    }

    /**
     * 获取skirtlength属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSkirtlength() {
        return skirtlength;
    }

    /**
     * 设置skirtlength属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSkirtlength(String value) {
        this.skirtlength = value;
    }

    /**
     * 获取province属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProvince() {
        return province;
    }

    /**
     * 设置province属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProvince(String value) {
        this.province = value;
    }

    /**
     * 获取city属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCity() {
        return city;
    }

    /**
     * 设置city属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCity(String value) {
        this.city = value;
    }

    /**
     * 获取area属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getArea() {
        return area;
    }

    /**
     * 设置area属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setArea(String value) {
        this.area = value;
    }

    /**
     * 获取address属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAddress() {
        return address;
    }

    /**
     * 设置address属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAddress(String value) {
        this.address = value;
    }

    /**
     * 获取description属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescription() {
        return description;
    }

    /**
     * 设置description属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescription(String value) {
        this.description = value;
    }

    /**
     * 获取hobby属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHobby() {
        return hobby;
    }

    /**
     * 设置hobby属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHobby(String value) {
        this.hobby = value;
    }

    /**
     * 获取attend属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAttend() {
        return attend;
    }

    /**
     * 设置attend属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAttend(String value) {
        this.attend = value;
    }

    /**
     * 获取joinDate属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getJoinDate() {
        return joinDate;
    }

    /**
     * 设置joinDate属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setJoinDate(String value) {
        this.joinDate = value;
    }

    /**
     * 获取parameter6属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getParameter6() {
        return parameter6;
    }

    /**
     * 设置parameter6属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setParameter6(String value) {
        this.parameter6 = value;
    }

    /**
     * 获取parameter7属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getParameter7() {
        return parameter7;
    }

    /**
     * 设置parameter7属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setParameter7(String value) {
        this.parameter7 = value;
    }

    /**
     * 获取parameter8属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getParameter8() {
        return parameter8;
    }

    /**
     * 设置parameter8属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setParameter8(String value) {
        this.parameter8 = value;
    }

    /**
     * 获取parameter9属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getParameter9() {
        return parameter9;
    }

    /**
     * 设置parameter9属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setParameter9(String value) {
        this.parameter9 = value;
    }

	public MemberInfo getMemberInfo() {
		return memberInfo;
	}

	public void setMemberInfo(MemberInfo memberInfo) {
		this.memberInfo = memberInfo;
	}

	public String getDiscount() {
		return discount;
	}

	public void setDiscount(String discount) {
		this.discount = discount;
	}


}
