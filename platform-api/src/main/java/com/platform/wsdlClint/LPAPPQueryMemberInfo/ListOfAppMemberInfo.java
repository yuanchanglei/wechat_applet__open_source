
package com.platform.wsdlClint.LPAPPQueryMemberInfo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>ListOfAppMemberInfo complex type的 Java 类。
 * 
 * <p>以下模式片段指定包含在此类中的预期内容。
 * 
 * <pre>
 * &lt;complexType name="ListOfAppMemberInfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="AppMemberInfo" type="{http://www.siebel.com/xml/LP%20APP%20Member%20Info%20IO}AppMemberInfo"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ListOfAppMemberInfo", propOrder = {
    "appMemberInfo"
})
public class ListOfAppMemberInfo {

    @XmlElement(name = "AppMemberInfo", required = true)
    protected AppMemberInfo appMemberInfo;

    /**
     * 获取appMemberInfo属性的值。
     * 
     * @return
     *     possible object is
     *     {@link AppMemberInfo }
     *     
     */
    public AppMemberInfo getAppMemberInfo() {
        return appMemberInfo;
    }

    /**
     * 设置appMemberInfo属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link AppMemberInfo }
     *     
     */
    public void setAppMemberInfo(AppMemberInfo value) {
        this.appMemberInfo = value;
    }

}
