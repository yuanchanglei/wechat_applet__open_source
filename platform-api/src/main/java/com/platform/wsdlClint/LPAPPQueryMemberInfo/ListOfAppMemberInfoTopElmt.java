
package com.platform.wsdlClint.LPAPPQueryMemberInfo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>ListOfAppMemberInfoTopElmt complex type的 Java 类。
 * 
 * <p>以下模式片段指定包含在此类中的预期内容。
 * 
 * <pre>
 * &lt;complexType name="ListOfAppMemberInfoTopElmt">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ListOfAppMemberInfo" type="{http://www.siebel.com/xml/LP%20APP%20Member%20Info%20IO}ListOfAppMemberInfo"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ListOfAppMemberInfoTopElmt", propOrder = {
    "listOfAppMemberInfo"
})
public class ListOfAppMemberInfoTopElmt {

    @XmlElement(name = "ListOfAppMemberInfo", required = true)
    protected ListOfAppMemberInfo listOfAppMemberInfo;

    /**
     * 获取listOfAppMemberInfo属性的值。
     * 
     * @return
     *     possible object is
     *     {@link ListOfAppMemberInfo }
     *     
     */
    public ListOfAppMemberInfo getListOfAppMemberInfo() {
        return listOfAppMemberInfo;
    }

    /**
     * 设置listOfAppMemberInfo属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link ListOfAppMemberInfo }
     *     
     */
    public void setListOfAppMemberInfo(ListOfAppMemberInfo value) {
        this.listOfAppMemberInfo = value;
    }

}
