
package com.platform.wsdlClint.LPAPPQueryMemberInfo;

import java.net.MalformedURLException;
import java.net.URL;

import javax.jws.HandlerChain;
import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import javax.xml.ws.WebEndpoint;
import javax.xml.ws.WebServiceClient;
import javax.xml.ws.WebServiceException;
import javax.xml.ws.WebServiceFeature;

import com.platform.utils.ResourceUtil;


/**
 * This class was generated by the JAX-WS RI.
 * JAX-WS RI 2.2.9-b130926.1035
 * Generated source version: 2.2
 * 
 */
@WebServiceClient(name = "LP_spcAPP_spcMember_spcQuery_spcBS", targetNamespace = "http://siebel.com.loyalty/eai/LPAPPMemberQueryBS", wsdlLocation = "${wsdlLocation_LPAPPQueryMemberInfo}")
//@WebServiceClient(name = "LP_spcAPP_spcMember_spcQuery_spcBS", targetNamespace = "http://siebel.com.loyalty/eai/LPAPPMemberQueryBS", wsdlLocation = "file:/D:/workSpaseTwo/wechat_applet__open_source/platform-api/src/main/java/com/platform/wsdl/LPAPPQueryMemberInfo.WSDL")
@HandlerChain(file="../handler-chain.xml")
public class LPSpcAPPSpcMemberSpcQuerySpcBS_Service
    extends Service
{

    private final static URL LPSPCAPPSPCMEMBERSPCQUERYSPCBS_WSDL_LOCATION;
    private final static WebServiceException LPSPCAPPSPCMEMBERSPCQUERYSPCBS_EXCEPTION;
    private final static QName LPSPCAPPSPCMEMBERSPCQUERYSPCBS_QNAME = new QName("http://siebel.com.loyalty/eai/LPAPPMemberQueryBS", "LP_spcAPP_spcMember_spcQuery_spcBS");

    static {
        URL url = null;
        WebServiceException e = null;
        try {
          url = new URL(ResourceUtil.getConfigByName("wsdlLocation_LPAPPQueryMemberInfo"));
          //           url = new URL("file:/D:/workSpaseTwo/wechat_applet__open_source/platform-api/src/main/java/com/platform//wsdl/LPAPPQueryMemberInfo.WSDL");
        } catch (MalformedURLException ex) {
            e = new WebServiceException(ex);
        }
        LPSPCAPPSPCMEMBERSPCQUERYSPCBS_WSDL_LOCATION = url;
        LPSPCAPPSPCMEMBERSPCQUERYSPCBS_EXCEPTION = e;
    }

    public LPSpcAPPSpcMemberSpcQuerySpcBS_Service() {
        super(__getWsdlLocation(), LPSPCAPPSPCMEMBERSPCQUERYSPCBS_QNAME);
    }

    public LPSpcAPPSpcMemberSpcQuerySpcBS_Service(WebServiceFeature... features) {
        super(__getWsdlLocation(), LPSPCAPPSPCMEMBERSPCQUERYSPCBS_QNAME, features);
    }

    public LPSpcAPPSpcMemberSpcQuerySpcBS_Service(URL wsdlLocation) {
        super(wsdlLocation, LPSPCAPPSPCMEMBERSPCQUERYSPCBS_QNAME);
    }

    public LPSpcAPPSpcMemberSpcQuerySpcBS_Service(URL wsdlLocation, WebServiceFeature... features) {
        super(wsdlLocation, LPSPCAPPSPCMEMBERSPCQUERYSPCBS_QNAME, features);
    }

    public LPSpcAPPSpcMemberSpcQuerySpcBS_Service(URL wsdlLocation, QName serviceName) {
        super(wsdlLocation, serviceName);
    }

    public LPSpcAPPSpcMemberSpcQuerySpcBS_Service(URL wsdlLocation, QName serviceName, WebServiceFeature... features) {
        super(wsdlLocation, serviceName, features);
    }

    /**
     * 
     * @return
     *     returns LPSpcAPPSpcMemberSpcQuerySpcBS
     */
    @WebEndpoint(name = "LP_spcAPP_spcMember_spcQuery_spcBS")
    public LPSpcAPPSpcMemberSpcQuerySpcBS getLPSpcAPPSpcMemberSpcQuerySpcBS() {
        return super.getPort(new QName("http://siebel.com.loyalty/eai/LPAPPMemberQueryBS", "LP_spcAPP_spcMember_spcQuery_spcBS"), LPSpcAPPSpcMemberSpcQuerySpcBS.class);
    }

    /**
     * 
     * @param features
     *     A list of {@link javax.xml.ws.WebServiceFeature} to configure on the proxy.  Supported features not in the <code>features</code> parameter will have their default values.
     * @return
     *     returns LPSpcAPPSpcMemberSpcQuerySpcBS
     */
    @WebEndpoint(name = "LP_spcAPP_spcMember_spcQuery_spcBS")
    public LPSpcAPPSpcMemberSpcQuerySpcBS getLPSpcAPPSpcMemberSpcQuerySpcBS(WebServiceFeature... features) {
        return super.getPort(new QName("http://siebel.com.loyalty/eai/LPAPPMemberQueryBS", "LP_spcAPP_spcMember_spcQuery_spcBS"), LPSpcAPPSpcMemberSpcQuerySpcBS.class, features);
    }

    private static URL __getWsdlLocation() {
        if (LPSPCAPPSPCMEMBERSPCQUERYSPCBS_EXCEPTION!= null) {
            throw LPSPCAPPSPCMEMBERSPCQUERYSPCBS_EXCEPTION;
        }
        return LPSPCAPPSPCMEMBERSPCQUERYSPCBS_WSDL_LOCATION;
    }

}
