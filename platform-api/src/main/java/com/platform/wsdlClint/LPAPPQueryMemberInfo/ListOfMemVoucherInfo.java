
package com.platform.wsdlClint.LPAPPQueryMemberInfo;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>ListOfMemVoucherInfo complex type的 Java 类。
 * 
 * <p>以下模式片段指定包含在此类中的预期内容。
 * 
 * <pre>
 * &lt;complexType name="ListOfMemVoucherInfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="MemVoucherInfo" type="{http://www.siebel.com/xml/LP%20APP%20Member%20Info%20IO}MemVoucherInfo" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ListOfMemVoucherInfo", propOrder = {
    "memVoucherInfo"
})
public class ListOfMemVoucherInfo {

    @XmlElement(name = "MemVoucherInfo")
    protected List<MemVoucherInfo> memVoucherInfo;

    /**
     * Gets the value of the memVoucherInfo property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the memVoucherInfo property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getMemVoucherInfo().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link MemVoucherInfo }
     * 
     * 
     */
    public List<MemVoucherInfo> getMemVoucherInfo() {
        if (memVoucherInfo == null) {
            memVoucherInfo = new ArrayList<MemVoucherInfo>();
        }
        return this.memVoucherInfo;
    }

}
