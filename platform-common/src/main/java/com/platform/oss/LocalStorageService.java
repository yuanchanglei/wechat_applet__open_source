package com.platform.oss;

import com.platform.utils.RRException;
import org.apache.commons.io.IOUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

import javax.servlet.http.HttpServletRequest;

/**
 * 本地存储
 *
 * @author lipengjun
 * @email 939961241@qq.com
 * @date 2017-03-25 15:41
 */
public class LocalStorageService extends CloudStorageService {
    private String basePath;//获取当前web项目根路径

    public LocalStorageService(CloudStorageConfig config) {
        this.config = config;
    }

    @Override
    public String upload(HttpServletRequest request,MultipartFile file) throws Exception {
    	basePath = request.getServletContext().getRealPath("/");
        String fileName = file.getOriginalFilename();
        String prefix = fileName.substring(fileName.lastIndexOf(".") + 1);
        return upload(file, getPath(config.getLocalPrefix()) + "." + prefix);
    }
    
    public String upload(MultipartFile file, String path) {
    	String saveFile =basePath+File.separator+path;
       File toFile = new File(saveFile);
       if (!toFile.getParentFile().exists()) {
           toFile.mkdirs();
       }
       try {
    	   file.transferTo(toFile);
       } catch (IOException e) {
           e.printStackTrace();
       }
       System.out.println("*********");

       String path1= Thread.currentThread().getContextClassLoader().getResource("").getPath();//获取当前资源的虚拟路径
    	System.out.println(path1);
       return config.getLocalDomain() + "/" + path;
    }
    @Override
    public String upload(byte[] data, String path) {
    	return null;
    }

    @Override
    public String upload(InputStream inputStream, String path) {
        try {
            byte[] data = IOUtils.toByteArray(inputStream);
            return this.upload(data, path);
        } catch (IOException e) {
            throw new RRException("上传文件失败", e);
        }
    }

	
}
