$(function () {
    $("#jqGrid").Grid({
        url: '../sys/user/list?status=2',
        colModel: [
            {label: '用户ID', name: 'userId', index: "user_id", key: true, hidden: true},
            {label: '姓名', name: 'username', width: 75},
            {label: '店铺名称', name: 'merchantName', width: 75},
//            {label: '所属部门', name: 'deptName', width: 75},
            {label: '手机号', name: 'mobile', width: 100},
            {
            	label: '创建时间', name: 'createTime', index: "create_time", width: 80, formatter: function (value) {
            		return transDate(value);
            	}
            },
            {
                label: '状态', name: 'status', align: 'center',width: 80, formatter: function (value) {
                	switch(value) {
                    case 0:
                    	return '<span class="label label-danger">禁用</span>';
                       break;
                    case 1:
                    	return '<span class="label label-success">正常</span>';
                       break;
                    case 2:
                        return '<span class="label label-success">待审核</span>';
                        break; 
                    case 3:
                    	return '<span class="label label-warning">已退回修改</span>';
                        break;
                    case 4:
                    	return '<span class="label label-danger">已拒绝用户申请</span>';
                        break;
                    default:
                    	return '<span class="label label-success"></span>';
                	} 
                }
            },
            {
                label: '操作', width: 80, align: 'center', sortable: false, formatter: function (value, col, row) {
                	if(row.status==3){
                		str = '<button class="btn btn-default  btn-info " disabled="disabled" >&nbsp; 待重新提交</button>';
                	}else{
                		str = '<button class="btn btn-outline btn-info" onclick="vm.lookDetail(' + row.userId + ')"><i class="fa fa-info-circle"></i>&nbsp; 审核</button>'
                	}
                	
                    return str;
                }
            }]
    });
});

var setting = {
    data: {
        simpleData: {
            enable: true,
            idKey: "deptId",
            pIdKey: "parentId",
            rootPId: -1
        },
        key: {
            url: "nourl"
        }
    }
};
var ztree;

var vm = new Vue({
    el: '#rrapp',
    data: {
        q: {
            username: null,
            mobile: null
        },
        showList: true,
        detail: false,
        title: null,
        roleList: {},
        user: {
            status: 2,
            allShow: 0,
            deptName: '',
            roleIdList: []
        },
        ruleValidate: {}
    },
    methods: {
        query: function () {
            vm.reload(1);
        },
        lookDetail: function (rowId) { //第三步：定义编辑操作
        	 //获取角色信息
            this.getRoleList();
            vm.detail = true;
            vm.title = "审核";
            Ajax.request({
                url: "../sys/user/info/" + rowId,
                async: true,
                successCallback: function (r) {
                    vm.user = r.user;
                }
            });
        },
        add: function () {
            vm.showList = false;
            vm.title = "新增(默认密码：888888)";
            vm.roleList = {};
            vm.user = {status: 1, roleIdList: [], deptId: '', deptName: ''};

            //获取角色信息
            this.getRoleList();
            vm.getDept();
        },
        getDept: function () {
            //加载部门树
            Ajax.request({
                url: '../sys/dept/list',
                async: true,
                successCallback: function (r) {
                    ztree = $.fn.zTree.init($("#deptTree"), setting, r.list);
                    var node = ztree.getNodeByParam("deptId", vm.user.deptId);
                    if (node != null) {
                        ztree.selectNode(node);

                        vm.user.deptName = node.name;
                    }
                }
            });
        },
        update: function () {
            var userId = getSelectedRow("#jqGrid");
            if (userId == null) {
                return;
            }

            vm.showList = false;
            vm.title = "修改";

            Ajax.request({
                url: "../sys/user/info/" + userId,
                async: true,
                successCallback: function (r) {
                    vm.user = r.user;
                    //获取角色信息
                    vm.getRoleList();
                    vm.getDept();
                }
            });

        },
        del: function () {
            var userIds = getSelectedRows("#jqGrid");
            if (userIds == null) {
                return;
            }

            confirm('确定要删除选中的记录？', function () {
                Ajax.request({
                    url: "../sys/user/delete",
                    params: JSON.stringify(userIds),
                    contentType: "application/json",
                    type: 'POST',
                    successCallback: function () {
                        alert('操作成功', function (index) {
                            vm.reload();
                        });
                    }
                });
            });
        },
        saveOrUpdate: function (event) {
            var url = vm.user.userId == null ? "../sys/user/save" : "../sys/user/update";
            Ajax.request({
                url: url,
                params: JSON.stringify(vm.user),
                contentType: "application/json",
                type: 'POST',
                successCallback: function () {
                    alert('操作成功', function (index) {
                        vm.reload();
                    });
                }
            });
        },
        getRoleList: function () {
            Ajax.request({
                url: '../sys/role/select',
                async: true,
                successCallback: function (r) {
                    vm.roleList = r.list;
                }
            });
        },
        reload: function (event) {
        	vm.showList = true;
            vm.detail = false;
            var page = $("#jqGrid").jqGrid('getGridParam', 'page');
            if(typeof(event) != undefined){
            	page= event;
            }
            $("#jqGrid").jqGrid('setGridParam', {
                postData: {'username': vm.q.username,"mobile":vm.q.mobile},
                page: page
            }).trigger("reloadGrid");
            vm.handleReset('formValidate');
        },
        deptTree: function () {
            openWindow({
                title: "选择部门",
                area: ['300px', '450px'],
                content: jQuery("#deptLayer"),
                btn: ['确定', '取消'],
                btn1: function (index) {
                    var node = ztree.getSelectedNodes();
                    //选择上级部门
                    vm.user.deptId = node[0].deptId;
                    vm.user.deptName = node[0].name;

                    layer.close(index);
                }
            });
        },
        handleSubmit: function (name) {
        		vm.user.status = 1;
                vm.saveOrUpdate()
        },
        //退回修改填写拒绝的理由。
        returnSubmit: function (name) {
        	layer.open({
        		  type: 1,
                  title: "请填写退回原因",
                  area: ['300px', '200px'],
                  content: $("#rejectInfoDiv"),
                  btn: ['确定', '取消'],
                  btn1: function (index) {
                	  vm.user.rejectInfo = $("#rejectInfo").val();
                      vm.user.status = 3;
                      vm.saveOrUpdate()
                      layer.close(index);
                  }
              });
        		
        },
        //拒绝申请；填写拒绝的理由。
        rejectSubmit: function (name) {
        	layer.open({
        		  type: 1,
                  title: "请填写拒绝原因",
                  area: ['300px', '200px'],
                  content: $("#rejectInfoDiv"),
                  btn: ['确定', '取消'],
                  btn1: function (index) {
                	  vm.user.rejectInfo = $("#rejectInfo").val();
                      vm.user.status = 4;
                      vm.saveOrUpdate()
                      layer.close(index);
                  }
              });
        		
        },
        handleReset: function (name) {
            handleResetForm(this, name);
        }
    }
});