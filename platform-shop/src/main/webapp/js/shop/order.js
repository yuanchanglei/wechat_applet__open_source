$(function () {
    let shippingStatus = getQueryString("shippingStatus");
    let payStatus = getQueryString("payStatus");
    let orderStatus = getQueryString("orderStatus");
    let orderType = getQueryString("orderType");
    let url = '../order/list';
    if (shippingStatus) {
        url += '?shippingStatus=' + shippingStatus;
    }
    if (payStatus) {
        url += '?payStatus=' + payStatus;
    }
    if (orderStatus) {
        url += '?orderStatus=' + orderStatus;
    }
    if (orderType) {
        url += '?orderType=' + orderType;
    }
    $("#jqGrid").Grid({
        url: url,
        datatype: "json",
        colModel: [
            {label: 'id', name: 'id', index: 'id', key: true, hidden: true},
            {label: '商户名称', name: 'merchantName', index: 'merchantName', width: 100},
            {label: '订单号', name: 'orderSn', index: 'order_sn', width: 100},
            {label: '会员', name: 'userName', index: 'username', width: 70},
//            {label: '真实姓名', name: 'realName', index: 'realName', width: 80},
            {
                label: '订单类型', name: 'orderType', index: 'orderType', width: 80, formatter: function (value) {
                    if (value == '1') {
                        return '购物车订单';
                    } else if (value == '2') {
                        return '普通订单';
                    } else if (value == '3') {
                        return '秒杀订单';
                    } else if (value == '4') {
                        return '团购订单';
                    }
                    return '-';
                }
            },
            {
                label: '订单状态', name: 'orderStatus', index: 'orderStatus', width: 90, formatter: function (value) {
                    if (value == '0') {
                        return '待付款';
                    } else if (value == '101') {
                        return '订单已取消';
                    } else if (value == '102') {
                        return '订单已删除';
                    } else if (value == '103') {
                        return '订单已失效';
                    }else if (value == '201') {
                        return '订单已付款';
                    } else if (value == '300') {
                        return '订单已发货';
                    } else if (value == '301') {
                        return '用户确认收货';
                    } else if (value == '401') {
                        return '没有发货-退款';
                    } else if (value == '402') {
                        return '订单完成-退款';
                    }else if (value == '403') {
                        return '订单完成-部分退款';
                    }
                    else if (value == '501'||value == '502'||value == '503'||value == '504'||value == '505') {
                        return '售后中';
                    }
                   /*  else if (value == '501') {
                        return '退货:买家申请退款';
                    }else if (value == '502') {
                        return '退货:待买家发货';
                    }else if (value == '503') {
                        return '退货:待卖家收货';
                    }else if (value == '504') {
                        return '退货:待确认退款';
                    }else if (value == '505') {
                        return '换货：待卖家发货';
                    }
                    */
                    return value;
                }
            },
            {
                label: '发货状态',
                name: 'shippingStatus',
                index: 'shippingStatus',
                width: 60,
                formatter: function (value) {
                    if (value == '0') {
                        return '未发货';
                    } else if (value == '1') {
                        return '已发货';
                    } else if (value == '2') {
                        return '已收货';
                    }else if (value == '3') {
                        return '部分退货';
                    }else if (value == '4') {
                        return '退货';
                    }
                    return value;
                }
            },
            {
                label: '付款状态', name: 'payStatus', index: 'payStatus', width: 80,
                formatter: function (value) {
                    if (value == '0') {
                        return '未付款';
                    } else if (value == '1') {
                        return '付款中';
                    } else if (value == '2') {
                        return '已付款';
                    }else if (value == '3') {
                        return '部分退款';
                    }else if (value == '4') {
                        return '已退款';
                    }
                    return value;
                }
            },
//            {label: '快递公司', name: 'shippingName', index: 'shipping_name', width: 80},
//            {label: '快递单号', name: 'shippingNo', index: 'shipping_No', width: 80},
            {label: '实际支付金额', name: 'actualPrice', index: 'actualPrice', width: 80
            	,
                formatter: function (value, col, row) {
                    if (row.payStatus == '2') {
                        return value;
                    } else if (row.payStatus == '4') {
                        return '- '+value;
                    } else{
                        return '0';
                    }
                }
            },
            {label: '订单总价', name: 'orderPrice', index: 'orderPrice', width: 60},
            {label: '商品总价', name: 'goodsPrice', index: 'goodsPrice', width: 60},
            {
                label: '下单时间', name: 'addTime', index: 'addTime', width: 80,
                formatter: function (value) {
                    return transDate(value);
                }
            },
            {
                label: '是否需要开票', name: 'isNeedInvoice', index: 'isNeedInvoice', width: 80,
                formatter: function (value, col, row) {
                	if(row.payStatus==2){
                		if (value == '0'||value==null) {
                            return '<button type="button" class="btn btn-default btn-xs">不需要开票</button>';
                        } else if (value == '1'||value == '2') {
//                            return  '<button onclick="vm.viewInvoice(\''+row.id+'\',\''+row.enterpriseName+'\',\''+row.taxNumber+'\')"'+
//                            			' type="button" class="btn btn btn-warning btn-xs">需要开票</button>';;
                			 return  '<button onclick="vm.viewInvoice(\''+row.id+'\',\''+row.isNeedInvoice+'\')"'+
                 			' type="button" class="btn btn btn-warning btn-xs">需要开票</button>';;                            			
                        } else if (value == '3') {
                        	 return '<button type="button" class="btn btn-success btn-xs">已完成开票</button>';
                        }
                	}else{
                		 return '<button type="button" class="btn btn-default btn-xs">不需要开票</button>';
                	}
                    
                    return value;
                }
            },
            {
                label: '操作', width: 100, align: 'center', sortable: false, formatter: function (value, col, row) {
                    return '<button class="btn btn-outline btn-info" onclick="vm.lookDetail(' + row.id + ')"><i class="fa fa-info-circle"></i>&nbsp;详情</button>' ;
                /*    return '<button class="btn btn-outline btn-info" onclick="vm.lookDetail(' + row.id + ')"><i class="fa fa-info-circle"></i>&nbsp;详情</button>' +
                        '<button class="btn btn-outline btn-primary" style="margin-left: 10px;" onclick="vm.printDetail(' + row.id + ')"><i class="fa fa-print"></i>&nbsp;打印</button>';*/
                }
            }
        ]
    });
});

let vm = new Vue({
    el: '#rrapp',
    data: {
        showList: true,
        detail: false,
        title: null,
        order: {},
        shippings: [],
        flag:"true",
        q: {
            orderSn: '',
            orderStatus: '',
            orderType: ''
        }
    },
    methods: {
        query: function () {
            vm.reload(1);
        },
        sendGoods: function (event) {
            let id = getSelectedRow("#jqGrid");
            if (id == null) {
                return;
            }
            let rowDate = getSelectedRowData("#jqGrid")
            if(rowDate.payStatus=='已付款'&&rowDate.shippingStatus=='未发货'){
                vm.showList = false;
                vm.title = "发货";
                vm.flag="true";
                Ajax.request({
                    url: "../order/info/" + id,
                    async: true,
                    successCallback: function (r) {
                        vm.order = r.order;
                    }
                });
            }else{
            	 alert("无权操作当前数据！");
            }

        },
        EditSendGoods: function (event) {
            let id = getSelectedRow("#jqGrid");
            if (id == null) {
                return;
            }
            let rowDate = getSelectedRowData("#jqGrid")
            if(rowDate.payStatus=='已付款'&&rowDate.shippingStatus=='已发货'){
                vm.showList = false;
                vm.title = "修改发货信息";
                vm.flag="false";
                Ajax.request({
                    url: "../order/info/" + id,
                    async: true,
                    successCallback: function (r) {
                        vm.order = r.order;
                    }
                });
            }else{
            	 alert("无权操作当前数据！");
            }

        },
        viewInvoice:function(id,type){
        	//type:1 普票； 2：专票
        	var area=['400px', '300px'];
            Ajax.request({
                url: "../order/info/" + id,
                async: true,
                successCallback: function (r) {
                	if(type=="1"){
                		$("#viewInvoiceDiv .enterpriseName").val(r.order.enterpriseName);
                    	$("#viewInvoiceDiv .taxNumber").val(r.order.taxNumber);
                	}else{
                		area=['400px', '400px'];
                		$("#viewInvoiceDiv .enterpriseName").val(r.order.enterpriseName);
                    	$("#viewInvoiceDiv .taxNumber").val(r.order.taxNumber);
                    	$("#viewInvoiceDiv .InvoiceType").css("display","block");
                    	$("#viewInvoiceDiv .enterpriseAddr").val(r.order.enterpriseAddr);
                    	$("#viewInvoiceDiv .enterprisePhone").val(r.order.enterprisePhone);
                    	$("#viewInvoiceDiv .enterpriseOpeningBank").val(r.order.enterpriseOpeningBank);
                    	$("#viewInvoiceDiv .enterpriseAccount").val(r.order.enterpriseAccount);
                	}
                	
                  	layer.open({
                		  type: 1,
                          title: "发票信息",
                          area: area,
                          content: $("#viewInvoiceDiv"),
                          btn: ['确定'],
                          btn1: function (index) {
                              layer.close(index);
                              $("#viewInvoiceDiv .InvoiceType").css("display","none");
                              $("#viewInvoiceDiv .enterpriseName").val("");
                              $("#viewInvoiceDiv .taxNumber").val("");
                              $("#viewInvoiceDiv .enterpriseAddr").val("");
                              $("#viewInvoiceDiv .enterprisePhone").val("");
                              $("#viewInvoiceDiv .enterpriseOpeningBank").val("");
                              $("#viewInvoiceDiv .enterpriseAccount").val("");
                          }
                      });
                }
            });
        },
        confirm: function (event) {
            let id = getSelectedRow("#jqGrid");
            if (id == null) {
                return;
            }
            let rowDate = getSelectedRowData("#jqGrid")
            if(rowDate.shippingStatus=='已发货'){
                confirm('确定收货？', function () {
                    Ajax.request({
                        type: "POST",
                        url: "../order/confirm",
                        contentType: "application/json",
                        params: JSON.stringify(id),
                        successCallback: function (r) {
                            if (r.code == 0) {
                                alert('操作成功', function (index) {
                                    vm.reload();
                                });
                            } else {
                                alert(r.msg);
                            }
                        }
                    });
                });
            }else{
            	 alert("无权操作当前数据！");
            }
   
        },
        saveOrUpdate: function (event) {
        	var biaoshi = vm.flag;
        	if(biaoshi=="true"){
        		   Ajax.request({
                       type: "POST",
                       url: "../order/sendGoods",
                       contentType: "application/json",
                       params: JSON.stringify(vm.order),
                       successCallback: function (r) {
                           vm.reload();
                       }
                   });
        	}else{
        		   Ajax.request({
                       type: "POST",
                       url: "../order/EditSendGoods",
                       contentType: "application/json",
                       params: JSON.stringify(vm.order),
                       successCallback: function (r) {
                           vm.reload();
                       }
                   });
        		   vm.flag ="true";
        	}
         
        },
        reload: function (event) {
            vm.showList = true;
            vm.detail = false;
            let page = $("#jqGrid").jqGrid('getGridParam', 'page');
            if(typeof(event) != undefined){
            	page= event;
            }
            $("#jqGrid").jqGrid('setGridParam', {
                postData: {
                    'orderSn': vm.q.orderSn,
                    'orderStatus': vm.q.orderStatus,
                    'orderType': vm.q.orderType
                },
                page: page
            }).trigger("reloadGrid");
        },
        lookDetail: function (rowId) { //第三步：定义编辑操作
            vm.detail = true;
            vm.title = "详情";
            Ajax.request({
                url: "../order/info/" + rowId,
                async: true,
                successCallback: function (r) {
                	vm.order = r.order;
                	 if (vm.order.payStatus == '0') {
                		 vm.order.actualPrice = 0;
                     } else if (vm.order.payStatus == '1') {
                    	 vm.order.actualPrice = 0;
                     } else if (vm.order.payStatus == '2') {
                       
                     }else if (vm.order.payStatus == '4') {
                    	 vm.order.actualPrice = "- "+vm.order.actualPrice;
                     }else{
                    	 vm.order.actualPrice = 0;
                     }
                }
            });
        },
        printDetail: function (rowId) {
            openWindow({
                type: 2,
                title: '<i class="fa fa-print"></i>打印票据',
                content: '../shop/orderPrint.html?orderId=' + rowId
            })
        }
    },
    created: function () {
        let vue = this;
        Ajax.request({
            url: "../shipping/queryAll",
            async: true,
            successCallback: function (r) {
                vue.shippings = r.list;
            }
        });
    }
});