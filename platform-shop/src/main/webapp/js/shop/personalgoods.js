$(function () {
/*    $("#jqGrid").Grid({
        url: '../personalgoods/list',
        colModel: [
			{label: 'id', name: 'id', index: 'id', key: true, hidden: true},
			{label: '商品Id', name: 'goodsId', index: 'goods_id', hidden: true},
			{label: '系统用户id', name: 'sysUserId', index: 'sys_user_id', hidden: true},
			{label: '商户id', name: 'merchantId', index: 'merchant_id', hidden: true},
			{label: '创建时间', name: 'addTime', index: 'add_time', width: 80}]
    });*/
    
    $("#jqGrid").Grid({
        url: '../goods/list?isPersonalGoods=Y',
        colModel: [
            {label: 'id', name: 'id', index: 'id', key: true, hidden: true},
            {label: '商品类型', name: 'categoryName', index: 'category_id', width: 80},
            {label: '名称', name: 'name', index: 'name', width: 160},
            {label: '预览图', name: 'listPicUrl', index: 'listPicUrl', width: 100,formatter: function (value, col, row) {
                return '<img width="100px" src="'+value+'"/>';
            	}
            },
//            {label: '品牌', name: 'brandName', index: 'brand_id', width: 120},
            {
                label: '上架', name: 'isOnSale', index: 'is_on_sale', width: 50,
                formatter: function (value) {
                    return transIsNot(value);
                }
            },
            {
                label: '录入日期', name: 'addTime', index: 'add_time', width: 80, formatter: function (value) {
                    return transDate(value, 'yyyy-MM-dd');
                }
            },
            {label: '属性类别', name: 'attributeCategoryName', index: 'attribute_category', width: 80},
            {label: '吊牌价', name: 'marketPrice', index: 'market_price', width: 80},
            {label: '活动价', name: 'retailPrice', index: 'retail_price', width: 80},
            {label: '商品库存', name: 'goodsNumber', index: 'goods_number', width: 80},
            {label: '销售量', name: 'sellVolume', index: 'sell_volume', width: 80},
            {
                label: '热销', name: 'isHot', index: 'is_hot', width: 80, formatter: function (value) {
                    return transIsNot(value);
                }
            }]
    });
});

let vm = new Vue({
	el: '#rrapp',
	data: {
        showList: true,
        title: null,
        personalGoods: {},
		ruleValidate: {
			name: [
				{required: true, message: '名称不能为空', trigger: 'blur'}
			]
		},
		q: {
		    name: ''
		}
	},
	methods: {
		query: function () {
			vm.reload(1);
		},
		
		openSelect: function () {
            var selectWindow = openWindow({
                type: 2,
                title: '商品库',
                content: '../shop/personalGoodsSelect.html',
                end: function () {
        			vm.reload();
                }
            });
        },
		
		
/*		add: function () {
			vm.showList = false;
			vm.title = "新增";
			vm.personalGoods = {};
		},
		update: function (event) {
            let id = getSelectedRow("#jqGrid");
			if (id == null) {
				return;
			}
			vm.showList = false;
            vm.title = "修改";

            vm.getInfo(id)
		},
		saveOrUpdate: function (event) {
            let url = vm.personalGoods.id == null ? "../personalgoods/save" : "../personalgoods/update";
            Ajax.request({
			    url: url,
                params: JSON.stringify(vm.personalGoods),
                type: "POST",
			    contentType: "application/json",
                successCallback: function (r) {
                    alert('操作成功', function (index) {
                        vm.reload();
                    });
                }
			});
		},
		del: function (event) {
            let ids = getSelectedRows("#jqGrid");
			if (ids == null){
				return;
			}

			confirm('确定要删除选中的记录？', function () {
                Ajax.request({
				    url: "../personalgoods/delete",
                    params: JSON.stringify(ids),
                    type: "POST",
				    contentType: "application/json",
                    successCallback: function () {
                        alert('操作成功', function (index) {
                            vm.reload();
                        });
					}
				});
			});
		},
		*/
		
        deleteByExample: function (event) {
            let goodsIds = getSelectedRows("#jqGrid");
            debugger;
			if (goodsIds == null){
				return;
			}

			confirm('确定要删除选中的记录？', function () {
                Ajax.request({
				    url: "../personalgoods/deleteByExample",
                    params: JSON.stringify(goodsIds),
                    type: "POST",
				    contentType: "application/json",
                    successCallback: function () {
                        alert('操作成功', function (index) {
                            vm.reload();
                        });
					}
				});
			});
		},
		getInfo: function(id){
            Ajax.request({
                url: "../personalgoods/info/"+id,
                async: true,
                successCallback: function (r) {
                    vm.personalGoods = r.personalGoods;
                }
            });
		},
		reload: function (event) {
			vm.showList = true;
            let page = $("#jqGrid").jqGrid('getGridParam', 'page');
            if(typeof(event) != undefined){
            	page= event;
            }
			$("#jqGrid").jqGrid('setGridParam', {
                postData: {'name': vm.q.name},
                page: page
            }).trigger("reloadGrid");
            vm.handleReset('formValidate');
		},
        reloadSearch: function() {
            vm.q = {
                name: ''
            }
            vm.reload();
        },
        handleSubmit: function (name) {
            handleSubmitValidate(this, name, function () {
                vm.saveOrUpdate()
            });
        },
        handleReset: function (name) {
            handleResetForm(this, name);
        }
	}
});