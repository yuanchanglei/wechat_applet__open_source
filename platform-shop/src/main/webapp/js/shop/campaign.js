$(function () {
    $("#jqGrid").Grid({
        url: '../campaign/list',
        colModel: [
			{label: 'id', name: 'id', index: 'id', key: true, hidden: true},
			{label: '活动类型', name: 'campaignType', index: 'campaign_type', width: 40,align: 'center',
	             formatter: function (value) {
	                 if (value == '0') {
	                     return '满减';
	                 } else if (value == '1') {
	                     return '多件多折扣';
	                 } 
	                 return value;
	             }
				},
			{label: '活动名称', name: 'name', index: 'name', width: 80},
			{
                label: '活动开始时间', name: 'startDate', index: 'start_date', width: 80,
                formatter: function (value) {
                    return transDate(value);
                }
            },
            {
                label: '活动结束时间', name: 'endDate', index: 'end_date', width: 80,
                formatter: function (value) {
                    return transDate(value);
                }
            },
			{label: '是否全部商品参加活动',   name: 'isAllGoods',  index: 'isAllGoods', width: 80,align: 'center',
             formatter: function (value) {
                 if (value == '0') {
                     return '部分商品参加';
                 } else if (value == '1') {
                     return '全部商品参加';
                 } 
                 return value;
             }
			},
			{label: '状态',   name: 'status',  index: 'status', width: 80,
	             formatter: function (value) {
	                 if (value == '1') {
	                     return '启用';
	                 } else if (value == '0') {
	                     return '未启用';
	                 } 
	                 return value;
	             }
				}
			,
            {
                label: '操作', width: 100, align: 'center', sortable: false, formatter: function (value, col, row) {
                    if(row.isAllGoods == 0){
                    	return '<button class="btn btn-outline btn-info" onclick=\'vm.lookCampaignGoods(' + row.id +',"'+row.name+'")\'><i class="fa fa-info-circle"></i>&nbsp;查看参加活动的商品</button>' ;

                    }else{
//                    	return '<button class="btn btn-outline btn-info" onclick="vm.lookDetail(' + row.id + ')"><i class="fa fa-info-circle"></i>&nbsp;活动规则</button>' ;
                    }
                }
            }
		]
    });
});

let vm = new Vue({
	el: '#rrapp',
	data: {
        showList: true,
        title: null,
		campaign: {},
		ruleValidate: {
			name: [
				{required: true, message: '名称不能为空', trigger: 'blur'}
			]
		},
		q: {
		    name: ''
		}
	},
	methods: {
		query: function () {
			vm.reload();
		},
		add: function () {
			vm.showList = false;
			vm.title = "新增";
			vm.campaign = {};
		},	
		openSelect: function () {
			let id = getSelectedRow("#jqGrid");
			if (id == null) {
				return;
			}
			let rowData = getSelectedRowData("#jqGrid");
            var selectWindow = openWindow({
                type: 2,
                title: '商品库（为活动添加商品）---'+rowData.name,
                content: '../shop/campaigngoodsSelect.html?campaignId='+id,
                end: function () {
        			vm.reload();
                }
            });
        },
        lookCampaignGoods:function(id,name){
        	 var selectWindow = openWindow({
                 type: 2,
                 title: '（当前参加活动的商品）---'+name,
                 content: '../shop/campaigngoods.html?campaignId='+id,
                 end: function () {
         			vm.reload();
                 }
             });
        },
		update: function (event) {
            let id = getSelectedRow("#jqGrid");
			if (id == null) {
				return;
			}
			vm.showList = false;
            vm.title = "修改";

            vm.getInfo(id)
		},
		saveOrUpdate: function (event) {
            let url = vm.campaign.id == null ? "../campaign/save" : "../campaign/update";
            Ajax.request({
			    url: url,
                params: JSON.stringify(vm.campaign),
                type: "POST",
			    contentType: "application/json",
                successCallback: function (r) {
                    alert('操作成功', function (index) {
                        vm.reload();
                    });
                }
			});
		},
		del: function (event) {
            let ids = getSelectedRows("#jqGrid");
			if (ids == null){
				return;
			}

			confirm('确定要删除选中的记录？', function () {
                Ajax.request({
				    url: "../campaign/delete",
                    params: JSON.stringify(ids),
                    type: "POST",
				    contentType: "application/json",
                    successCallback: function () {
                        alert('操作成功', function (index) {
                            vm.reload();
                        });
					}
				});
			});
		},
		getInfo: function(id){
            Ajax.request({
                url: "../campaign/info/"+id,
                async: true,
                successCallback: function (r) {
                    vm.campaign = r.campaign;
                }
            });
		},
		reload: function (event) {
			vm.showList = true;
            let page = $("#jqGrid").jqGrid('getGridParam', 'page');
			$("#jqGrid").jqGrid('setGridParam', {
                postData: {'name': vm.q.name},
                page: page
            }).trigger("reloadGrid");
            vm.handleReset('formValidate');
		},
        reloadSearch: function() {
            vm.q = {
                name: ''
            }
            vm.reload();
        },
        handleSubmit: function (name) {
            handleSubmitValidate(this, name, function () {
                vm.saveOrUpdate()
            });
        },
        handleReset: function (name) {
            handleResetForm(this, name);
        }
	}
});