$(function () {
    $("#jqGrid").Grid({
        url: '../ordershippingsynchro/list',
        colModel: [
			{label: '订单号', name: 'orderSn', key: true, index: 'order_sn',width: 80},
			{label: '快递公司ID', name: 'shippingId', index: 'shipping_id', width: 80},
			{label: '快递公司名称', name: 'shippingName', index: 'shipping_name', width: 80},
			{label: '快递单号', name: 'shippingNo', index: 'shipping_no', width: 80},
			{label: '数据状态(0未同步,1已同步)', name: 'status', index: 'status', width: 80,align: 'center'},
			{label: '添加时间', name: 'addTime', index: 'addTime', width: 80,}]
    });
    new AjaxUpload('#upload', {
        action: '../ordershippingsynchro/upload',
        name: 'file',
        autoSubmit: true,
        responseType: "json",
        onSubmit: function (file, extension) {
            if (!(extension && /^(xlsx|xls)$/.test(extension.toLowerCase()))) {
                alert('请上传excel文档');
                return false;
            }
            dialogLoading(true);
        },
        onComplete: function (file, r) {
        	 dialogLoading(false);
            if (r.code == 0) {
                alert(r.msg);
                vm.reload();
            } else {
                alert(r.msg);
            }
        }
    });

});

let vm = new Vue({
	el: '#rrapp',
	data: {
        showList: true,
        title: null,
		ordershippingsynchro: {},
		ruleValidate: {
			name: [
				{required: true, message: '名称不能为空', trigger: 'blur'}
			]
		},
		q: {
		    name: ''
		}
	},
	methods: {
		query: function () {
			vm.reload();
		},
		add: function () {
			vm.showList = false;
			vm.title = "新增";
			vm.ordershippingsynchro = {};
		},
		update: function (event) {
            let orderSn = getSelectedRow("#jqGrid");
			if (orderSn == null) {
				return;
			}
			vm.showList = false;
            vm.title = "修改";

            vm.getInfo(orderSn)
		},
		saveOrUpdate: function (event) {
            let url = vm.ordershippingsynchro.orderSn == null ? "../ordershippingsynchro/save" : "../ordershippingsynchro/update";
            Ajax.request({
			    url: url,
                params: JSON.stringify(vm.ordershippingsynchro),
                type: "POST",
			    contentType: "application/json",
                successCallback: function (r) {
                    alert('操作成功', function (index) {
                        vm.reload();
                    });
                }
			});
		},
		del: function (event) {
            let orderSns = getSelectedRows("#jqGrid");
			if (orderSns == null){
				return;
			}

			confirm('确定要删除选中的记录？', function () {
				 dialogLoading(true);
                Ajax.request({
				    url: "../ordershippingsynchro/delete",
                    params: JSON.stringify(orderSns),
                    type: "POST",
				    contentType: "application/json",
                    successCallback: function () {
                    	dialogLoading(false);
                        alert('操作成功', function (index) {
                            vm.reload();
                        });
					}
				});
			});
		},
		synchroData: function (event) {
			var flag =false;
			confirm('确定要同步导入的发货单号么？', function () {
                Ajax.request({
				    url: "../ordershippingsynchro/synchroData",
                    type: "POST",
				    contentType: "application/json",
                    successCallback: function () {
                        alert('操作成功', function (index) {
                            vm.reload();
                        });
					}
				});
			});
		},
		getInfo: function(orderSn){
            Ajax.request({
                url: "../ordershippingsynchro/info/"+orderSn,
                async: true,
                successCallback: function (r) {
                    vm.ordershippingsynchro = r.ordershippingsynchro;
                }
            });
		},
		reload: function (event) {
			vm.showList = true;
            let page = $("#jqGrid").jqGrid('getGridParam', 'page');
			$("#jqGrid").jqGrid('setGridParam', {
                postData: {'name': vm.q.name},
                page: page
            }).trigger("reloadGrid");
            vm.handleReset('formValidate');
		},
        reloadSearch: function() {
            vm.q = {
                name: ''
            }
            vm.reload();
        },
        handleSubmit: function (name) {
            handleSubmitValidate(this, name, function () {
                vm.saveOrUpdate()
            });
        },
        handleReset: function (name) {
            handleResetForm(this, name);
        }
	}
});