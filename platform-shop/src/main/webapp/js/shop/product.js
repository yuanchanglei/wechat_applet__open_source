$(function () {
    let goodsId = getQueryString("goodsId");
    let url = '../product/list';
    if (goodsId) {
        url += '?goodsId=' + goodsId;
        getInfoByinit(goodsId);
    }
    $("#jqGrid").Grid({
        url: url,
        colModel: [
            {label: 'id', name: 'id', index: 'id', key: true, hidden: true},
            {label: '商品', name: 'goodsName', index: 'goods_id', width: 120},
            {label: '商品序列号', name: 'goodsSn', index: 'goods_sn', width: 80},
            {
                label: '商品规格',
                name: 'specificationValue',
                index: 'goods_specification_ids',
                width: 100,
                formatter: function (value, options, row) {
                    return value.replace(row.goodsName + " ", '');
                }
            },
          
            {label: '商品库存', name: 'goodsNumber', index: 'goods_number', width: 80},
            {label: '零售价格(元)', name: 'retailPrice', index: 'retail_price', width: 80},
            {label: '市场价格(元)', name: 'marketPrice', index: 'market_price', width: 80},
            {label: '产品编码', name: 'productCode', index: 'product_code', width: 80}
//            {label: '团购价格(元)', name: 'groupPrice', index: 'group_price', width: 80}
            ]
    });
});

let vm = new Vue({
    el: '#rrapp',
    data: {
        showList: true,
        addGoodsNumberCard: false,
        title: null,
        product: {},
        UpProductGoods: {},
        new_product_goodsNumber:0,
        ruleValidate: {
            name: [
                {required: true, message: '名称不能为空', trigger: 'blur'}
            ]
        },
        q: {
            goodsName: ''
        },
        goodss: [],
        attribute: [],
        specifications: [],
        type: '',
        goodsId:0,
        ggArr:[],
        isSecKill:1,
        isAdd:false,
        params:[
            { param : [] , ggArr:[]},
            { param : [] , ggArr:[]},
            { param : [] , ggArr:[]}
        ]
    },
    methods: {
        query: function () {
            vm.reload(1);
        },
        add: function () {
            vm.showList = false;
            vm.addGoodsNumberCard=false;
            vm.title = "新增："+vm.goods.name;
            vm.product = {groupPrice:0};
            vm.getGoodss();
            vm.type = 'add';
            vm.isAdd=true;
            vm.product.goodsId=vm.goods.id;
            vm.product.goodsName =vm.goods.name;
//            vm.changeGoodsByauto(vm.goods);
            
            $("#product_goodsNumber").readOnly=false;
        },
        update: function (event) {
            let id = getSelectedRow("#jqGrid");
            if (id == null) {
                return;
            }
            vm.showList = false;
            vm.addGoodsNumberCard=false;
            vm.title = "修改："+vm.goods.name;
            vm.type = 'update';
            vm.isAdd=false;
            
            vm.getInfo(id)
            
            $("#product_goodsNumber").readOnly=true;
        },
        
        addGoodsNumber: function (event) {
            let id = getSelectedRow("#jqGrid");
            if (id == null) {
                return;
            }
            vm.showList = false;
            vm.addGoodsNumberCard=true;
            vm.title = "增加库存："+vm.goods.name;
            vm.type = 'updateGoodsNumber';
            vm.getInfo(id)
            
            $("#product_goodsNumber").readOnly=true;
        },
        addGoodsNumberSubmit: function (name) {
        	handleSubmitValidate(this, name, function () {
        		debugger;
        		let id = getSelectedRow("#jqGrid");
        		let addNumber = vm.new_product_goodsNumber;
        		if(id == null || addNumber == null ||addNumber ==""){
        			return false;
        		}
               let url = "../product/addGoodsNumber?id="+id+"&addNumber="+addNumber;
               Ajax.request({
                   type: "POST",
                   url: url,
                   successCallback: function (r) {
                       alert('操作成功', function (index) {
                           vm.reload();
                           vm.UpProductGoods.id="";
                           vm.UpProductGoods.addNumber=0;
                       });
                   }
               });
            });
        },
        changeGoods: function (opt) {
        	if(opt.value=="")
        		return;
            let model= vm.goods
            vm.goodsId = model.id;
            if (vm.type == 'add') {
                vm.product.goodsSn = model.goodsSn;
                vm.product.goodsNumber = 0;
                vm.product.retailPrice = model.retailPrice;
                vm.product.marketPrice = model.marketPrice;
                vm.product.id=null;
            }
            if(!vm.goodsId)return;

            Ajax.request({
                url: "../specification/queryListByGoodsId?goodsId="+vm.goodsId,
                async: true,
                successCallback: function (r) {
                    vm.specifications= r.list;
                    if(vm.specifications.length==0){
                    	layer.alert('当前无<span style="color:red;">“商品规格”</span>，请先添加商品规格后，再添加产品信息！', {icon: 2}, function (index) {
                   		 layer.close(index);
                   		 vm.reload();
                        });
                    }
                }
            });
            vm.isSecKill=model.isSecKill;
        },
        saveOrUpdate: function (event) {
        	if(vm.isAdd==true){
        		if(vm.product.goodsNumber==0){
                    alert("产品库存不能为 （0）");
                    return false;
                }
                if(vm.attribute.length>2){
                	alert('属性最多选择两项');
                	return false;
                }else if(vm.attribute.length==0){
               		alert('请选择添加-商品规格！');
                    return false;
                }
        	
	            if(vm.attribute.length<=1){
	            	if(vm.params[0].param==''){
	            		 alert('请选择具体规格信息');
	            		 return false;
	            	}
	                vm.product.goodsSpecificationIds = vm.params[0].param+'_';
	            }else if(vm.attribute.length<=2){
	            	if(vm.params[0].param==''||vm.params[1].param==''){
	           		 	alert('请选择具体规格信息');
	           			return false;
	            	}
	                vm.product.goodsSpecificationIds = vm.params[0].param + '_' + vm.params[1].param ;
	            }else if(vm.attribute.length<=3){
	            	if(vm.params[0].param==''||vm.params[1].param==''||vm.params[2].param==''){
	           		 	alert('请选择具体规格信息');
	           		 return false;
	            	}
	                vm.product.goodsSpecificationIds = vm.params[0].param + '_' + vm.params[1].param + '_' + vm.params[2].param;
	            }else{
	                vm.product.goodsSpecificationIds='';
	            }
        	}
            let url = vm.product.id == null ? "../product/save" : "../product/update";
            vm.product.goodsId=vm.goodsId;
            Ajax.request({
                type: "POST",
                url: url,
                contentType: "application/json",
                params: JSON.stringify(vm.product),
                successCallback: function (r) {
                    alert('操作成功', function (index) {
                        vm.reload();
                        vm.product.goodsId=0;
                        vm.product.goodsSn = '';
                        vm.product.goodsNumber = 1;
                        vm.product.retailPrice = 1;
                        vm.product.marketPrice = 1;
                        vm.product.goodsSpecificationIds='';
                        vm.product.productCode='';
                        vm.specifications = [],
                        vm.attribute = [],
                        vm.params=[
                            { param : [] , ggArr:[]},
                            { param : [] , ggArr:[]},
                            { param : [] , ggArr:[]}
                        ]
                    });
                }
            });


        },
        del: function (event) {
            let ids = getSelectedRows("#jqGrid");
            if (ids == null) {
                return;
            }

            confirm('确定要删除选中的记录？', function () {
                Ajax.request({
                    type: "POST",
                    url: "../product/delete",
                    contentType: "application/json",
                    params: JSON.stringify(ids),
                    successCallback: function (r) {
                        alert('操作成功', function (index) {
                            vm.reload();
                        });
                    }
                });


            });
        },
        getInfo: function (id) {
            vm.attribute = [];
            Ajax.request({
                url: "../product/info/" + id,
                async: true,
                successCallback: function (r) {
                    vm.getGoodss();
                    vm.product = r.product;
                    vm.goodsId=r.product.goodsId;
                    if(vm.product.goodsSpecificationIds.indexOf('_')>0){
                        let goodsSpecificationIds = vm.product.goodsSpecificationIds.split("_");
                        goodsSpecificationIds.forEach((goodsSpecificationId, index) => {
                            if(goodsSpecificationId.indexOf(',')>0){
                                let specificationIds = goodsSpecificationId.split(",").filter(id => !!id).map(id => Number(id));
                                if (index == 0) {
                                    vm.params[index].param = specificationIds;
                                    if (specificationIds.length > 0) {
                                        vm.attribute.push(1);
                                    }
                                } else if (index == 1) {
                                    vm.params[index].param = specificationIds;
                                    if (specificationIds.length > 0) {
                                        vm.attribute.push(2);
                                    }
                                } else if (index == 2) {
                                    vm.params[index].param = specificationIds;
                                    if (specificationIds.length > 0) {
                                        vm.attribute.push(4);
                                    }
                                }
                            }else {
                               vm.params[index].param=vm.product.goodsSpecificationId
                            }
                      });
                    } else{
                        vm.params[0].param=vm.product.goodsSpecificationIds
                    }
                }
            });
        },
        reload: function (event) {
            vm.showList = true;
            vm.addGoodsNumberCard=false;
            let page = $("#jqGrid").jqGrid('getGridParam', 'page');
            if(typeof(event) != undefined){
            	page= event;
            }
            $("#jqGrid").jqGrid('setGridParam', {
                postData: {'goodsName': vm.q.goodsName},
                page: page
            }).trigger("reloadGrid");
            vm.handleReset('product');
        },
        handleSubmit: function (name) {
            handleSubmitValidate(this, name, function () {
                vm.saveOrUpdate()
            });
        },
        handleReset: function (name) {
            handleResetForm(this, name);
        },
        getGoodss: function () {
            Ajax.request({
                url: "../goods/queryAll/",
                async: true,
                successCallback: function (r) {
                    vm.goodss = r.list;
                }
            });
            
           
        },
        changeAttributes: function(){
        	if(vm.attribute.length==0){
        		return;
        	}
            Ajax.request({
                url: "../goodsspecification/queryAll?goodsId=" + vm.goodsId + "&specificationId=" + vm.attribute[vm.attribute.length-1].id,
                async: true,
                successCallback: function (r) {
                    vm.params[vm.attribute.length-1].ggArr = r.list;
                }
            });
        }
    }
});
function getInfoByinit(goodid){
	 $.ajax({
        type: "get",
        url: "../goods/info/" + goodid,
        async: true,
        success: function(data) {
       	 vm.goods = data.goods;
       	 if(data.goods.name!=null)
       	$("#goodInfo").html("当前商品名称： "+data.goods.name);
        },
    });
}