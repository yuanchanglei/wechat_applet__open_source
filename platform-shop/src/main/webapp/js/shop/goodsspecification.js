$(function () {
    let goodsId = getQueryString("goodsId");
    let url = '../goodsspecification/list';
    if (goodsId) {
        url += '?goodsId=' + goodsId;
        getInfoByinit(goodsId);
    }
   
    vm.goodsId = goodsId;
    $("#jqGrid").Grid({
        url: url,
        colModel: [
            {label: 'id', name: 'id', index: 'id', key: true, hidden: true},
            {label: '商品', name: 'goodsName', index: 'goods_id', width: 80},
            {label: '规格', name: 'specificationName', index: 'specification_id', width: 80},
            {label: '规格说明', name: 'value', index: 'value', width: 80},
            {
                label: '规格图片', name: 'picUrl', index: 'pic_url', width: 80, formatter: function (value) {
                    return transImg(value);
                }
            }]
    });
});

var vm = new Vue({
    el: '#rrapp',
    data: {
        showList: true,
        title: null,
        goodsSpecification: {},
        ruleValidate: {
            name: [
                {required: true, message: '名称不能为空', trigger: 'blur'}
            ]
        },
        q: {
            name: ''
        },
        goodss: [],
        goods:'',
        specifications: []
        },
    methods: {
        getSpecification: function () {
            Ajax.request({
                url: "../specification/queryAll",
                async: true,
                successCallback: function (r) {
                    vm.specifications = r.list;
                }
            });
        },
        getGoodss: function () {
            Ajax.request({
                url: "../goods/queryAll/",
                async: true,
                successCallback: function (r) {
                    vm.goodss = r.list;
                }
            });
        },
        query: function () {
            vm.reload(1);
        },
        add: function () {
            vm.showList = false;
            vm.title = "新增";
            vm.goodsSpecification = {};
            vm.getSpecification();
            vm.getGoodss();
            vm.goodsSpecification.goodsId=vm.goods.id;
        },
        update: function (event) {
            var id = getSelectedRow("#jqGrid");
            if (id == null) {
                return;
            }
            vm.showList = false;
            vm.title = "修改";

            vm.getInfo(id)
        },
        saveOrUpdate: function (event) {
        	if(vm.goodsSpecification.goodsId==""){
        		 alert('商品不能为空！');
        		 return ;
        	}
            var url = vm.goodsSpecification.id == null ? "../goodsspecification/save" : "../goodsspecification/update";
            Ajax.request({
                type: "POST",
                url: url,
                contentType: "application/json",
                params: JSON.stringify(vm.goodsSpecification),
                successCallback: function (r) {
                    alert('操作成功', function (index) {
                        vm.reload();
                    });
                }
            });
        },
        del: function (event) {
            var ids = getSelectedRows("#jqGrid");
            if (ids == null) {
                return;
            }

            confirm('确定要删除选中的记录？', function () {
                Ajax.request({
                    type: "POST",
                    url: "../goodsspecification/delete",
                    contentType: "application/json",
                    params: JSON.stringify(ids),
                    successCallback: function (r) {
                        alert('操作成功', function (index) {
                            vm.reload();
                        });
                    }
                });

            });
        },
        getInfo: function (id) {
            Ajax.request({
                url: "../goodsspecification/info/" + id,
                async: true,
                successCallback: function (r) {
                    vm.goodsSpecification = r.goodsSpecification;
                    vm.getSpecification();
                    vm.getGoodss();
                }
            });
        },
        reload: function (event) {
            vm.showList = true;
            var page = $("#jqGrid").jqGrid('getGridParam', 'page');
            if(typeof(event) != undefined){
            	page= event;
            }
            $("#jqGrid").jqGrid('setGridParam', {
                postData: {'name': vm.q.name},
                page: page
            }).trigger("reloadGrid");
            vm.handleReset('formValidate');
        },
        handleFormatError: function (file) {
            this.$Notice.warning({
                title: '文件格式不正确',
                desc: '文件 ' + file.name + ' 格式不正确，请上传 jpg 或 png 格式的图片。'
            });
        },
        handleMaxSize: function (file) {
            this.$Notice.warning({
                title: '超出文件大小限制',
                desc: '文件 ' + file.name + ' 太大，不能超过 2M。'
            });
        },
        handleSuccess: function (res, file) {
        	this.$set(vm.goodsSpecification,'picUrl',file.response.url)
        },
        eyeImage: function () {
            var url = vm.goodsSpecification.picUrl;
            eyeImage(url);
        },
        handleSubmit: function (name) {
        	
            handleSubmitValidate(this, name, function () {
                vm.saveOrUpdate()
            });
        },
        handleReset: function (name) {
            handleResetForm(this, name);
        }
    }
});
function getInfoByinit(goodid){
	 $.ajax({
         type: "get",
         url: "../goods/info/" + goodid,
         async: true,
         success: function(data) {
        	 vm.goods = data.goods;
        	 if(data.goods.name!=null)
        	$("#goodInfo").html("当前商品名称： "+data.goods.name);
         },
     });
}