$(function () {
    $("#jqGrid").Grid({
        url: '../orderreturn/list',
        colModel: [
			{label: 'id', name: 'id', index: 'id', key: true, hidden: true},
//			{label: '订单id', name: 'orderId', index: 'order_id', width: 80},
			{label: '订单编号', name: 'orderSn', index: 'orderSn', width: 80},
			{label: '退换货类型', name: 'returnType', index: 'returnType', width: 50,formatter: function (value, col, row) {
				if(row.returnType=='1'){
						return "退货";
					}else if(row.returnType=='2'){
						return '换货';
					}else{
						return "退货";
					}
            	}
			},
//			{label: '商品名称', name: 'orderGoodsName', index: 'orderGoodsName',width: 80},
			{label: '联系人', name: 'userName', width:40},
			{label: '联系方式', name: 'mobileNumber', width: 60},
			{label: '退货原因', name: 'returnReason', index: 'return_reason', width: 90},
			{label: '快递信息', name: 'shippingName', index: 'shipping_name', width: 80,formatter: function (value, col, row) {
				if(row.status=='3'||row.status=='4'||row.status=='5'){
						return row.shippingName+':\n'+row.shippingNo;
					}else{
						return '-';
					}
            	}
			},
//			{label: '快递单号', name: 'shippingNo', index: 'shipping_no', width: 80},
			
			{label: '申请时间', name: 'creatTime', index: 'creatTime', width: 70},
//			（0：拒绝申请；1：提交申请；2：同意申请；3：买家已发货；4：卖家已收货；5：退款已完成）
			{label: '状态', name: 'status', index: 'status', width: 60, formatter: function (value) {
				if (value == '-1') {
                    return '已撤回申请';
                }else if (value == '1') {
                    return '待审核';
                } else if (value == '2') {
                    return '待买家发货';
                } else if (value == '3') {
                    return '待商家收货';
                } else if (value == '4') {
                    return '待确认退款';
                }else if (value == '5') {
                    return '已完成-退款';
                }else if (value == '6') {
                    return '已完成-换货';
                }
                else if (value == '0') {
                    return '拒绝申请';
                }
                return '-';
            }
				
			},
			 {
                label: '操作', width: 180, align: 'center', sortable: false, formatter: function (value, col, row) {
                	if(row.status==1){
                		str = '<button class="btn btn-info" onclick="vm.agreeRefund(' + row.id + ','+row.status + ','+row.orderId+')" >'
                			+'同意申请</button>  &nbsp; '+
                			'<button class="btn btn-warning" onclick="vm.rejuectRefund(' + row.id + ','+row.status + ','+row.orderId+')" >'
                			+'拒绝申请</button>';
                	}else if(row.status==3){
                		str = '<button class="btn btn-primary" onclick="vm.confirmGoods(' + row.id + ','+row.status+ ','+row.orderId+')" >'
            			+'<i class="fa fa-file-text-o">&nbsp; 确认收货</button>';
                	}else if(row.status==4){
                		if(row.returnType=='1'){
                			str = '<button class="btn btn-warning" onclick="vm.conformRefund(' + row.id + ','+row.status+ ','+row.orderId+')" >'
                			+'<i class="fa fa-mail-reply">&nbsp; 确认退款</button>';
                		}else{
                			str = '<button class="btn btn-warning" onclick="vm.conformChange(' + row.id + ','+row.status+ ','+row.orderId+')" >'
                			+'<i class="fa fa-mail-reply">&nbsp; 确认换货</button>';
                		}
                	
                	}else{
                		 return '<button class="btn btn-outline btn-info" onclick="vm.lookDetail(' + row.id + ')"><i class="fa fa-info-circle"></i>&nbsp;订单详情</button>';
                	}
                	str = '<button class="btn btn-outline btn-info" onclick="vm.lookDetail(' + row.id + ')"><i class="fa fa-info-circle"></i>&nbsp;订单详情</button> &nbsp;'+str;
                   
                	return str;
                }
            }
//			{label: '拒绝原因', name: 'rejectInfo', index: 'reject_info', width: 80}
//			{label: '更新时间', name: 'updatetime', index: 'updateTime', width: 80}
			]
    });
});

let vm = new Vue({
	el: '#rrapp',
	data: {
        showList: true,
        ShippingContent:false,
        title: null,
        shippings: [],
		orderReturn: {},
		ruleValidate: {
			name: [
				{required: true, message: '名称不能为空', trigger: 'blur'}
			]
		},
		q: {
			orderSn: '',
    		orderGoodsName: '',
    		mobileNumber: '',
    		status: ''
		}
	},
	methods: {
		query: function () {
			vm.reload(1);
		},
		add: function () {
			vm.showList = false;
			vm.title = "新增";
			vm.orderReturn = {};
		},
		update: function (event) {
            let id = getSelectedRow("#jqGrid");
			if (id == null) {
				return;
			}
			vm.showList = false;
            vm.title = "修改";

            vm.getInfo(id)
		},
		rejuectRefund: function (id,status,orderId) {
			 if(status=="1"){
					vm.orderReturn={};
					vm.orderReturn.id = id;
					vm.orderReturn.status ="0";
					vm.orderReturn.orderId = orderId;
					Ajax.request({
					    url: "../orderreturn/agreeRefund",
		                params: JSON.stringify(vm.orderReturn),
		                type: "POST",
					    contentType: "application/json",
		                successCallback: function (r) {
		                    alert('操作成功', function (index) {
		                        vm.reload();
		                    });
		                }
					});
			 }else{
				 alert('当前数据，您无操作权限！')
			 }
		},
		agreeRefund: function (id,status,orderId) {
			 if(status=="1"){
					vm.orderReturn={};
					vm.orderReturn.id = id;
					vm.orderReturn.status ="2";
					vm.orderReturn.orderId = orderId;
					Ajax.request({
					    url: "../orderreturn/agreeRefund",
		                params: JSON.stringify(vm.orderReturn),
		                type: "POST",
					    contentType: "application/json",
		                successCallback: function (r) {
		                    alert('操作成功', function (index) {
		                        vm.reload();
		                    });
		                }
					});
			 }else{
				 alert('当前数据，您无操作权限！')
			 }
		},
		confirmGoods: function (id,status,orderId) {
			 if(status=="3"){
					vm.orderReturn={};
					vm.orderReturn.id = id;
					vm.orderReturn.status ="4";
					vm.orderReturn.orderId = orderId;
					Ajax.request({
					    url: "../orderreturn/confirmGoods",
		                params: JSON.stringify(vm.orderReturn),
		                type: "POST",
					    contentType: "application/json",
		                successCallback: function (r) {
		                        vm.reload();
		                }
					});
			 }else{
				 alert('当前数据，您无操作权限！')
			 }
		},
		conformRefund: function (id,status,orderId) {
			console.log(status);
			 if(status=="4"){
				 	vm.orderReturn={};
					vm.orderReturn.id = id;
					vm.orderReturn.status ="5";
					vm.orderReturn.orderId = orderId;
			       confirm('确定要进行退款？', function () {
						Ajax.request({
						    url: "../orderreturn/conformRefund",
			                params: JSON.stringify(vm.orderReturn),
			                type: "POST",
						    contentType: "application/json",
			                successCallback: function (r) {
			                    alert('退款完成！', function (index) {
			                        vm.reload();
			                    });
			                }
						});
		            });
			 }else{
				 alert('当前数据，您无操作权限！')
			 }
		},
		conformChange:function (id,status,orderId) {
			vm.orderReturn={};
			vm.orderReturn.id=id;
			vm.orderReturn.orderId=orderId;
			vm.ShippingContent=true;
			vm.showList = false;
			vm.orderReturn.ChangeShippingName="";
			vm.orderReturn.ChangeShippingNo="";
			vm.orderReturn.id = id;
			 vm.title = "请填写换货物流信息";
		},
		ShippingContentConfirm:function () {
			var ChangeShippingName = vm.orderReturn.changeShippingName;
			var ChangeShippingNo  = vm.orderReturn.changeShippingNo;
			var flag =true;
			if(ChangeShippingName==""||ChangeShippingName==null){
				flag =false;
				alert("物流名称不能为空！");
			}
			if(ChangeShippingNo==""||ChangeShippingNo==null){
				flag =false;
				alert("物流编码不能为空！");
			}
			if(flag){
				vm.orderReturn.status ="6";
			     Ajax.request({
			    	 	url: "../orderreturn/ShippingContentConfirm",
		                params: JSON.stringify(vm.orderReturn),
		                type: "POST",
					    contentType: "application/json",
		                successCallback: function (r) {
		                    alert('操作成功', function (index) {
		                    	vm.ShippingContent=false;
		                        vm.reload();
		                    });
		                }
					});
			}
		},
		saveOrUpdate: function (event) {
            let url = vm.orderReturn.id == null ? "../orderreturn/save" : "../orderreturn/update";
            Ajax.request({
			    url: url,
                params: JSON.stringify(vm.orderReturn),
                type: "POST",
			    contentType: "application/json",
                successCallback: function (r) {
                    alert('操作成功', function (index) {
                        vm.reload();
                    });
                }
			});
		},
		del: function (event) {
            let ids = getSelectedRows("#jqGrid");
			if (ids == null){
				return;
			}

			confirm('确定要删除选中的记录？', function () {
                Ajax.request({
				    url: "../orderreturn/delete",
                    params: JSON.stringify(ids),
                    type: "POST",
				    contentType: "application/json",
                    successCallback: function () {
                        alert('操作成功', function (index) {
                            vm.reload();
                        });
					}
				});
			});
		},
		getInfo: function(id){
            Ajax.request({
                url: "../orderreturn/info/"+id,
                async: true,
                successCallback: function (r) {
                    vm.orderReturn = r.orderReturn;
                }
            });
		},
		reload: function (event) {
			vm.showList = true;
			vm.ShippingContent = false;
            let page = $("#jqGrid").jqGrid('getGridParam', 'page');
            if(typeof(event) != undefined){
            	page= event;
            }
			$("#jqGrid").jqGrid('setGridParam', {
                postData: {'orderSn': vm.q.orderSn,'orderGoodsName': vm.q.orderGoodsName,
                			'mobileNumber': vm.q.mobileNumber,'status': vm.q.status},
                page: page
            }).trigger("reloadGrid");
            vm.handleReset('formValidate');
		},
        reloadSearch: function() {
            vm.q = {
            		orderSn: '',
            		orderGoodsName: '',
            		mobileNumber: '',
            		status: ''
            }
            vm.reload();
        },
        handleSubmit: function (name) {
            handleSubmitValidate(this, name, function () {
                vm.saveOrUpdate()
            });
        },
        handleReset: function (name) {
            handleResetForm(this, name);
        },
        lookDetail:function (id){
            Ajax.request({
                url: "../orderreturn/info/" + id,
                async: true,
                successCallback: function (r) {
                	vm.orderReturn = r.orderReturn;
                	 $("#orderReturnDetail .tkContent").text("");
                	 $("#orderReturnDetail .xdsj").text(transDate(vm.orderReturn.orderEntity.addTime));
                	 $("#orderReturnDetail .ddbh").text(vm.orderReturn.orderEntity.orderSn);
                	 $("#orderReturnDetail .sfje").text(vm.orderReturn.orderEntity.orderPrice);
                	 $("#orderReturnDetail .zk").text(vm.orderReturn.orderEntity.goodsDiscount);
                	 $("#orderReturnDetail .ytkje").text(vm.orderReturn.orderEntity.goodsPrice);
                	 $("#orderReturnDetail .spxxList").children().remove();
                	 var orderGoodsList =  vm.orderReturn.orderGoodsList;
                	 console.log(orderGoodsList);
                	 for(var i=0;i<orderGoodsList.length;i++){
                		 var orderGoods = orderGoodsList[i];
                		var htmlDiv = '<div class="ivu-input-wrapper ivu-input-type" style="margin-bottom:10px">'
                			+'<div style="float: left;width: 150px"><img class="goodsPic" width="130px; " alt="" src="'+orderGoods.listPicUrl+'"></div>'
                			+'<div style="float: right;width: 250px;"><div class="goodsName">'+orderGoods.goodsName+'</div>'+
                			'<div class="goodsSpecifitionNameValue">'+orderGoods.goodsSpecifitionNameValue+'</div>'+
                			'<div class="number"> x'+orderGoods.number+'</div>'+
                			'<div  class="retailPrice">商品价格：￥'+orderGoods.retailPrice+'</div></div></div>';
                		 $("#orderReturnDetail .spxxList").append(htmlDiv);
                	 }
	               	  openWindow({
	                      title: "退款订单详情",
	                      area: ['470px','98%'],
	                      skin: 'demo-class',
	                      shadeClose :true,
	                      content: jQuery("#orderReturnDetail"),
	                      btn: ['确定'],
	                      btn1: function (index) {
	                          layer.close(index);
	                      }
	                  });
                }
            });
        
        }
        
	},
	 created: function () {
	        let vue = this;
	        Ajax.request({
	            url: "../shipping/queryAll",
	            async: true,
	            successCallback: function (r) {
	                vue.shippings = r.list;
	            }
	        });
	    }
});