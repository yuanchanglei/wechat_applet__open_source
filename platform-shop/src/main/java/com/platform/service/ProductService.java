package com.platform.service;

import com.platform.entity.ProductEntity;

import java.util.List;
import java.util.Map;

/**
 * Service接口
 *
 * @author lipengjun
 * @email 939961241@qq.com
 * @date 2017-08-30 14:31:21
 */
public interface ProductService {

    /**
     * 根据主键查询实体
     *
     * @param id 主键
     * @return 实体
     */
    ProductEntity queryObject(Integer id);

    /**
     * 分页查询
     *
     * @param map 参数
     * @return list
     */
    List<ProductEntity> queryList(Map<String, Object> map);

    /**
     * 分页统计总数
     *
     * @param map 参数
     * @return 总数
     */
    int queryTotal(Map<String, Object> map);

    /**
     * 保存实体
     *
     * @param product 实体
     * @return 保存条数
     */
    int save(ProductEntity product);

    /**
     * 根据主键更新实体
     *
     * @param product 实体
     * @return 更新条数
     */
    int update(ProductEntity product);
    
    /**
     * 增加库存数
     *
     * @param product 实体
     * @return 更新条数
     */
    int addGoodsNumber(Integer id,Integer addNumber);
    
    /**
     * 根据主键删除
     *
     * @param id
     * @return 删除条数
     */
    int delete(Integer id);

    /**
     * 根据主键批量删除
     *
     * @param ids
     * @return 删除条数
     */
    int deleteBatch(Integer[] ids);
    //减掉购买的产品库存
    public void updateProductNumber_cut(int productId,Integer cartNumber); 
    //还原购买的产品库存
    public void updateProductNumber_add(int productId,Integer cartNumber);
    /**
               * 根据参数修改商品库存，和销量_cut 为减库存
     * @param goodsId  商品id
     * @param cartNumber 购物车本次购买的数量
     */
    public void updateGoodsNumber_cut(int goodsId,Integer cartNumber);
    /**
            * 根据参数修改商品库存，和销量_add 为还原库存
	* @param goodsId  商品id
	* @param cartNumber 购物车本次购买的数量
	*/
	public void updateGoodsNumber_add(int goodsId,Integer cartNumber);
	}
