package com.platform.service;

import com.platform.entity.PersonalGoodsEntity;
import com.platform.entity.example.PersonalGoodsExample;

import java.util.List;
import java.util.Map;

/**
 * Service接口
 *
 * @author lipengjun
 * @email 939961241@qq.com
 * @date 2019-07-31 10:44:54
 */
public interface PersonalGoodsService {

    /**
     * 根据主键查询实体
     *
     * @param id 主键
     * @return 实体
     */
    PersonalGoodsEntity queryObject(Long id);

    /**
     * 分页查询
     *
     * @param map 参数
     * @return list
     */
    List<PersonalGoodsEntity> queryList(Map<String, Object> map);

    /**
     * 分页统计总数
     *
     * @param map 参数
     * @return 总数
     */
    int queryTotal(Map<String, Object> map);

    /**
     * 保存实体
     *
     * @param personalGoods 实体
     * @return 保存条数
     */
    int save(PersonalGoodsEntity personalGoods);
    /**
     * 保存实体
     *
     * @param personalGoods 实体
     * @return 保存条数
     */
    int saveBatch(Long[] ids);

    /**
     * 根据主键更新实体
     *
     * @param personalGoods 实体
     * @return 更新条数
     */
    int update(PersonalGoodsEntity personalGoods);

    /**
     * 根据主键删除
     *
     * @param id
     * @return 删除条数
     */
    int delete(Long id);

    /**
     * 根据主键批量删除
     *
     * @param ids
     * @return 删除条数
     */
    int deleteBatch(Long[] ids);

	int deleteByExample(PersonalGoodsExample example);
}
