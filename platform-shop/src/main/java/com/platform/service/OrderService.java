package com.platform.service;

import java.util.List;
import java.util.Map;

import com.platform.entity.GroupBuyingEntity;
import com.platform.entity.OrderEntity;
import com.platform.entity.OrderGoodsEntity;

/**
 * @author lipengjun
 * @email 939961241@qq.com
 * @date 2017-08-13 10:41:09
 */
public interface OrderService {	
	
    OrderEntity queryObject(Integer id);
    OrderEntity queryObjectByOrderSn(String orderSn);
    OrderGoodsEntity queryOrderGoodsObject(Integer orderId,Integer orderGoodsId,Integer productId);
    List<OrderGoodsEntity> queryOrderGoodsByOrderId(Integer orderId);
    List<OrderEntity> queryList(Map<String, Object> map);
    List<GroupBuyingEntity> queryGroupList(Map<String, Object> map);
    List<OrderEntity> queryByAllOrderId(String all_order_id);
    int queryTotal(Map<String, Object> map);
    int queryGroupTotal(Map<String, Object> map);

    int save(OrderEntity order);

    int update(OrderEntity order);

    int delete(Integer id);

    int deleteBatch(Integer[] ids);

    /**
     * 确定收货
     *
     * @param id
     * @return
     */
    int confirm(Integer id);

    int sendGoods(OrderEntity order);
   
    //定时任务调用的，自动发货
    int autoSendGoods(OrderEntity order);
    
    List<OrderEntity> queryOvertimeOrders();
}
