package com.platform.service;

import com.platform.entity.OrderReturnEntity;

import java.util.List;
import java.util.Map;

/**
 * 退款申请表Service接口
 *
 * @author lipengjun
 * @email 939961241@qq.com
 * @date 2019-08-28 21:42:11
 */
public interface OrderReturnService {

    /**
     * 根据主键查询实体
     *
     * @param id 主键
     * @return 实体
     */
    OrderReturnEntity queryObject(Long id);

    /**
     * 分页查询
     *
     * @param map 参数
     * @return list
     */
    List<OrderReturnEntity> queryList(Map<String, Object> map);

    /**
     * 分页统计总数
     *
     * @param map 参数
     * @return 总数
     */
    int queryTotal(Map<String, Object> map);

    /**
     * 保存实体
     *
     * @param orderReturn 实体
     * @return 保存条数
     */
    int save(OrderReturnEntity orderReturn);

    /**
     * 根据主键更新实体
     *
     * @param orderReturn 实体
     * @return 更新条数
     */
    int update(OrderReturnEntity orderReturn);

    /**
     * 根据主键删除
     *
     * @param id
     * @return 删除条数
     */
    int delete(Long id);

    /**
     * 根据主键批量删除
     *
     * @param ids
     * @return 删除条数
     */
    int deleteBatch(Integer[] ids);
}
