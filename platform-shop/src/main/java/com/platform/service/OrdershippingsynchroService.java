package com.platform.service;

import com.platform.entity.OrdershippingsynchroEntity;

import java.util.List;
import java.util.Map;

/**
 * 批量导入订单的发货单号；同步运单号Service接口
 *
 * @author lipengjun
 * @email 939961241@qq.com
 * @date 2020-02-17 19:44:51
 */
public interface OrdershippingsynchroService {

    /**
     * 根据主键查询实体
     *
     * @param id 主键
     * @return 实体
     */
    OrdershippingsynchroEntity queryObject(String orderSn);

    /**
     * 分页查询
     *
     * @param map 参数
     * @return list
     */
    List<OrdershippingsynchroEntity> queryList(Map<String, Object> map);

    /**
     * 分页统计总数
     *
     * @param map 参数
     * @return 总数
     */
    int queryTotal(Map<String, Object> map);

    /**
     * 保存实体
     *
     * @param ordershippingsynchro 实体
     * @return 保存条数
     */
    int save(OrdershippingsynchroEntity ordershippingsynchro);

    /**
     * 根据主键更新实体
     *
     * @param ordershippingsynchro 实体
     * @return 更新条数
     */
    int update(OrdershippingsynchroEntity ordershippingsynchro);

    /**
     * 根据主键删除
     *
     * @param orderSn
     * @return 删除条数
     */
    int delete(String orderSn);

    /**
     * 根据主键批量删除
     *
     * @param orderSns
     * @return 删除条数
     */
    int deleteBatch(String[] orderSns);
    
    
    /**
     * 根据订单号更新状态
     *
     * @param ordershippingsynchro 实体
     * @return 更新条数
     */
    int updateStatus(OrdershippingsynchroEntity ordershippingsynchro);
}
