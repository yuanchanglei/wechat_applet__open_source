package com.platform.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

import com.platform.dao.CampaignMapper;
import com.platform.entity.CampaignEntity;
import com.platform.service.CampaignService;

/**
 * 店铺活动表Service实现类
 *
 * @author lipengjun
 * @email 939961241@qq.com
 * @date 2020-02-29 19:14:47
 */
@Service("campaignService")
public class CampaignServiceImpl implements CampaignService {
    @Autowired
    private CampaignMapper campaignMapper;

    @Override
    public CampaignEntity queryObject(Integer id) {
        return campaignMapper.selectByPrimaryKey(id);
    }

    @Override
    public List<CampaignEntity> queryList(Map<String, Object> map) {
        return campaignMapper.queryList(map);
    }

    @Override
    public int queryTotal(Map<String, Object> map) {
        return campaignMapper.queryTotal(map);
    }

    @Override
    public int save(CampaignEntity campaign) {
        return campaignMapper.insertSelective(campaign);
    }

    @Override
    public int update(CampaignEntity campaign) {
        return campaignMapper.updateByPrimaryKeySelective(campaign);
    }

    @Override
    public int delete(Integer id) {
        return campaignMapper.deleteByPrimaryKey(id);
    }

    @Override
    public int deleteBatch(Integer[] ids) {
        return campaignMapper.deleteBatch(ids);
    }
}
