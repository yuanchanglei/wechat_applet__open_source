package com.platform.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

import com.platform.dao.OrderReturnMapper;
import com.platform.entity.OrderReturnEntity;
import com.platform.service.OrderReturnService;

/**
 * 退款申请表Service实现类
 *
 * @author lipengjun
 * @email 939961241@qq.com
 * @date 2019-08-28 21:42:11
 */
@Service("orderReturnService")
public class OrderReturnServiceImpl implements OrderReturnService {
    @Autowired
    private OrderReturnMapper orderReturnMapper;

    @Override
    public OrderReturnEntity queryObject(Long id) {
        return orderReturnMapper.selectByPrimaryKey(id);
    }

    @Override
    public List<OrderReturnEntity> queryList(Map<String, Object> map) {
        return orderReturnMapper.queryList(map);
    }

    @Override
    public int queryTotal(Map<String, Object> map) {
        return orderReturnMapper.queryTotal(map);
    }

    @Override
    public int save(OrderReturnEntity orderReturn) {
        return orderReturnMapper.insertSelective(orderReturn);
    }

    @Override
    public int update(OrderReturnEntity orderReturn) {
        return orderReturnMapper.updateByPrimaryKeySelective(orderReturn);
    }

    @Override
    public int delete(Long id) {
        return orderReturnMapper.deleteByPrimaryKey(id);
    }

    @Override
    public int deleteBatch(Integer[] ids) {
        return orderReturnMapper.deleteBatch(ids);
    }
}
