package com.platform.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Map;

import com.platform.dao.PersonalGoodsMapper;
import com.platform.entity.PersonalGoodsEntity;
import com.platform.entity.SysUserEntity;
import com.platform.entity.example.PersonalGoodsExample;
import com.platform.service.PersonalGoodsService;
import com.platform.utils.ShiroUtils;

/**
 * Service实现类
 *
 * @author lipengjun
 * @email 939961241@qq.com
 * @date 2019-07-31 10:44:54
 */
@Service("personalGoodsService")
public class PersonalGoodsServiceImpl implements PersonalGoodsService {
    @Autowired
    private PersonalGoodsMapper personalGoodsMapper;

    @Override
    public PersonalGoodsEntity queryObject(Long id) {
        return personalGoodsMapper.selectByPrimaryKey(id);
    }

    @Override
    public List<PersonalGoodsEntity> queryList(Map<String, Object> map) {
        return personalGoodsMapper.queryList(map);
    }

    @Override
    public int queryTotal(Map<String, Object> map) {
        return personalGoodsMapper.queryTotal(map);
    }

    @Override
    public int save(PersonalGoodsEntity personalGoods) {
        return personalGoodsMapper.insertSelective(personalGoods);
    }
    @Override
    public int saveBatch(Long[] ids) {
    	SysUserEntity sysUserEntity= ShiroUtils.getUserEntity();
    	 int result = 0;
    	 PersonalGoodsEntity personalGoods = new PersonalGoodsEntity();
    	 personalGoods.setSysUserId(sysUserEntity.getUserId());
    	 personalGoods.setMerchantId(sysUserEntity.getMerchantId());
    	 personalGoods.setAddTime(new Date());
         for (Long id : ids) {
     		 personalGoods.setGoodsId(id);
             result +=personalGoodsMapper.insertSelective(personalGoods);
         }
         return result;
    }
    @Override
    public int update(PersonalGoodsEntity personalGoods) {
        return personalGoodsMapper.updateByPrimaryKeySelective(personalGoods);
    }

    @Override
    public int delete(Long id) {
        return personalGoodsMapper.deleteByPrimaryKey(id);
    }

    @Override
    public int deleteBatch(Long[] ids) {
        return personalGoodsMapper.deleteBatch(ids);
    }
    
    @Override
    public int deleteByExample(PersonalGoodsExample example) {
        return personalGoodsMapper.deleteByExample(example);
    }
}
