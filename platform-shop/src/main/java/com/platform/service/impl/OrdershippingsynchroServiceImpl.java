package com.platform.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

import com.platform.dao.OrdershippingsynchroMapper;
import com.platform.entity.OrdershippingsynchroEntity;
import com.platform.service.OrdershippingsynchroService;

/**
 * 批量导入订单的发货单号；同步运单号Service实现类
 *
 * @author lipengjun
 * @email 939961241@qq.com
 * @date 2020-02-17 19:44:51
 */
@Service("ordershippingsynchroService")
public class OrdershippingsynchroServiceImpl implements OrdershippingsynchroService {
    @Autowired
    private OrdershippingsynchroMapper ordershippingsynchroMapper;

    @Override
    public OrdershippingsynchroEntity queryObject(String orderSn) {
        return ordershippingsynchroMapper.selectByPrimaryKey(orderSn);
    }

    @Override
    public List<OrdershippingsynchroEntity> queryList(Map<String, Object> map) {
        return ordershippingsynchroMapper.queryList(map);
    }

    @Override
    public int queryTotal(Map<String, Object> map) {
        return ordershippingsynchroMapper.queryTotal(map);
    }

    @Override
    public int save(OrdershippingsynchroEntity ordershippingsynchro) {
        return ordershippingsynchroMapper.insertSelective(ordershippingsynchro);
    }

    @Override
    public int update(OrdershippingsynchroEntity ordershippingsynchro) {
        return ordershippingsynchroMapper.updateByPrimaryKeySelective(ordershippingsynchro);
    }

    @Override
    public int delete(String orderSn) {
        return ordershippingsynchroMapper.deleteByPrimaryKey(orderSn);
    }

    @Override
    public int deleteBatch(String[] orderSns) {
        return ordershippingsynchroMapper.deleteBatch(orderSns);
    }

	@Override
	public int updateStatus(OrdershippingsynchroEntity ordershippingsynchro) {
		// TODO Auto-generated method stub
		return ordershippingsynchroMapper.updateStatus(ordershippingsynchro);
	}
}
