package com.platform.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

import com.platform.dao.CampaignGoodsMapper;
import com.platform.entity.CampaignGoodsEntity;
import com.platform.entity.example.CampaignGoodsExample;
import com.platform.service.CampaignGoodsService;

/**
 * 活动关联商品Service实现类
 *
 * @author lipengjun
 * @email 939961241@qq.com
 * @date 2020-02-29 19:14:48
 */
@Service("campaignGoodsService")
public class CampaignGoodsServiceImpl implements CampaignGoodsService {
    @Autowired
    private CampaignGoodsMapper campaignGoodsMapper;

    @Override
    public CampaignGoodsEntity queryObject(Integer id) {
        return campaignGoodsMapper.selectByPrimaryKey(id);
    }

    @Override
    public List<CampaignGoodsEntity> queryList(Map<String, Object> map) {
        return campaignGoodsMapper.queryList(map);
    }

    @Override
    public int queryTotal(Map<String, Object> map) {
        return campaignGoodsMapper.queryTotal(map);
    }

    @Override
    public int save(CampaignGoodsEntity campaignGoods) {
        return campaignGoodsMapper.insertSelective(campaignGoods);
    }

    @Override
    public int update(CampaignGoodsEntity campaignGoods) {
        return campaignGoodsMapper.updateByPrimaryKeySelective(campaignGoods);
    }

    @Override
    public int delete(Integer id) {
        return campaignGoodsMapper.deleteByPrimaryKey(id);
    }

    @Override
    public int deleteBatch(Integer[] ids) {
        return campaignGoodsMapper.deleteBatch(ids);
    }

	@Override
	public int deleteByExample(CampaignGoodsExample example) {
		// TODO Auto-generated method stub
		return campaignGoodsMapper.deleteByExample(example);
	}
}
