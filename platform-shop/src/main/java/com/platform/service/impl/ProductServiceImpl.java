package com.platform.service.impl;

import com.platform.dao.GoodsSpecificationDao;
import com.platform.entity.GoodsSpecificationEntity;
import com.platform.utils.BeanUtils;
import com.platform.utils.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.platform.dao.ProductDao;
import com.platform.entity.ProductEntity;
import com.platform.service.ProductService;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service实现类
 *
 * @author lipengjun
 * @email 939961241@qq.com
 * @date 2017-08-30 14:31:21
 */
@Service("productService")
public class ProductServiceImpl implements ProductService {
    @Autowired
    private ProductDao productDao;
    @Autowired
    private GoodsSpecificationDao goodsSpecificationDao;

    @Override
    public ProductEntity queryObject(Integer id) {
        return productDao.queryObject(id);
    }

    @Override
    public List<ProductEntity> queryList(Map<String, Object> map) {
        List<ProductEntity> list = productDao.queryList(map);

        List<ProductEntity> result = new ArrayList<>();
        //翻译产品规格
        if (null != list && list.size() > 0) {
            for (ProductEntity item : list) {
                String specificationIds = item.getGoodsSpecificationIds();
                String specificationValue = "";
                if (!StringUtils.isNullOrEmpty(specificationIds)) {
                    String[] arr = specificationIds.split("_");

                    for (String goodsSpecificationId : arr) {
                        GoodsSpecificationEntity entity = goodsSpecificationDao.queryObject(goodsSpecificationId);
                        if (null != entity) {
                            specificationValue += entity.getValue() + "；";
                        }
                    }
                }
                item.setSpecificationValue(item.getGoodsName() + " " + specificationValue);
                result.add(item);
            }
        }
        return result;
    }

    @Override
    public int queryTotal(Map<String, Object> map) {
        return productDao.queryTotal(map);
    }

    @Override
    @Transactional
    public int save(ProductEntity product) {
        int result = 0;
        int goodsId =product.getGoodsId();
        String goodsSpecificationIds = product.getGoodsSpecificationIds();
        if (!StringUtils.isNullOrEmpty(goodsSpecificationIds)) {
            String[] goodsSpecificationIdArr = goodsSpecificationIds.split("_");
            if(goodsSpecificationIdArr.length==2) {
            	 for (int i = 0; i < goodsSpecificationIdArr.length - 1; i++) {
                     String[] oneId = goodsSpecificationIdArr[i].split(",");
                     String[] twoId = goodsSpecificationIdArr[i + 1].split(",");
                     for (int j = 0; j < oneId.length; j++) {
                         for (int k = 0; k < twoId.length; k++) {
                             String strGoodsSpecificationIds = null;
                             if (StringUtils.isNullOrEmpty(oneId[j]) || StringUtils.isNullOrEmpty(twoId[k])){
                                 continue;
                             }
                             strGoodsSpecificationIds = oneId[j] + "_" + twoId[k];
                             product.setGoodsSpecificationIds(strGoodsSpecificationIds);
                             ProductEntity entity = new ProductEntity();
                             BeanUtils.copyProperties(product, entity);
                             result += productDao.save(entity);
                         }
                     }
                 }
            }else if(goodsSpecificationIdArr.length==1) {
            	String goodsSpecificationId[]=goodsSpecificationIdArr[0].split(",");
            	for (int i = 0; i < goodsSpecificationId.length; i++) {
					String string = goodsSpecificationId[i];
					 product.setGoodsSpecificationIds(string);
                     ProductEntity entity = new ProductEntity();
                     BeanUtils.copyProperties(product, entity);
                     result += productDao.save(entity);
				}
            }
           //修改当前商品库存信息
            this.updateGoodsNumber(goodsId);
         // 修改当前商品最低零售价
           	this.updateGoodsRetailPrice(goodsId);
        }
        return result;
    }

    @Override
    public int update(ProductEntity product) {
    	int goodsId =product.getGoodsId();
        if (StringUtils.isNullOrEmpty(product.getGoodsSpecificationIds())){
            product.setGoodsSpecificationIds("");
        }
        int result =  productDao.update(product);
        
        // 修改当前商品库存信息
        this.updateGoodsNumber(goodsId);
        // 修改当前商品最低零售价
       	this.updateGoodsRetailPrice(goodsId);
        return result;
    }
    
    @Override
    public int addGoodsNumber(Integer id,Integer addNumber) {
    	ProductEntity product = this.queryObject(id);
    	int goodsId =product.getGoodsId();
        if (StringUtils.isNullOrEmpty(product.getGoodsSpecificationIds())){
            product.setGoodsSpecificationIds("");
        }
        // 修改产品库存
        Integer newGoodsNumber = product.getGoodsNumber().intValue()+addNumber.intValue();
        product.setGoodsNumber(newGoodsNumber);
        int result =  productDao.update(product);
        // 修改当前商品库存信息
        this.updateGoodsNumber(goodsId);
        // 修改当前商品最低零售价
       	this.updateGoodsRetailPrice(goodsId);
        return result;
    }

    @Override
    public int delete(Integer id) {
    	ProductEntity productEntity=productDao.queryObject(id);
    	int resultNum = productDao.delete(id);
    	//修改当前商品库存信息
    	this.updateGoodsNumber(productEntity.getGoodsId());
    	// 修改当前商品最低零售价
    	this.updateGoodsRetailPrice(productEntity.getGoodsId());
        return resultNum;
    }

    @Override
    public int deleteBatch(Integer[] ids) {
    	ProductEntity productEntity=productDao.queryObject(ids[0]);
    	int resultNum = productDao.deleteBatch(ids);
    	//修改当前商品库存信息
    	this.updateGoodsNumber(productEntity.getGoodsId());
    	// 修改当前商品最低零售价
    	this.updateGoodsRetailPrice(productEntity.getGoodsId());
        return resultNum;
    }
    
    private int updateGoodsNumber(Integer goodsId) {
    	return productDao.updateGoodsNumber(goodsId);
    }
    private int updateGoodsRetailPrice(Integer goodsId) {
       	return productDao.updateGoodsRetailPrice(goodsId);
    }
    //减掉购买的产品库存
    public void updateProductNumber_cut(int productId,Integer cartNumber) {
    	productDao.updateProductNumber_cut(productId,cartNumber);
    }
    //还原购买的产品库存
    public void updateProductNumber_add(int productId,Integer cartNumber) {
        productDao.updateProductNumber_add(productId,cartNumber);
    }
    /**
     * 根据参数修改商品库存，和销量_cut 为减库存
     * @param goodsId  商品id
     * @param cartNumber 购物车本次购买的数量
     */
    public void updateGoodsNumber_cut(int goodsId,Integer cartNumber) {
        productDao.updateGoodsNumber_cut(goodsId,cartNumber);
    }
    /**
	     * 根据参数修改商品库存，和销量_add 为还原库存
	* @param goodsId  商品id
	* @param cartNumber 购物车本次购买的数量
	*/
	public void updateGoodsNumber_add(int goodsId,Integer cartNumber) {
		productDao.updateGoodsNumber_add(goodsId,cartNumber);
	}
    
}
