package com.platform.controller;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.platform.entity.OrderEntity;
import com.platform.entity.OrderGoodsEntity;
import com.platform.entity.OrderReturnEntity;
import com.platform.entity.UserEntity;
import com.platform.service.OrderGoodsService;
import com.platform.service.OrderReturnService;
import com.platform.service.OrderService;
import com.platform.service.UserService;
import com.platform.util.wechat.WSDLUtilsForShop;
import com.platform.util.wechat.WechatRefundApiResult;
import com.platform.util.wechat.WechatUtil;
import com.platform.utils.PageUtils;
import com.platform.utils.Query;
import com.platform.utils.R;
import com.platform.utils.ResourceUtil;

/**
 * 退款申请表Controller
 *
 * @author lipengjun
 * @email 939961241@qq.com
 * @date 2019-08-28 21:42:11
 */
@RestController
@RequestMapping("orderreturn")
public class OrderReturnController {

	@Autowired
    private OrderService orderService;
    @Autowired
    private OrderReturnService orderReturnService;
    @Autowired
	private OrderGoodsService orderGoodsService;
    @Autowired
    private UserService userService;
    /**
     * 查看列表
     */
    @RequestMapping("/list")
    @RequiresPermissions("orderreturn:list")
    public R list(@RequestParam Map<String, Object> params) {
        //查询列表数据
        Query query = new Query(params);

        List<OrderReturnEntity> orderReturnList = orderReturnService.queryList(query);
        int total = orderReturnService.queryTotal(query);

        PageUtils pageUtil = new PageUtils(orderReturnList, total, query.getLimit(), query.getPage());

        return R.ok().put("page", pageUtil);
    }

    /**
     * 查看信息
     */
    @RequestMapping("/info/{id}")
    public R info(@PathVariable("id") Long id) {
        OrderReturnEntity orderReturn = orderReturnService.queryObject(id);
        OrderEntity orderEntity = orderService.queryObject(orderReturn.getOrderId());
        Map<String, Object> params =new HashMap<String, Object>();
        params.put("orderId", orderEntity.getId());
        if(orderReturn.getOrderGoodsId()!=null) {
        	 params.put("orderGoodsId", orderReturn.getOrderGoodsId());
        }
        List<OrderGoodsEntity> orderGoodsList=  orderGoodsService.queryList(params);
        BigDecimal goodsTotalPrice = BigDecimal.ZERO;
        if(orderReturn.getOrderGoodsId()!=null) {
       	 for(OrderGoodsEntity orderGoodsEntity :orderGoodsList ) {
       		 goodsTotalPrice = goodsTotalPrice
   				  .add(orderGoodsEntity.getRetailPrice().multiply(new  BigDecimal(orderGoodsEntity.getNumber())));       	 
       	 }
       //计算商品价格 --- 要算上折扣
		    String goodsDiscont = orderEntity.getGoodsDiscount();
		    if(goodsDiscont!=null&&!"1".equals(goodsDiscont)) {
		    	goodsTotalPrice = goodsTotalPrice.multiply(new  BigDecimal(goodsDiscont)) ;
			  }
       	orderEntity.setGoodsPrice(goodsTotalPrice);
       }
        
        orderReturn.setOrderEntity(orderEntity);
        orderReturn.setOrderGoodsList(orderGoodsList);
        return R.ok().put("orderReturn", orderReturn);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @RequiresPermissions("orderreturn:save")
    public R save(@RequestBody OrderReturnEntity orderReturn) {
        orderReturnService.save(orderReturn);
        return R.ok();
    }

    /**
     * 同意退货申请审核
     */
    @RequestMapping("/agreeRefund")
    @RequiresPermissions("orderreturn:update")
    public R agreeRefund(@RequestBody OrderReturnEntity orderReturn) {
    	Integer status = orderReturn.getStatus();
    	//更新订单状态
    	Integer orderId = orderReturn.getOrderId();
    	OrderEntity orderEntity = new OrderEntity();
    	orderEntity.setId(orderId);
    	if(status==2) {
    		orderEntity.setOrderStatus(502);//订单状态为 售后中
    	}else {
    		orderEntity.setOrderStatus(301);//退货申请被拒，恢复到已确认收货阶段
    	}
        orderService.update(orderEntity);
        //更新退货申请表
        orderReturnService.update(orderReturn);
        return R.ok();
    }
    /**
     * 确认收货
     */
    @RequestMapping("/confirmGoods")
    @RequiresPermissions("orderreturn:update")
    public R confirmGoods(@RequestBody OrderReturnEntity orderReturn) {
    	 //更新退货申请表
        orderReturnService.update(orderReturn);
     	orderReturn  =orderReturnService.queryObject(Long.parseLong(orderReturn.getId()+""));
    	Integer orderId = orderReturn.getOrderId();
    	OrderEntity orderEntity = new OrderEntity();
    	orderEntity.setId(orderId);
    	if("1".equals(orderReturn.getReturnType())) {
    		orderEntity.setOrderStatus(504);
    	}else {
    		orderEntity.setOrderStatus(505);
    	}
    	
    	
        orderService.update(orderEntity);
    	
       
        return R.ok();
    }
    /**
     * 退货
     */
    @RequestMapping("/conformRefund")
    @RequiresPermissions("orderreturn:update")
    public R conformRefund(@RequestBody OrderReturnEntity orderReturn) {
    	String flag = this.cancelOrder(orderReturn);
    	if(flag.equals("success")) {
    		//退款完成后更新状态；
        	orderReturnService.update(orderReturn);
        	return R.ok("退款成功");
    	}else {
    		if(flag.equals("fail")) {
    			 return R.error("退款失败");
    		}else {
    			 return R.error(flag);
    		}
    			
    	}
    	
       
    }
    /**
     * 换货
     */
    @RequestMapping("/ShippingContentConfirm")
    @RequiresPermissions("orderreturn:update")
    public R ShippingContentConfirm(@RequestBody OrderReturnEntity orderReturn) {
    	Integer orderId = orderReturn.getOrderId();
    	OrderEntity orderEntity = new OrderEntity();
    	orderEntity.setId(orderId);
    	orderEntity.setOrderStatus(402);
        orderService.update(orderEntity);
    	orderReturnService.update(orderReturn);
    	//更新订单商品表
    	 orderReturn  =orderReturnService.queryObject(Long.parseLong(orderReturn.getId()+""));
    	 Integer orderGoodsId =  orderReturn.getOrderGoodsId();
    	 if(orderGoodsId!=null) {
    		 OrderGoodsEntity orderGoods = new OrderGoodsEntity();
    		 orderGoods.setId(orderGoodsId);
    		 orderGoods.setIsReturn(3);//2已退货，3 已换货
    		 orderGoodsService.update(orderGoods); 
    	 }
    		 
        	return R.ok();
    }
    /**
     * 删除
     */
    @RequestMapping("/delete")
    @RequiresPermissions("orderreturn:delete")
    public R delete(@RequestBody Integer[] ids) {
        orderReturnService.deleteBatch(ids);

        return R.ok();
    }

    /**
     * 查看所有列表
     */
    @RequestMapping("/queryAll")
    public R queryAll(@RequestParam Map<String, Object> params) {

        List<OrderReturnEntity> list = orderReturnService.queryList(params);

        return R.ok().put("list", list);
    }
    
    /**
     * 退货退款
     * @param orderId
     * @param orderGoodsId
     * @param productId
     * @return
     */
    public String cancelOrder(OrderReturnEntity orderReturn) {
    	Integer orderId = orderReturn.getOrderId();
    	orderReturn  =orderReturnService.queryObject(Long.parseLong(orderReturn.getId()+""));
    	Integer orderGoodsId = orderReturn.getOrderGoodsId();

        try {
            OrderEntity orderEntity = orderService.queryObject(orderId);
            
            List<OrderEntity> orders = orderService.queryByAllOrderId(orderEntity.getAll_order_id());
            //没有具体产品信息，则表明需要退整个订单
            if(orderGoodsId==null) {
            	  BigDecimal allPrice = BigDecimal.ZERO;
                  for(OrderEntity o : orders) {
                  	allPrice = allPrice.add(o.getAll_price());
                  }
               // 需要退款
                  if (orderEntity.getPayStatus() == 2) {
                  	//只退商品的价格
                	  WechatRefundApiResult result = WechatUtil.wxRefund(orderEntity.getAll_order_id().toString(),
                      		allPrice.doubleValue(), orderEntity.getAll_price().doubleValue());
                      //测试修改金额
//                      WechatRefundApiResult result = WechatUtil.wxRefund(orderVo.getId().toString(), 0.01d, 0.01d);
                      if (result.getResult_code().equals("SUCCESS")||"订单已全额退款".equals(result.getErr_code_des())) {
                          if (orderEntity.getOrderStatus() == 504) {
                        	  orderEntity.setOrderStatus(402);//完成订单（已全部退款，完成订单）
                          }
                        	//查询当前订单下是否是全部退货完成
							/*
							 * Map<String, Object> params =new HashMap<String, Object>();
							 * params.put("orderId", orderEntity.getId()); params.put("isReturn",
							 * 0);//是否完成退货。0：未退货。1：已退货 List<OrderGoodsEntity> orderGoodsList=
							 * orderGoodsService.queryList(params); if(orderGoodsList.size()==0) {
							 * orderEntity.setOrderStatus(402);//完成订单（已全部退款，完成订单） }else {
							 * orderEntity.setOrderStatus(403);//部分商品退货 }
							 */
                          orderEntity.setPayStatus(4);
                          //记录退款金额和时间
 						 orderEntity.setRefundTime(new Date());
 						 orderEntity.setRefundPrice(allPrice);
                          orderService.update(orderEntity);
                          //修改订单商品的状态为退货状态
                          Map<String, Object> params =new HashMap<String, Object>();
                          params.put("orderId", orderEntity.getId());
                          List<OrderGoodsEntity> list=  orderGoodsService.queryList(params);
                          for(OrderGoodsEntity orderGoods : list) {
                        	  orderGoods.setIsReturn(2);
                        	  orderGoodsService.updateReturnStatus(orderGoods);
						  }
                          if(list.size()<20) {//对查下的list条数做限制。防止意外情况，导致查询出所有的商品
                        	  if(ResourceUtil.getConfigByName("sys.demo").equals("1")){
                                  //演示环境不需要注册账号
                              }else {
                            	  orderEntity = orderService.queryObject(orderEntity.getId());
                                  UserEntity user = userService.queryObject(orderEntity.getUserId());
                                  String type ="Return";
                                  WSDLUtilsForShop.OrderSynchronization(orderEntity,list,user,type,null);
                              }
                		  }
                          //更新优惠券状态和实际
						/*
						 * UserCouponVo uc = new UserCouponVo(); uc.setId(orderEntity.getCouponId());
						 * uc.setCoupon_status(1); uc.setUsed_time(null);
						 * userCouponService.updateCouponStatus(uc);
						 */
                          
						/*
						 * //去掉订单成功成立分润退还 try { orderService.cancelFx(orderVo.getId(),
						 * orderVo.getPay_time(), orderVo.getAll_price().multiply(new
						 * BigDecimal("100")).intValue()); }catch(Exception e) {
						 * System.out.println("================取消订单返还分润开始================");
						 * e.printStackTrace();
						 * System.out.println("================取消订单返还分润开始================"); } //还原库存
						 * Map<String, Object> orderGoodsParam = new HashMap<String, Object>();
						 * orderGoodsParam.put("order_id", orderVo.getId()); //订单的商品 List<OrderGoodsVo>
						 * orderGoodsList = orderGoodsService.queryList(orderGoodsParam);
						 * for(OrderGoodsVo orderGoodVo:orderGoodsList) { //还原产品的库存信息
						 * productService.updateProductNumber_add(orderGoodVo.getProduct_id(),
						 * orderGoodVo.getNumber()); //更新商品的库存以及销量
						 * productService.updateGoodsNumber_add(orderGoodVo.getGoods_id(),
						 * orderGoodVo.getNumber()); }
						 */
                          
                          //更新退货申请
                      	orderReturn.setStatus(5);
                      	orderReturnService.update(orderReturn);
                          return "success";
                      } else {
                    	  System.err.println(result.getErr_code_des());
                    	  return result.getErr_code_des();
                      }
                  }
            }else {
            	//此处用于退单个商品
            	//总订单的价格
            	BigDecimal allPrice = BigDecimal.ZERO;
                for(OrderEntity o : orders) {
                	allPrice = allPrice.add(o.getAll_price());
                }
              //获取单个商品的金额。
            	OrderGoodsEntity  orderGoodsEntity= orderGoodsService.queryObject(orderGoodsId);
            	BigDecimal  orderGoodsPrice = BigDecimal.ZERO;
        	  //计算商品价格 --- 要算上折扣
			    String goodsDiscont = orderEntity.getGoodsDiscount();
            	orderGoodsPrice = orderGoodsEntity.getRetailPrice().multiply(new BigDecimal(orderGoodsEntity.getNumber()));
            	if(goodsDiscont!=null&&!"1".equals(goodsDiscont)) {
            		orderGoodsPrice =orderGoodsPrice.multiply(new  BigDecimal(goodsDiscont)) ;
				  }
            	if (orderEntity.getPayStatus() == 2) {
                	//只退商品的价格
              	  	WechatRefundApiResult result = WechatUtil.wxRefund(orderEntity.getAll_order_id().toString(),
                    		allPrice.doubleValue(), orderGoodsPrice.doubleValue());
              	  	if (result.getResult_code().equals("SUCCESS")) {
                      if (orderEntity.getOrderStatus() == 504) {
                    	 //修改订单商品的状态为退货状态
                         orderGoodsEntity.setIsReturn(2);
                    	 orderGoodsService.updateReturnStatus(orderGoodsEntity);
                    	 
                	  	//查询当前订单下是否是全部退货完成
						 Map<String, Object> params =new HashMap<String, Object>();
						 params.put("orderId", orderEntity.getId()); 
						 params.put("isReturn", "0");//是否完成退货。0：未退货。1：申请退货，2已退款，3已换货
						 List<OrderGoodsEntity> orderGoodsList = orderGoodsService.queryList(params); 
						 List<OrderGoodsEntity> list = new ArrayList<OrderGoodsEntity>();
						 if(orderGoodsList.size()==0) {
							 orderEntity.setOrderStatus(402);//完成订单（已全部退款，完成订单）
							 orderEntity.setPayStatus(4);
							 list.add(orderGoodsEntity);
						 }else {
							 orderEntity.setOrderStatus(403);//部分商品退货
							 //部分退货，需要将当前退货的商品记录下来，用于退货订单的同步
							 list.add(orderGoodsEntity);
						 }
						 //记录退款金额和时间
						 orderEntity.setRefundTime(new Date());
						 if(orderEntity.getRefundPrice()!=null) {
							 orderEntity.setRefundPrice(orderEntity.getRefundPrice().add(orderGoodsPrice));
						 }else {
							 orderEntity.setRefundPrice(orderGoodsPrice);

						 }
                         orderService.update(orderEntity);
                        
                   	   
                         if(list.size()<20) {//对查下的list条数做限制。防止意外情况，导致查询出所有的商品
                       	   if(ResourceUtil.getConfigByName("sys.demo").equals("1")){
                                 //演示环境不需要注册账号
                             }else {
                           	  orderEntity = orderService.queryObject(orderEntity.getId());
                                 UserEntity user = userService.queryObject(orderEntity.getUserId());
                                 String type ="Return";
                                 WSDLUtilsForShop.OrderSynchronization(orderEntity,list,user,type,orderGoodsEntity.getGoodsSn());
                             }
               		  }
                      }
                      //更新退货申请
                      orderReturn.setStatus(5);
                      orderReturnService.update(orderReturn);
                      return "success";
              	  	}else {
              	  		System.err.println(result.getErr_code_des());
              	  		return result.getErr_code_des();
              	  	}
            	
            	}
            }
        } catch (Exception e) {
            e.printStackTrace();
            return "fail";
        }
        return "success";
    }

}
