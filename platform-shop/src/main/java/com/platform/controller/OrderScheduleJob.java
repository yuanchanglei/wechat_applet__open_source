package com.platform.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.platform.entity.OrderEntity;
import com.platform.entity.OrderGoodsEntity;
import com.platform.service.OrderGoodsService;
import com.platform.service.OrderService;
import com.platform.service.ProductService;
//	定时执行，用于扫描过期订单，并进行关闭处理。
@Component("OrderScheduleJob")
public class OrderScheduleJob {
	 @Autowired
	    private OrderService orderService;
	 @Autowired
	    private OrderGoodsService orderGoodsService;
		@Autowired
		private ProductService productService;
	/*
	 * 订单扫描，将过期订单进行关闭
	 */
	  public void OrderScan() {
		  List<OrderEntity> list = orderService.queryOvertimeOrders();
		  if(list.size()!=0) {
			  for(int i=0;i<list.size();i++) {
				  OrderEntity orderEntity =  orderService.queryObject(list.get(i).getId());
				  orderEntity.setOrderStatus(101);
	              orderService.update(orderEntity);
				  Map<String, Object> orderGoodsParam = new HashMap<String, Object>();
	                orderGoodsParam.put("orderId", orderEntity.getId());
	                //订单的商品
	                List<OrderGoodsEntity> orderGoodsList = orderGoodsService.queryList(orderGoodsParam);
	                for(OrderGoodsEntity orderGoodsEntity:orderGoodsList) {
	                    //还原产品的库存信息
	                	productService.updateProductNumber_add(orderGoodsEntity.getProductId(), orderGoodsEntity.getNumber());
						//更新商品的库存以及销量
	                	productService.updateGoodsNumber_add(orderGoodsEntity.getGoodsId(), orderGoodsEntity.getNumber());
	                }
			  }
		  }
	  }
	  
}
