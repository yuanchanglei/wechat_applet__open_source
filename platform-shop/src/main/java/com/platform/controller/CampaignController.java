package com.platform.controller;

import java.util.List;
import java.util.Map;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.platform.entity.CampaignEntity;
import com.platform.service.CampaignService;
import com.platform.utils.PageUtils;
import com.platform.utils.Query;
import com.platform.utils.R;

/**
 * 店铺活动表Controller
 *
 * @author lipengjun
 * @email 939961241@qq.com
 * @date 2020-02-29 19:14:47
 */
@RestController
@RequestMapping("campaign")
public class CampaignController {
    @Autowired
    private CampaignService campaignService;
    /**
     * 查看列表
     */
    @RequestMapping("/list")
    @RequiresPermissions("campaign:list")
    public R list(@RequestParam Map<String, Object> params) {
        //查询列表数据
        Query query = new Query(params);

        List<CampaignEntity> campaignList = campaignService.queryList(query);
        int total = campaignService.queryTotal(query);

        PageUtils pageUtil = new PageUtils(campaignList, total, query.getLimit(), query.getPage());

        return R.ok().put("page", pageUtil);
    }

    /**
     * 查看信息
     */
    @RequestMapping("/info/{id}")
    @RequiresPermissions("campaign:info")
    public R info(@PathVariable("id") Integer id) {
        CampaignEntity campaign = campaignService.queryObject(id);

        return R.ok().put("campaign", campaign);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @RequiresPermissions("campaign:save")
    public R save(@RequestBody CampaignEntity campaign) {
        campaignService.save(campaign);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    @RequiresPermissions("campaign:update")
    public R update(@RequestBody CampaignEntity campaign) {
        campaignService.update(campaign);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    @RequiresPermissions("campaign:delete")
    public R delete(@RequestBody Integer[] ids) {
        campaignService.deleteBatch(ids);

        return R.ok();
    }
    /**
     * 查看所有列表
     */
    @RequestMapping("/queryAll")
    public R queryAll(@RequestParam Map<String, Object> params) {

        List<CampaignEntity> list = campaignService.queryList(params);

        return R.ok().put("list", list);
    }
}
