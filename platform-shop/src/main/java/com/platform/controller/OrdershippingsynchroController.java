package com.platform.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.fileupload.disk.DiskFileItem;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

import com.platform.entity.OrderEntity;
import com.platform.entity.OrderGoodsEntity;
import com.platform.entity.OrdershippingsynchroEntity;
import com.platform.entity.ShippingEntity;
import com.platform.entity.UserEntity;
import com.platform.oss.OSSFactory;
import com.platform.service.OrderGoodsService;
import com.platform.service.OrderService;
import com.platform.service.OrdershippingsynchroService;
import com.platform.service.ShippingService;
import com.platform.service.UserService;
import com.platform.util.wechat.WSDLUtilsForShop;
import com.platform.utils.PageUtils;
import com.platform.utils.Query;
import com.platform.utils.R;
import com.platform.utils.RRException;
import com.platform.utils.ResourceUtil;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map.Entry;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DateUtil;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
/**
 * 批量导入订单的发货单号；同步运单号Controller
 *
 * @author lipengjun
 * @email 939961241@qq.com
 * @date 2020-02-17 19:44:51
 */
@RestController
@RequestMapping("ordershippingsynchro")
public class OrdershippingsynchroController {
    @Autowired
    private OrdershippingsynchroService ordershippingsynchroService;
    @Autowired
    private OrderService orderService;
	@Autowired
	private OrderGoodsService orderGoodsService;
	@Autowired
	ShippingService shippingService;
	@Autowired
    private UserService userService;
    /**
     * 查看列表
     */
    @RequestMapping("/list")
//    @RequiresPermissions("ordershippingsynchro:list")
    public R list(@RequestParam Map<String, Object> params) {
        //查询列表数据
        Query query = new Query(params);

        List<OrdershippingsynchroEntity> ordershippingsynchroList = ordershippingsynchroService.queryList(query);
        int total = ordershippingsynchroService.queryTotal(query);

        PageUtils pageUtil = new PageUtils(ordershippingsynchroList, total, query.getLimit(), query.getPage());

        return R.ok().put("page", pageUtil);
    }

    /**
     * 查看信息
     */
    @RequestMapping("/info/{orderSn}")
    @RequiresPermissions("ordershippingsynchro:info")
    public R info(@PathVariable("orderSn") String orderSn) {
        OrdershippingsynchroEntity ordershippingsynchro = ordershippingsynchroService.queryObject(orderSn);

        return R.ok().put("ordershippingsynchro", ordershippingsynchro);
    }

    /**
     * 上传文件
     *
     * @param file 文件
     * @return R
     * @throws Exception 异常
     */
    @RequiresPermissions("ordershippingsynchro:uploadFile")
    @RequestMapping("/upload")
    public R upload(HttpServletRequest request,@RequestParam("file") MultipartFile file) throws Exception {
        if (file.isEmpty()) {
            throw new RRException("上传文件不能为空");
        }
        Workbook wb =null;
        Sheet sheet = null;
        Row row = null;
        List<OrdershippingsynchroEntity> list = null;
        wb = readExcel(file);
        int DataNum=0;
        if(wb != null){
            //用来存放表中数据
            list = new ArrayList<OrdershippingsynchroEntity>();
            //获取第一个sheet
            sheet = wb.getSheetAt(0);
           if( checkColTitle(sheet)) {
        	   //获取最大行数
               int rownum = sheet.getPhysicalNumberOfRows();
               //获取第一行
               row = sheet.getRow(0);
               //获取最大列数
               for (int i = 2; i<rownum; i++) {
                   row = sheet.getRow(i);
                   if(row !=null){
                    	   OrdershippingsynchroEntity ordershippingsynchroEntity = new OrdershippingsynchroEntity();
                    	   String orderSn = (String) getCellFormatValue(sheet.getRow(i).getCell(0)).toString().trim(); 	//订单号
                           if(orderSn!=null&&!"".equals(orderSn)) {
                        	   String shippingId =(String) getCellFormatValue(sheet.getRow(i).getCell(1)).toString().trim(); //快递公司id
                               String shippingName = (String) getCellFormatValue(sheet.getRow(i).getCell(2)).toString().trim(); //快递公司名称
                               String shippingNo = (String) getCellFormatValue(sheet.getRow(i).getCell(3)).toString().trim(); 	//快递单号
                               ordershippingsynchroEntity.setOrderSn(orderSn);
                               try {
                            	   ordershippingsynchroEntity.setShippingId(Integer.parseInt(shippingId.replace(".0", "")));
                               } catch (Exception e) {
    							 return R.error("快递公司ID解析有误请查验，订单号："+orderSn+"快递id："+shippingId);	
                               }
                               ordershippingsynchroEntity.setShippingName(shippingName);
                               ordershippingsynchroEntity.setShippingNo(shippingNo);
                               ordershippingsynchroEntity.setStatus(0);
                               ordershippingsynchroEntity.setAddTime(new Date());
                               list.add(ordershippingsynchroEntity);
                           }
                   }else{
                       break;
                   }
               }
               //批量保存
               //遍历解析出来的list
               for (OrdershippingsynchroEntity ordershippingsynchro : list) {
                   ordershippingsynchroService.save(ordershippingsynchro);
               }
               DataNum = list.size();
           }else {
        	   return R.error("模版格式校验失败，请检查后重新上传！");
           }
       }
        return R.ok("数据导入成功，共导入： "+DataNum+" 条数据");
    }
		/**
		* @Description 判断excel的列标题是否和模板一致
		 */
    	private boolean checkColTitle(Sheet sheet) {
      	  Workbook wb =null;
          Row row = null;
          String orderSn = (String) getCellFormatValue(sheet.getRow(1).getCell(0)).toString().trim(); 	//订单号
          String shippingId =(String) getCellFormatValue(sheet.getRow(1).getCell(1)).toString().trim(); //快递公司id
          String shippingName = (String) getCellFormatValue(sheet.getRow(1).getCell(2)).toString().trim(); //快递公司名称
          String shippingNo = (String) getCellFormatValue(sheet.getRow(1).getCell(3)).toString().trim(); 	//快递单号
          if("订单号".equals(orderSn) && "快递公司id".equals(shippingId) && "快递公司名称".equals(shippingName)
        		  && "快递单号".equals(shippingNo) ) {
        	  return true;
          }else {
        	  return false;
          }
    	}

    //读取excel
    public static Workbook readExcel(MultipartFile file) throws IOException{
    	
    	CommonsMultipartFile cFile = (CommonsMultipartFile) file;  
    	DiskFileItem fileItem = (DiskFileItem) cFile.getFileItem();
    	 InputStream inputStream = fileItem.getInputStream();
    	 Workbook wb = null;
    	 String fileName = fileItem.getName();
         String suffix = fileName.substring(fileName.lastIndexOf(".") + 1);
        try {
            if("xls".equals(suffix)){
                return wb = new HSSFWorkbook(inputStream);
            }else if("xlsx".equals(suffix)){
                return wb = new XSSFWorkbook(inputStream);
            }else{
                return wb = null;
            }
            
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return wb;
    }
    public static Object getCellFormatValue(Cell cell){
        Object cellValue = null;
        if(cell!=null){
            //判断cell类型
            switch(cell.getCellType()){
            case Cell.CELL_TYPE_NUMERIC:{
                cellValue = String.valueOf(cell.getNumericCellValue());
                break;
            }
            case Cell.CELL_TYPE_FORMULA:{
                //判断cell是否为日期格式
                if(DateUtil.isCellDateFormatted(cell)){
                    //转换为日期格式YYYY-mm-dd
                    cellValue = cell.getDateCellValue();
                }else{
                    //数字
                    cellValue = String.valueOf(cell.getNumericCellValue());
                }
                break;
            }
            case Cell.CELL_TYPE_STRING:{
                cellValue = cell.getRichStringCellValue().getString();
                break;
            }
            default:
                cellValue = "";
            }
        }else{
            cellValue = "";
        }
        return cellValue;
    }
    /**
     * 保存
     */
    @RequestMapping("/save")
    @RequiresPermissions("ordershippingsynchro:save")
    public R save(@RequestBody OrdershippingsynchroEntity ordershippingsynchro) {
        ordershippingsynchroService.save(ordershippingsynchro);
        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    @RequiresPermissions("ordershippingsynchro:update")
    public R update(@RequestBody OrdershippingsynchroEntity ordershippingsynchro) {
        ordershippingsynchroService.update(ordershippingsynchro);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    @RequiresPermissions("ordershippingsynchro:delete")
    public R delete(@RequestBody String[] orderSns) {
        ordershippingsynchroService.deleteBatch(orderSns);

        return R.ok();
    }
    /**
     * 删除
     */
    @RequestMapping("/synchroData")
    @RequiresPermissions("ordershippingsynchro:uploadFile")
    public R synchroData() {
    	Map<String, Object> params =new HashMap<String, Object>();
    	params.put("status", "0");
    	List<OrdershippingsynchroEntity> list =ordershippingsynchroService.queryList(params);
    	for(int i=0;i<list.size();i++) {
    		OrdershippingsynchroEntity ordershippingsynchroEntity=list.get(i);
    		String orderSn =ordershippingsynchroEntity.getOrderSn();
    		int shippingId = ordershippingsynchroEntity.getShippingId();
    		String shippingName =ordershippingsynchroEntity.getShippingName();
    		String shippingNo =ordershippingsynchroEntity.getShippingNo();
    		if(shippingName == null||shippingNo == null) {
    			continue;
    		}
    		params.clear();
    		OrderEntity orderEntity = orderService.queryObjectByOrderSn(orderSn);
    		 if(orderEntity!=null) {
    			 Integer payStatus = orderEntity.getPayStatus();//付款状态
    			 Integer orderStatus = orderEntity.getOrderStatus();
    	         if (2 == payStatus&&(orderStatus==201||orderStatus==300)) {
    	        	 	  orderEntity.setShippingId(shippingId);
    	            	  orderEntity.setShippingName(shippingName);
    	            	  orderEntity.setShippingNo(shippingNo);
    	            	  orderEntity.setAddress(null);
    	            	  orderEntity.setOrderStatus(300);//订单已发货
    	            	  orderEntity.setShippingStatus(1);//已发货
    	            	  orderService.update(orderEntity);
	    	             if(orderStatus==201) {
	    	            	  UserEntity user = userService.queryObject(orderEntity.getUserId());
	 	    	             Map<String, Object> map =new HashMap<String, Object>();
	 	    	             map.put("orderId", orderEntity.getId());
	 	    	             List<OrderGoodsEntity> OrderGoodsEntitylist=  orderGoodsService.queryList(map);
	 	    	             String type ="Shipped";
	 	    	             if(ResourceUtil.getConfigByName("sys.demo").equals("1")){
	 	    	                 //演示环境不需要注册账号
	 	    	             }else {
	 	    	             	WSDLUtilsForShop.OrderSynchronization(orderEntity,OrderGoodsEntitylist,user,type,null);
	 	    	             }
	    	             }
		    		}else {
		    			 continue;
		    		}
    	         	//更新状态
    	         ordershippingsynchroEntity.setStatus(1);
    	         ordershippingsynchroService.updateStatus(ordershippingsynchroEntity);
		    	}
    	}
        return R.ok();
    }

    /**
     * 查看所有列表
     */
    @RequestMapping("/queryAll")
    public R queryAll(@RequestParam Map<String, Object> params) {

        List<OrdershippingsynchroEntity> list = ordershippingsynchroService.queryList(params);

        return R.ok().put("list", list);
    }
}
