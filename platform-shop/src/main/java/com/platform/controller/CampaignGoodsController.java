package com.platform.controller;

import java.util.List;
import java.util.Map;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.platform.entity.CampaignGoodsEntity;
import com.platform.entity.example.CampaignGoodsExample;
import com.platform.entity.example.CampaignGoodsExample.Criteria;
import com.platform.service.CampaignGoodsService;
import com.platform.utils.PageUtils;
import com.platform.utils.Query;
import com.platform.utils.R;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

/**
 * 活动关联商品Controller
 *
 * @author lipengjun
 * @email 939961241@qq.com
 * @date 2020-02-29 19:14:48
 */
@RestController
@RequestMapping("campaigngoods")
public class CampaignGoodsController {
    @Autowired
    private CampaignGoodsService campaignGoodsService;

    /**
     * 查看列表
     */
    @RequestMapping("/list")
    @RequiresPermissions("campaigngoods:list")
    public R list(@RequestParam Map<String, Object> params) {
        //查询列表数据
        Query query = new Query(params);

        List<CampaignGoodsEntity> campaignGoodsList = campaignGoodsService.queryList(query);
        int total = campaignGoodsService.queryTotal(query);

        PageUtils pageUtil = new PageUtils(campaignGoodsList, total, query.getLimit(), query.getPage());

        return R.ok().put("page", pageUtil);
    }

    /**
     * 查看信息
     */
    @RequestMapping("/info/{id}")
    @RequiresPermissions("campaigngoods:info")
    public R info(@PathVariable("id") Integer id) {
        CampaignGoodsEntity campaignGoods = campaignGoodsService.queryObject(id);

        return R.ok().put("campaignGoods", campaignGoods);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @RequiresPermissions("campaigngoods:save")
    public R save(@RequestBody Object json) {
    	JSONObject jsonobj=JSONObject.fromObject(json);
//        campaignGoodsService.save(campaignGoods);
    	if(jsonobj.get("campaignId")==null||jsonobj.getJSONArray("ids")==null) {
    		 R.error("参数错误！数据添加失败！");
    	}else {
    		try {
    			String campaignId = (String) jsonobj.get("campaignId");
            	JSONArray  jsonArray  = jsonobj.getJSONArray("ids");
            	for(int i=0;i<jsonArray.size();i++){
            		CampaignGoodsEntity campaignGoods = new CampaignGoodsEntity();
            		campaignGoods.setCampaignId(Integer.parseInt(campaignId));
            		campaignGoods.setGoodsId(Integer.parseInt(jsonArray.get(i).toString()));
            		campaignGoodsService.save(campaignGoods);
            	}
			} catch (Exception e) {
				 R.error("数据添加失败！");
			}
        	
    	}

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    @RequiresPermissions("campaigngoods:update")
    public R update(@RequestBody CampaignGoodsEntity campaignGoods) {
        campaignGoodsService.update(campaignGoods);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    @RequiresPermissions("campaigngoods:delete")
    public R delete(@RequestBody Integer[] ids) {
        campaignGoodsService.deleteBatch(ids);

        return R.ok();
    }
    /**
     * 条件删除
     */
    @RequestMapping("/deleteByExample")
    @RequiresPermissions("campaigngoods:delete")
    public R deleteByExample(@RequestBody List<Integer> ids) {
    	CampaignGoodsExample example = new CampaignGoodsExample();
    	Criteria criteria = example.createCriteria();
    	criteria.andGoodsIdIn(ids);
    	campaignGoodsService.deleteByExample(example);

        return R.ok();
    }
    /**
     * 查看所有列表
     */
    @RequestMapping("/queryAll")
    public R queryAll(@RequestParam Map<String, Object> params) {

        List<CampaignGoodsEntity> list = campaignGoodsService.queryList(params);

        return R.ok().put("list", list);
    }
}
