package com.platform.controller;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.platform.entity.GroupBuyingEntity;
import com.platform.entity.OrderEntity;
import com.platform.entity.OrderGoodsEntity;
import com.platform.entity.SysUserEntity;
import com.platform.entity.UserEntity;
import com.platform.service.OrderGoodsService;
import com.platform.service.OrderService;
import com.platform.service.ShippingService;
import com.platform.service.UserService;
import com.platform.util.wechat.WSDLUtilsForShop;
import com.platform.util.wechat.WechatRefundApiResult;
import com.platform.util.wechat.WechatUtil;
import com.platform.utils.Base64;
import com.platform.utils.PageUtils;
import com.platform.utils.Query;
import com.platform.utils.R;
import com.platform.utils.RRException;
import com.platform.utils.ResourceUtil;
import com.platform.utils.ShiroUtils;



/**
 * @author lipengjun
 * @email 939961241@qq.com
 * @date 2017-08-13 10:41:09
 */
@RestController
@RequestMapping("order")
public class OrderController {
    @Autowired
    private OrderService orderService;
	@Autowired
	private OrderGoodsService orderGoodsService;
	@Autowired
	ShippingService shippingService;
	@Autowired
    private UserService userService;
    /**
     * 列表
     */
    @RequestMapping("/list")
    @RequiresPermissions("order:list")
    public R list(@RequestParam Map<String, Object> params) {
        SysUserEntity sysUserEntity= ShiroUtils.getUserEntity();
        // 查询列表数据
        Query query = new Query(params);
        // sysUserEntity.getAllShow()为1时，查看所有商户数据
        //是否显示其他商户 0:不显示，1:显示
        if(sysUserEntity!=null && 1!=sysUserEntity.getAllShow()) {
        	query.put("merchantId",sysUserEntity.getMerchantId());
        }
        List<OrderEntity> orderList = orderService.queryList(query);
        int total = orderService.queryTotal(query);
        for(OrderEntity user : orderList) {
        	user.setUserName(Base64.decode(user.getUserName()));
        }
        PageUtils pageUtil = new PageUtils(orderList, total, query.getLimit(), query.getPage());

        return R.ok().put("page", pageUtil);
    }
    
    
    /**
     * 列表
     */
    @RequestMapping("/groupList")
    public R groupList(@RequestParam Map<String, Object> params) {
        SysUserEntity sysUserEntity= ShiroUtils.getUserEntity();
        // 查询列表数据
        Query query = new Query(params);
        query.put("merchantId",sysUserEntity.getMerchantId());
        List<GroupBuyingEntity> list = orderService.queryGroupList(query);
        int total = orderService.queryGroupTotal(query);
        PageUtils pageUtil = new PageUtils(list, total, query.getLimit(), query.getPage());
        return R.ok().put("page", pageUtil);
    }



    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    @RequiresPermissions("order:info")
    public R info(@PathVariable("id") Integer id) {
        OrderEntity order = orderService.queryObject(id);
        Map<String, Object> params =new HashMap<String, Object>();
        params.put("orderId", order.getId());
        List<OrderGoodsEntity> list=  orderGoodsService.queryList(params);
        String goodsName="";
        //商品规格详情
      	String goodsSpecifitionNameValue="";
        for (OrderGoodsEntity orderGoodsEntity : list) {
        	goodsName=goodsName+orderGoodsEntity.getGoodsName()+",";
			if(orderGoodsEntity.getGoodsSpecifitionNameValue()!=null) {
				goodsSpecifitionNameValue=goodsSpecifitionNameValue+orderGoodsEntity.getGoodsSpecifitionNameValue()+",";
			}
		}
        order.setGoodsNames(goodsName);
        order.setGoodsSpecifitionNameValue(goodsSpecifitionNameValue);
        order.setUserName(Base64.decode(order.getUserName()));
        order.setAddress(order.getProvince() + order.getCity() + order.getDistrict() + order.getAddress());
        return R.ok().put("order", order);
    }
    
    /**
     * 信息
     */
    @RequestMapping("/goodsinfo/{gid}")
    @RequiresPermissions("order:info")
    public R goodsinfo(@PathVariable("gid") Integer gid) {
    	OrderGoodsEntity orderGoods = orderGoodsService.queryObject(gid);
        return R.ok().put("orderGoods", orderGoods);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @RequiresPermissions("order:save")
    public R save(@RequestBody OrderEntity order) {
        orderService.save(order);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    @RequiresPermissions("order:update")
    public R update(@RequestBody OrderEntity order) {
        orderService.update(order);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    @RequiresPermissions("order:delete")
    public R delete(@RequestBody Integer[] ids) {
        orderService.deleteBatch(ids);

        return R.ok();
    }

    /**
     * 查看所有列表
     */
    @RequestMapping("/queryAll")
    public R queryAll(@RequestParam Map<String, Object> params) {

        List<OrderEntity> list = orderService.queryList(params);

        return R.ok().put("list", list);
    }

    /**
     * 总计
     */
    @RequestMapping("/queryTotal")
    public R queryTotal(@RequestParam Map<String, Object> params) {
        int sum = orderService.queryTotal(params);

        return R.ok().put("sum", sum);
    }

    /**
     * 确定收货
     *
     * @param id
     * @return
     */
    @RequestMapping("/confirm")
    @RequiresPermissions("order:confirm")
    public R confirm(@RequestBody Integer id) {
        orderService.confirm(id);

        return R.ok();
    }

    /**
     * 发货
     *
     * @param order
     * @return
     */
    @RequestMapping("/sendGoods")
    @RequiresPermissions("order:sendGoods")
    public R sendGoods(@RequestBody OrderEntity order) {
    	 Integer payStatus = order.getPayStatus();//付款状态
         if (2 != payStatus) {
             throw new RRException("此订单未付款！");
         }else {
        	 orderService.sendGoods(order);
             order = orderService.queryObject(order.getId());
             UserEntity user = userService.queryObject(order.getUserId());
             Map<String, Object> params =new HashMap<String, Object>();
             params.put("orderId", order.getId());
             List<OrderGoodsEntity> list=  orderGoodsService.queryList(params);
             String type ="Shipped";
             if(ResourceUtil.getConfigByName("sys.demo").equals("1")){
                 //演示环境不需要注册账号
             }else {
             	WSDLUtilsForShop.OrderSynchronization(order,list,user,type,null);
             }
         }
       
        return R.ok();
    }
    /**
     * 修改发货单据
     *
     * @param order
     * @return
     */
    @RequestMapping("/EditSendGoods")
    @RequiresPermissions("order:EditSendGoods")
    public R EditSendGoods(@RequestBody OrderEntity order) {
        orderService.sendGoods(order);
        return R.ok();
    }
   //退货退款
    @RequestMapping("orderRefund")
    @RequiresPermissions(value = "orderreturn:refund")
      public Object cancelOrder(Integer orderId,Integer orderGoodsId,Integer productId) {
        try {
            OrderEntity orderEntity = orderService.queryObject(orderId);
            
            List<OrderEntity> orders = orderService.queryByAllOrderId(orderEntity.getAll_order_id());
            //没有具体产品信息，则表明需要退整个订单
            if(orderGoodsId==null) {
            	  BigDecimal allPrice = BigDecimal.ZERO;
                  for(OrderEntity o : orders) {
                  	allPrice = allPrice.add(o.getAll_price());
                  }
               // 需要退款
                  if (orderEntity.getPayStatus() == 2) {
                  	//退订单总价
                	  WechatRefundApiResult result = WechatUtil.wxRefund(orderEntity.getAll_order_id().toString(),
                      		allPrice.doubleValue(), orderEntity.getAll_price().doubleValue());
                      //测试修改金额
//                      WechatRefundApiResult result = WechatUtil.wxRefund(orderVo.getId().toString(), 0.01d, 0.01d);

                	  //5xx 标识订单申请了售后，501，提交申请，待审核 ；  502 同意申请，待买家发货；503 买家已发货，待卖家收货；504 卖家已收货，待退款。-----402，已收货，退款退货
                	  if (result.getResult_code().equals("SUCCESS")) {
                        	  orderEntity.setOrderStatus(402);
                          orderEntity.setPayStatus(4);
                          orderService.update(orderEntity);
                          
                          //更新优惠券状态和实际
						/*
						 * UserCouponVo uc = new UserCouponVo(); uc.setId(orderEntity.getCouponId());
						 * uc.setCoupon_status(1); uc.setUsed_time(null);
						 * userCouponService.updateCouponStatus(uc);
						 */
                          
						/*
						 * //去掉订单成功成立分润退还 try { orderService.cancelFx(orderVo.getId(),
						 * orderVo.getPay_time(), orderVo.getAll_price().multiply(new
						 * BigDecimal("100")).intValue()); }catch(Exception e) {
						 * System.out.println("================取消订单返还分润开始================");
						 * e.printStackTrace();
						 * System.out.println("================取消订单返还分润开始================"); } //还原库存
						 * Map<String, Object> orderGoodsParam = new HashMap<String, Object>();
						 * orderGoodsParam.put("order_id", orderVo.getId()); //订单的商品 List<OrderGoodsVo>
						 * orderGoodsList = orderGoodsService.queryList(orderGoodsParam);
						 * for(OrderGoodsVo orderGoodVo:orderGoodsList) { //还原产品的库存信息
						 * productService.updateProductNumber_add(orderGoodVo.getProduct_id(),
						 * orderGoodVo.getNumber()); //更新商品的库存以及销量
						 * productService.updateGoodsNumber_add(orderGoodVo.getGoods_id(),
						 * orderGoodVo.getNumber()); }
						 */
                          
                          //更新退货申请
                          return R.ok("退款成功");
                      } else {
                          return R.error(400, "退款失败");
                      }
                  }
            }else {
            	//查询产品的价格；
				
				  OrderGoodsEntity goodsEntity=orderService.queryOrderGoodsObject(orderId,
				  orderGoodsId, productId); 
				  BigDecimal goodsTotalPrice = BigDecimal.ZERO;//商品总额
				  //计算商品价格 --- 要算上折扣
				  String goodsDiscont = orderEntity.getGoodsDiscount();
				  
				  goodsTotalPrice = goodsTotalPrice
				  .add(goodsEntity.getRetailPrice().multiply(new  BigDecimal(goodsEntity.getNumber())));
				  if(goodsDiscont!=null&&!"1".equals(goodsDiscont)) {
					  goodsTotalPrice.multiply(new  BigDecimal(goodsDiscont)) ;
				  }
				  BigDecimal allPrice = BigDecimal.ZERO;
				 	for(OrderEntity o : orders) {
				 		allPrice = allPrice.add(o.getAll_price()); 
				 	}
				 
              	//只退商品的价格
            	  WechatRefundApiResult result = WechatUtil.wxRefund(orderEntity.getAll_order_id().toString(),
                  		allPrice.doubleValue(), goodsTotalPrice.doubleValue());
                  //测试修改金额
//                  WechatRefundApiResult result = WechatUtil.wxRefund(orderVo.getId().toString(), 0.01d, 0.01d);

            	  //5xx 标识订单申请了售后，501，提交申请，待审核 ；  502 同意申请，待买家发货；503 买家已发货，待卖家收货；504 卖家已收货，待退款。-----402，已收货，退款退货
            	  if (result.getResult_code().equals("SUCCESS")) {
                      if (orderEntity.getOrderStatus() == 504) {
                    	  orderEntity.setOrderStatus(402);
  					} 
                      orderEntity.setPayStatus(4);
                      orderService.update(orderEntity);
                      
                      //更新优惠券状态和实际
					/*
					 * UserCouponVo uc = new UserCouponVo(); uc.setId(orderEntity.getCouponId());
					 * uc.setCoupon_status(1); uc.setUsed_time(null);
					 * userCouponService.updateCouponStatus(uc);
					 */
                      
					/*
					 * //去掉订单成功成立分润退还 try { orderService.cancelFx(orderVo.getId(),
					 * orderVo.getPay_time(), orderVo.getAll_price().multiply(new
					 * BigDecimal("100")).intValue()); }catch(Exception e) {
					 * System.out.println("================取消订单返还分润开始================");
					 * e.printStackTrace();
					 * System.out.println("================取消订单返还分润开始================"); } //还原库存
					 * Map<String, Object> orderGoodsParam = new HashMap<String, Object>();
					 * orderGoodsParam.put("order_id", orderVo.getId()); //订单的商品 List<OrderGoodsVo>
					 * orderGoodsList = orderGoodsService.queryList(orderGoodsParam);
					 * for(OrderGoodsVo orderGoodVo:orderGoodsList) { //还原产品的库存信息
					 * productService.updateProductNumber_add(orderGoodVo.getProduct_id(),
					 * orderGoodVo.getNumber()); //更新商品的库存以及销量
					 * productService.updateGoodsNumber_add(orderGoodVo.getGoods_id(),
					 * orderGoodVo.getNumber()); }
					 */
                      
                      //更新退货申请
                      
                      return R.ok("退款成功");
                  } else {
                      return R.error(400, "退款失败");
                  }
              }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
    
}
