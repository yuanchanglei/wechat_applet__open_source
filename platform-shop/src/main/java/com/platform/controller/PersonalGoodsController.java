package com.platform.controller;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.platform.entity.PersonalGoodsEntity;
import com.platform.entity.SysUserEntity;
import com.platform.entity.example.PersonalGoodsExample;
import com.platform.entity.example.PersonalGoodsExample.Criteria;
import com.platform.service.PersonalGoodsService;
import com.platform.utils.PageUtils;
import com.platform.utils.Query;
import com.platform.utils.R;
import com.platform.utils.ShiroUtils;

/**
 * Controller
 *
 * @author lipengjun
 * @email 939961241@qq.com
 * @date 2019-07-31 10:44:54
 */
@RestController
@RequestMapping("personalgoods")
public class PersonalGoodsController {
    @Autowired
    private PersonalGoodsService personalGoodsService;

    /**
     * 查看列表
     */
    @RequestMapping("/list")
    @RequiresPermissions("personalgoods:list")
    public R list(@RequestParam Map<String, Object> params) {
        //查询列表数据
        Query query = new Query(params);

        List<PersonalGoodsEntity> personalGoodsList = personalGoodsService.queryList(query);
        int total = personalGoodsService.queryTotal(query);

        PageUtils pageUtil = new PageUtils(personalGoodsList, total, query.getLimit(), query.getPage());

        return R.ok().put("page", pageUtil);
    }

    /**
     * 查看信息
     */
    @RequestMapping("/info/{id}")
    @RequiresPermissions("personalgoods:info")
    public R info(@PathVariable("id") Long id) {
        PersonalGoodsEntity personalGoods = personalGoodsService.queryObject(id);

        return R.ok().put("personalGoods", personalGoods);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @RequiresPermissions("personalgoods:save")
    public R save(@RequestBody Long[] ids) {
		personalGoodsService.saveBatch(ids);
		return R.ok();	
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    @RequiresPermissions("personalgoods:update")
    public R update(@RequestBody PersonalGoodsEntity personalGoods) {
        personalGoodsService.update(personalGoods);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    @RequiresPermissions("personalgoods:delete")
    public R delete(@RequestBody Long[] ids) {
        personalGoodsService.deleteBatch(ids);

        return R.ok();
    }
    
    /**
     * 条件删除
     */
    @RequestMapping("/deleteByExample")
    @RequiresPermissions("personalgoods:deleteByExample")
    public R deleteByExample(@RequestBody List<Long> ids) {
    	SysUserEntity sysUserEntity= ShiroUtils.getUserEntity();
    	
    	PersonalGoodsExample example = new PersonalGoodsExample();
    	Criteria criteria = example.createCriteria();
    	criteria.andMerchantIdEqualTo(sysUserEntity.getMerchantId());
    	criteria.andGoodsIdIn(ids);
        personalGoodsService.deleteByExample(example);

        return R.ok();
    }

    /**
     * 查看所有列表
     */
    @RequestMapping("/queryAll")
    public R queryAll(@RequestParam Map<String, Object> params) {

        List<PersonalGoodsEntity> list = personalGoodsService.queryList(params);

        return R.ok().put("list", list);
    }
}
