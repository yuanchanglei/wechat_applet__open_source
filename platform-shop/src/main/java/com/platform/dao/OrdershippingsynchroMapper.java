package com.platform.dao;

import java.util.Map;

import com.platform.entity.OrdershippingsynchroEntity;
import com.platform.entity.example.OrdershippingsynchroExample;

/**
 * 批量导入订单的发货单号；同步运单号Dao
 *
 * @author xuyang
 * @email 295640759@qq.com
 * @date 2020-02-17 19:44:51
 */
public interface OrdershippingsynchroMapper extends BaseMapper<OrdershippingsynchroEntity, OrdershippingsynchroExample> {
	/**
	 * 根据主键查询记录
	 * @param orderSn
	 * @return
	 */
	OrdershippingsynchroEntity selectByPrimaryKey(String orderSn);

	int deleteByPrimaryKey(String orderSn);
	int updateStatus(OrdershippingsynchroEntity ordershippingsynchro);
}
