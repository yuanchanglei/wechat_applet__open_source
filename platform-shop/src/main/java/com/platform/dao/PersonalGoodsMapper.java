package com.platform.dao;

import com.platform.entity.PersonalGoodsEntity;
import com.platform.entity.example.PersonalGoodsExample;

/**
 * Dao
 *
 * @author xuyang
 * @email 295640759@qq.com
 * @date 2019-07-31 10:44:54
 */
public interface PersonalGoodsMapper extends BaseMapper<PersonalGoodsEntity, PersonalGoodsExample> {

}
