package com.platform.dao;

import java.util.List;
import java.util.Map;

import com.platform.entity.GoodsEntity;

/**
 * Dao
 *
 * @author lipengjun
 * @email 939961241@qq.com
 * @date 2017-08-21 21:19:49
 */
public interface GoodsDao extends BaseDao<GoodsEntity> {
    Integer queryMaxId();
    /**
     * 分页查询：为店铺活动准备
     *
     * @param map 参数
     * @return list
     */
    List<GoodsEntity> listForCampaignGoods(Map<String, Object> map);
    
    /**
     * 分页统计总数：为店铺活动准备
     *
     * @param map 参数
     * @return 总数
     */
    int queryTotalForCampaignGoods(Map<String, Object> map);
    /**
     * 分页查询：为店铺活动准备
     *
     * @param map 参数
     * @return list
     */
    List<GoodsEntity> listForCampaignGoodsSelect(Map<String, Object> map);
    
    /**
     * 分页统计总数：为店铺活动准备
     *
     * @param map 参数
     * @return 总数
     */
    int queryTotalForCampaignGoodsSelect(Map<String, Object> map);
}
