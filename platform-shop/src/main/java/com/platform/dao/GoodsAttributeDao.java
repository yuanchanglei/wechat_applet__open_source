package com.platform.dao;

import java.util.List;
import java.util.Map;

import com.platform.entity.GoodsAttributeEntity;

/**
 * @author lipengjun
 * @email 939961241@qq.com
 * @date 2017-08-13 10:41:08
 */
public interface GoodsAttributeDao extends BaseDao<GoodsAttributeEntity> {

    int updateByGoodsIdAttributeId(GoodsAttributeEntity goodsAttributeEntity);
	List<GoodsAttributeEntity> queryListByGoodsId(Map<String, Object> map);

}
