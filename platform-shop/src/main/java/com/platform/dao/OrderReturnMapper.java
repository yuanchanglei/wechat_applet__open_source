package com.platform.dao;

import com.platform.entity.OrderReturnEntity;
import com.platform.entity.example.OrderReturnExample;

/**
 * 退款申请表Dao
 *
 * @author xuyang
 * @email 295640759@qq.com
 * @date 2019-08-28 21:42:11
 */
public interface OrderReturnMapper extends BaseMapper<OrderReturnEntity, OrderReturnExample> {

}
