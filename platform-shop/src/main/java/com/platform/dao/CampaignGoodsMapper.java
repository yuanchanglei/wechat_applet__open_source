package com.platform.dao;

import com.platform.entity.CampaignGoodsEntity;
import com.platform.entity.example.CampaignGoodsExample;

/**
 * 活动关联商品Dao
 *
 * @author xuyang
 * @email 295640759@qq.com
 * @date 2020-02-29 19:14:48
 */
public interface CampaignGoodsMapper extends BaseMapper<CampaignGoodsEntity, CampaignGoodsExample> {
	/**
	 * 根据主键查询记录
	 * @param id
	 * @return
	 */
	CampaignGoodsEntity selectByPrimaryKey(Integer id);
	/**
	 * 按主键物理删除记录<br>
	 * 慎用：建议逻辑删除
	 * @param id
	 * @return
	 */
	int deleteByPrimaryKey(Integer id);
}
