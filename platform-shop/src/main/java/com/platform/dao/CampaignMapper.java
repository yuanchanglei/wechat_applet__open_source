package com.platform.dao;

import com.platform.entity.CampaignEntity;
import com.platform.entity.example.CampaignExample;

/**
 * 店铺活动表Dao
 *
 * @author xuyang
 * @email 295640759@qq.com
 * @date 2020-02-29 19:14:47
 */
public interface CampaignMapper extends BaseMapper<CampaignEntity, CampaignExample> {
	/**
	 * 根据主键查询记录
	 * @param id
	 * @return
	 */
	CampaignEntity selectByPrimaryKey(Integer id);
	/**
	 * 按主键物理删除记录<br>
	 * 慎用：建议逻辑删除
	 * @param id
	 * @return
	 */
	int deleteByPrimaryKey(Integer id);
}
