package com.platform.dao;

import com.platform.entity.ProductEntity;

/**
 * Dao
 *
 * @author lipengjun
 * @email 939961241@qq.com
 * @date 2017-08-30 14:31:21
 */
public interface ProductDao extends BaseDao<ProductEntity> {
	
	int updateGoodsNumber(Integer goodsId);

	int updateGoodsRetailPrice(Integer goodsId);
    //减掉购买的产品库存
    public void updateProductNumber_cut(int productId,Integer cartNumber); 
    //还原购买的产品库存
    public void updateProductNumber_add(int productId,Integer cartNumber);
    /**
               * 根据参数修改商品库存，和销量_cut 为减库存
     * @param goodsId  商品id
     * @param cartNumber 购物车本次购买的数量
     */
    public void updateGoodsNumber_cut(int goodsId,Integer cartNumber);
    /**
            * 根据参数修改商品库存，和销量_add 为还原库存
	* @param goodsId  商品id
	* @param cartNumber 购物车本次购买的数量
	*/
	public void updateGoodsNumber_add(int goodsId,Integer cartNumber);
}
