package com.platform.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

/**
 * 退款申请表实体
 * 表名 nideshop_order_return
 *
 * @author lipengjun
 * @email 939961241@qq.com
 * @date 2019-08-28 21:42:11
 */
public class OrderReturnEntity implements Serializable {
    private static final long serialVersionUID = 1L;

    //
    private Integer id;
    //订单id
    private Integer orderId;
    //订单中的购买商品的id
    private Integer orderGoodsId;
    //订单中的购买产品的id
    private Integer orderProductId;
  //退货的产品数量
    private Integer number;
    //退货原因
    private String returnReason;
    //状态（0：拒绝申请；1：提交申请；2：同意申请；3：买家已发货；4：卖家已收货；5：退款已完成）
    private Integer status;
    //
    private Integer shippingId;
    //快递公司名称
    private String shippingName;
    //快递单号
    private String shippingNo;
    //拒绝原因
    private String rejectInfo;
    //创建时间
    private String creatTime;
    //更新时间
    private String updateTime;
    //辅助字段
    //订单编号
    private String orderSn;
    //商品名称
    private String orderGoodsName;
    //退货价格
    private BigDecimal returnPrice;
    //会员
    private String userName;
    //手机
    private String mobileNumber;
    private OrderEntity orderEntity;
    private List<OrderGoodsEntity> orderGoodsList;
    private String returnType;
    private String changeShippingName;//退换货--物流公司名称
    private String changeShippingNo;//退换货--物流编号
    /**
     * 设置：
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 获取：
     */
    public Integer getId() {
        return id;
    }
    /**
     * 设置：订单id
     */
    public void setOrderId(Integer orderId) {
        this.orderId = orderId;
    }

    /**
     * 获取：订单id
     */
    public Integer getOrderId() {
        return orderId;
    }
    /**
     * 设置：订单中的购买商品的id
     */
    public void setOrderGoodsId(Integer orderGoodsId) {
        this.orderGoodsId = orderGoodsId;
    }

    /**
     * 获取：订单中的购买商品的id
     */
    public Integer getOrderGoodsId() {
        return orderGoodsId;
    }
    /**
     * 设置：退货原因
     */
    public void setReturnReason(String returnReason) {
        this.returnReason = returnReason;
    }

    /**
     * 获取：退货原因
     */
    public String getReturnReason() {
        return returnReason;
    }
    /**
     * 设置：状态（0：拒绝申请；1：提交申请；2：同意申请；3：买家已发货；4：卖家已收货；5：退款已完成）
     */
    public void setStatus(Integer status) {
        this.status = status;
    }

    /**
     * 获取：状态（0：拒绝申请；1：提交申请；2：同意申请；3：买家已发货；4：卖家已收货；5：退款已完成）
     */
    public Integer getStatus() {
        return status;
    }
    /**
     * 设置：
     */
    public void setShippingId(Integer shippingId) {
        this.shippingId = shippingId;
    }

    /**
     * 获取：
     */
    public Integer getShippingId() {
        return shippingId;
    }
    /**
     * 设置：快递公司名称
     */
    public void setShippingName(String shippingName) {
        this.shippingName = shippingName;
    }

    /**
     * 获取：快递公司名称
     */
    public String getShippingName() {
        return shippingName;
    }
    /**
     * 设置：快递单号
     */
    public void setShippingNo(String shippingNo) {
        this.shippingNo = shippingNo;
    }

    /**
     * 获取：快递单号
     */
    public String getShippingNo() {
        return shippingNo;
    }
    /**
     * 设置：拒绝原因
     */
    public void setRejectInfo(String rejectInfo) {
        this.rejectInfo = rejectInfo;
    }

    /**
     * 获取：拒绝原因
     */
    public String getRejectInfo() {
        return rejectInfo;
    }
    /**
     * 设置：创建时间
     */
    public void setCreatTime(String creatTime) {
        this.creatTime = creatTime;
    }

    /**
     * 获取：创建时间
     */
    public String getCreatTime() {
        return creatTime;
    }
    /**
     * 设置：更新时间
     */
    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime;
    }

    /**
     * 获取：更新时间
     */
    public String getUpdateTime() {
        return updateTime;
    }

	public String getOrderSn() {
		return orderSn;
	}

	public void setOrderSn(String orderSn) {
		this.orderSn = orderSn;
	}

	public String getOrderGoodsName() {
		return orderGoodsName;
	}

	public void setOrderGoodsName(String orderGoodsName) {
		this.orderGoodsName = orderGoodsName;
	}


	public Integer getNumber() {
		return number;
	}

	public void setNumber(Integer number) {
		this.number = number;
	}

	public BigDecimal getReturnPrice() {
		return returnPrice;
	}

	public void setReturnPrice(BigDecimal returnPrice) {
		this.returnPrice = returnPrice;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public Integer getOrderProductId() {
		return orderProductId;
	}

	public void setOrderProductId(Integer orderProductId) {
		this.orderProductId = orderProductId;
	}

	public OrderEntity getOrderEntity() {
		return orderEntity;
	}

	public void setOrderEntity(OrderEntity orderEntity) {
		this.orderEntity = orderEntity;
	}

	public List<OrderGoodsEntity> getOrderGoodsList() {
		return orderGoodsList;
	}

	public void setOrderGoodsList(List<OrderGoodsEntity> orderGoodsList) {
		this.orderGoodsList = orderGoodsList;
	}

	public String getReturnType() {
		return returnType;
	}

	public void setReturnType(String returnType) {
		this.returnType = returnType;
	}

	public String getChangeShippingName() {
		return changeShippingName;
	}

	public void setChangeShippingName(String changeShippingName) {
		this.changeShippingName = changeShippingName;
	}

	public String getChangeShippingNo() {
		return changeShippingNo;
	}

	public void setChangeShippingNo(String changeShippingNo) {
		this.changeShippingNo = changeShippingNo;
	}
    
}
