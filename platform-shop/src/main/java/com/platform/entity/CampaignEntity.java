package com.platform.entity;

import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 店铺活动表实体
 * 表名 nideshop_campaign
 *
 * @author lipengjun
 * @email 939961241@qq.com
 * @date 2020-02-29 19:14:47
 */
public class CampaignEntity implements Serializable {
    private static final long serialVersionUID = 1L;

    //
    private Integer id;
    //活动类型（0：满减 1：多件多折扣）
    private Integer campaignType;
    //活动名称
    private String name;
    //活动开始时间
    private Date startDate;
    //结束时间
    private Date endDate;
    //是否启用（1：启用；0不启用）
    private Integer status;
    //0：不是全部商品参加。1全部商品参加
    private Integer isAllGoods;
    /**
     * 设置：
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 获取：
     */
    public Integer getId() {
        return id;
    }
    /**
     * 设置：活动类型（0：满减 1：多件多折扣）
     */
    public void setCampaignType(Integer campaignType) {
        this.campaignType = campaignType;
    }

    /**
     * 获取：活动类型（0：满减 1：多件多折扣）
     */
    public Integer getCampaignType() {
        return campaignType;
    }
    /**
     * 设置：活动名称
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 获取：活动名称
     */
    public String getName() {
        return name;
    }
    /**
     * 设置：活动开始时间
     */
    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    /**
     * 获取：活动开始时间
     */
    public Date getStartDate() {
        return startDate;
    }
    /**
     * 设置：结束时间
     */
    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    /**
     * 获取：结束时间
     */
    public Date getEndDate() {
        return endDate;
    }
    /**
     * 设置：是否启用（1：启用；0不启用）
     */
    public void setStatus(Integer status) {
        this.status = status;
    }

    /**
     * 获取：是否启用（1：启用；0不启用）
     */
    public Integer getStatus() {
        return status;
    }

	public Integer getIsAllGoods() {
		return isAllGoods;
	}

	public void setIsAllGoods(Integer isAllGoods) {
		this.isAllGoods = isAllGoods;
	}
    
}
