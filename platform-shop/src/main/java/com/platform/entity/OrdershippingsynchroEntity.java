package com.platform.entity;

import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 批量导入订单的发货单号；同步运单号实体
 * 表名 ordershippingsynchro
 *
 * @author lipengjun
 * @email 939961241@qq.com
 * @date 2020-02-17 19:44:51
 */
public class OrdershippingsynchroEntity implements Serializable {
    private static final long serialVersionUID = 1L;

    //
    private String orderSn;
    //
    private Integer shippingId;
    //
    private String shippingName;
    //
    private String shippingNo;
    private Integer status;
    private Date addTime;

    /**
     * 设置：
     */
    public void setOrderSn(String orderSn) {
        this.orderSn = orderSn;
    }

    /**
     * 获取：
     */
    public String getOrderSn() {
        return orderSn;
    }
    /**
     * 设置：
     */
    public void setShippingId(Integer shippingId) {
        this.shippingId = shippingId;
    }

    /**
     * 获取：
     */
    public Integer getShippingId() {
        return shippingId;
    }
    /**
     * 设置：
     */
    public void setShippingName(String shippingName) {
        this.shippingName = shippingName;
    }

    /**
     * 获取：
     */
    public String getShippingName() {
        return shippingName;
    }
    /**
     * 设置：
     */
    public void setShippingNo(String shippingNo) {
        this.shippingNo = shippingNo;
    }

    /**
     * 获取：
     */
    public String getShippingNo() {
        return shippingNo;
    }

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	public Date getAddTime() {
		return addTime;
	}

	public void setAddTime(Date addTime) {
		this.addTime = addTime;
	}
    
}
