package com.platform.entity;

import java.io.Serializable;
import java.util.Date;

/**
 * 活动关联商品实体
 * 表名 nideshop_campaign_goods
 *
 * @author lipengjun
 * @email 939961241@qq.com
 * @date 2020-02-29 19:14:48
 */
public class CampaignGoodsEntity implements Serializable {
    private static final long serialVersionUID = 1L;

    //主键
    private Integer id;
    //活动Id
    private Integer campaignId;
    //商品id
    private Integer goodsId;

    /**
     * 设置：主键
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 获取：主键
     */
    public Integer getId() {
        return id;
    }
    /**
     * 设置：活动Id
     */
    public void setCampaignId(Integer campaignId) {
        this.campaignId = campaignId;
    }

    /**
     * 获取：活动Id
     */
    public Integer getCampaignId() {
        return campaignId;
    }
    /**
     * 设置：商品id
     */
    public void setGoodsId(Integer goodsId) {
        this.goodsId = goodsId;
    }

    /**
     * 获取：商品id
     */
    public Integer getGoodsId() {
        return goodsId;
    }
}
