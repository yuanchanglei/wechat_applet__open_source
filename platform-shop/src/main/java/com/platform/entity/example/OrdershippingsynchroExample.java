package com.platform.entity.example;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * 批量导入订单的发货单号；同步运单号实体
 * 表名 ordershippingsynchro
 *
 * @author xuyang
 * @email 295640759@qq.com
 * @date 2020-02-17 19:44:51
 */
public class OrdershippingsynchroExample extends AbstractExample  {
    private static final long serialVersionUID = 1L;

    @Override
    public Criteria or() {
        return (Criteria)super.or();
    }

    @Override
    public Criteria createCriteria() {
        return (Criteria)super.createCriteria();
    }

    @Override
    protected Criteria createCriteriaInternal() {
        return new Criteria();
    }

    public class Criteria extends GeneratedCriteria {
        protected Criteria() {
            super();
        }

            public Criteria andOrderSnIsNull() {
            addCriterion("order_sn is null");
            return (Criteria) this;
        }

        public Criteria andOrderSnIsNotNull() {
            addCriterion("order_sn is not null");
            return (Criteria) this;
        }

        public Criteria andOrderSnEqualTo(String value) {
            addCriterion("order_sn =", value, "orderSn");
            return (Criteria) this;
        }

        public Criteria andOrderSnNotEqualTo(String value) {
            addCriterion("order_sn <>", value, "orderSn");
            return (Criteria) this;
        }

        public Criteria andOrderSnGreaterThan(String value) {
            addCriterion("order_sn >", value, "orderSn");
            return (Criteria) this;
        }

        public Criteria andOrderSnGreaterThanOrEqualTo(String value) {
            addCriterion("order_sn >=", value, "orderSn");
            return (Criteria) this;
        }

        public Criteria andOrderSnLessThan(String value) {
            addCriterion("order_sn <", value, "orderSn");
            return (Criteria) this;
        }

        public Criteria andOrderSnLessThanOrEqualTo(String value) {
            addCriterion("order_sn <=", value, "orderSn");
            return (Criteria) this;
        }
            public Criteria andOrderSnLike(String value) {
            addCriterion("order_sn like", value, "orderSn");
            return (Criteria) this;
        }

        public Criteria andOrderSnNotLike(String value) {
            addCriterion("order_sn not like", value, "orderSn");
            return (Criteria) this;
        }
            public Criteria andOrderSnIn(List<String> values) {
            addCriterion("order_sn in", values, "orderSn");
            return (Criteria) this;
        }

        public Criteria andorderSnNotIn(List<String> values) {
            addCriterion("order_sn not in", values, "orderSn");
            return (Criteria) this;
        }

        public Criteria andOrderSnBetween(String value1, String value2) {
            addCriterion("order_sn between", value1, value2, "orderSn");
            return (Criteria) this;
        }

        public Criteria andOrderSnNotBetween(String value1, String value2) {
            addCriterion("order_sn not between", value1, value2, "orderSn");
            return (Criteria) this;
        }
            public Criteria andShippingIdIsNull() {
            addCriterion("shipping_id is null");
            return (Criteria) this;
        }

        public Criteria andShippingIdIsNotNull() {
            addCriterion("shipping_id is not null");
            return (Criteria) this;
        }

        public Criteria andShippingIdEqualTo(Integer value) {
            addCriterion("shipping_id =", value, "shippingId");
            return (Criteria) this;
        }

        public Criteria andShippingIdNotEqualTo(Integer value) {
            addCriterion("shipping_id <>", value, "shippingId");
            return (Criteria) this;
        }

        public Criteria andShippingIdGreaterThan(Integer value) {
            addCriterion("shipping_id >", value, "shippingId");
            return (Criteria) this;
        }

        public Criteria andShippingIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("shipping_id >=", value, "shippingId");
            return (Criteria) this;
        }

        public Criteria andShippingIdLessThan(Integer value) {
            addCriterion("shipping_id <", value, "shippingId");
            return (Criteria) this;
        }

        public Criteria andShippingIdLessThanOrEqualTo(Integer value) {
            addCriterion("shipping_id <=", value, "shippingId");
            return (Criteria) this;
        }
            public Criteria andShippingIdIn(List<Integer> values) {
            addCriterion("shipping_id in", values, "shippingId");
            return (Criteria) this;
        }

        public Criteria andshippingIdNotIn(List<Integer> values) {
            addCriterion("shipping_id not in", values, "shippingId");
            return (Criteria) this;
        }

        public Criteria andShippingIdBetween(Integer value1, Integer value2) {
            addCriterion("shipping_id between", value1, value2, "shippingId");
            return (Criteria) this;
        }

        public Criteria andShippingIdNotBetween(Integer value1, Integer value2) {
            addCriterion("shipping_id not between", value1, value2, "shippingId");
            return (Criteria) this;
        }
            public Criteria andShippingNameIsNull() {
            addCriterion("shipping_name is null");
            return (Criteria) this;
        }

        public Criteria andShippingNameIsNotNull() {
            addCriterion("shipping_name is not null");
            return (Criteria) this;
        }

        public Criteria andShippingNameEqualTo(String value) {
            addCriterion("shipping_name =", value, "shippingName");
            return (Criteria) this;
        }

        public Criteria andShippingNameNotEqualTo(String value) {
            addCriterion("shipping_name <>", value, "shippingName");
            return (Criteria) this;
        }

        public Criteria andShippingNameGreaterThan(String value) {
            addCriterion("shipping_name >", value, "shippingName");
            return (Criteria) this;
        }

        public Criteria andShippingNameGreaterThanOrEqualTo(String value) {
            addCriterion("shipping_name >=", value, "shippingName");
            return (Criteria) this;
        }

        public Criteria andShippingNameLessThan(String value) {
            addCriterion("shipping_name <", value, "shippingName");
            return (Criteria) this;
        }

        public Criteria andShippingNameLessThanOrEqualTo(String value) {
            addCriterion("shipping_name <=", value, "shippingName");
            return (Criteria) this;
        }
            public Criteria andShippingNameLike(String value) {
            addCriterion("shipping_name like", value, "shippingName");
            return (Criteria) this;
        }

        public Criteria andShippingNameNotLike(String value) {
            addCriterion("shipping_name not like", value, "shippingName");
            return (Criteria) this;
        }
            public Criteria andShippingNameIn(List<String> values) {
            addCriterion("shipping_name in", values, "shippingName");
            return (Criteria) this;
        }

        public Criteria andshippingNameNotIn(List<String> values) {
            addCriterion("shipping_name not in", values, "shippingName");
            return (Criteria) this;
        }

        public Criteria andShippingNameBetween(String value1, String value2) {
            addCriterion("shipping_name between", value1, value2, "shippingName");
            return (Criteria) this;
        }

        public Criteria andShippingNameNotBetween(String value1, String value2) {
            addCriterion("shipping_name not between", value1, value2, "shippingName");
            return (Criteria) this;
        }
            public Criteria andShippingNoIsNull() {
            addCriterion("shipping_no is null");
            return (Criteria) this;
        }

        public Criteria andShippingNoIsNotNull() {
            addCriterion("shipping_no is not null");
            return (Criteria) this;
        }

        public Criteria andShippingNoEqualTo(String value) {
            addCriterion("shipping_no =", value, "shippingNo");
            return (Criteria) this;
        }

        public Criteria andShippingNoNotEqualTo(String value) {
            addCriterion("shipping_no <>", value, "shippingNo");
            return (Criteria) this;
        }

        public Criteria andShippingNoGreaterThan(String value) {
            addCriterion("shipping_no >", value, "shippingNo");
            return (Criteria) this;
        }

        public Criteria andShippingNoGreaterThanOrEqualTo(String value) {
            addCriterion("shipping_no >=", value, "shippingNo");
            return (Criteria) this;
        }

        public Criteria andShippingNoLessThan(String value) {
            addCriterion("shipping_no <", value, "shippingNo");
            return (Criteria) this;
        }

        public Criteria andShippingNoLessThanOrEqualTo(String value) {
            addCriterion("shipping_no <=", value, "shippingNo");
            return (Criteria) this;
        }
            public Criteria andShippingNoLike(String value) {
            addCriterion("shipping_no like", value, "shippingNo");
            return (Criteria) this;
        }

        public Criteria andShippingNoNotLike(String value) {
            addCriterion("shipping_no not like", value, "shippingNo");
            return (Criteria) this;
        }
            public Criteria andShippingNoIn(List<String> values) {
            addCriterion("shipping_no in", values, "shippingNo");
            return (Criteria) this;
        }

        public Criteria andshippingNoNotIn(List<String> values) {
            addCriterion("shipping_no not in", values, "shippingNo");
            return (Criteria) this;
        }

        public Criteria andShippingNoBetween(String value1, String value2) {
            addCriterion("shipping_no between", value1, value2, "shippingNo");
            return (Criteria) this;
        }

        public Criteria andShippingNoNotBetween(String value1, String value2) {
            addCriterion("shipping_no not between", value1, value2, "shippingNo");
            return (Criteria) this;
        }
    }
}
