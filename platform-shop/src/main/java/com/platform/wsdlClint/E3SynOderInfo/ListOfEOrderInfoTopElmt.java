
package com.platform.wsdlClint.E3SynOderInfo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>ListOfEOrderInfoTopElmt complex type的 Java 类。
 * 
 * <p>以下模式片段指定包含在此类中的预期内容。
 * 
 * <pre>
 * &lt;complexType name="ListOfEOrderInfoTopElmt">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ListOfEOrderInfo" type="{http://www.siebel.com/xml/LP%20Order%20Syn%20E3%20IO}ListOfEOrderInfo"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ListOfEOrderInfoTopElmt", propOrder = {
    "listOfEOrderInfo"
})
public class ListOfEOrderInfoTopElmt {

    @XmlElement(name = "ListOfEOrderInfo", required = true)
    protected ListOfEOrderInfo listOfEOrderInfo;

    /**
     * 获取listOfEOrderInfo属性的值。
     * 
     * @return
     *     possible object is
     *     {@link ListOfEOrderInfo }
     *     
     */
    public ListOfEOrderInfo getListOfEOrderInfo() {
        return listOfEOrderInfo;
    }

    /**
     * 设置listOfEOrderInfo属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link ListOfEOrderInfo }
     *     
     */
    public void setListOfEOrderInfo(ListOfEOrderInfo value) {
        this.listOfEOrderInfo = value;
    }

}
