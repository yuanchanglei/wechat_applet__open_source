
package com.platform.wsdlClint.E3SynOderInfo;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>ListOfEOrderOutputInfo complex type的 Java 类。
 * 
 * <p>以下模式片段指定包含在此类中的预期内容。
 * 
 * <pre>
 * &lt;complexType name="ListOfEOrderOutputInfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="EOrderOutputEntry" type="{http://www.siebel.com/xml/LP%20Order%20E3%20Outputs%20IO}EOrderOutputEntry" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ListOfEOrderOutputInfo", namespace = "http://www.siebel.com/xml/LP%20Order%20E3%20Outputs%20IO", propOrder = {
    "eOrderOutputEntry"
})
public class ListOfEOrderOutputInfo {

    @XmlElement(name = "EOrderOutputEntry", required = true)
    protected List<EOrderOutputEntry> eOrderOutputEntry;

    /**
     * Gets the value of the eOrderOutputEntry property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the eOrderOutputEntry property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getEOrderOutputEntry().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link EOrderOutputEntry }
     * 
     * 
     */
    public List<EOrderOutputEntry> getEOrderOutputEntry() {
        if (eOrderOutputEntry == null) {
            eOrderOutputEntry = new ArrayList<EOrderOutputEntry>();
        }
        return this.eOrderOutputEntry;
    }

}
