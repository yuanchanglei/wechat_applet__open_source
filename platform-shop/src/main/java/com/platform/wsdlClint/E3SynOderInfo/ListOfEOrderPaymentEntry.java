
package com.platform.wsdlClint.E3SynOderInfo;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>ListOfEOrderPaymentEntry complex type�� Java �ࡣ
 * 
 * <p>����ģʽƬ��ָ�������ڴ����е�Ԥ�����ݡ�
 * 
 * <pre>
 * &lt;complexType name="ListOfEOrderPaymentEntry">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="EOrderPaymentEntry" type="{http://www.siebel.com/xml/LP%20Order%20Syn%20E3%20IO}EOrderPaymentEntry" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ListOfEOrderPaymentEntry", propOrder = {
    "eOrderPaymentEntry"
})
public class ListOfEOrderPaymentEntry {

    @XmlElement(name = "EOrderPaymentEntry")
    protected List<EOrderPaymentEntry> eOrderPaymentEntry;

    /**
     * Gets the value of the eOrderPaymentEntry property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the eOrderPaymentEntry property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getEOrderPaymentEntry().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link EOrderPaymentEntry }
     * 
     * 
     */
    public List<EOrderPaymentEntry> getEOrderPaymentEntry() {
        if (eOrderPaymentEntry == null) {
            eOrderPaymentEntry = new ArrayList<EOrderPaymentEntry>();
        }
        return this.eOrderPaymentEntry;
    }


	public void setEOrderPaymentEntry(List<EOrderPaymentEntry> eOrderPaymentEntry) {
		this.eOrderPaymentEntry = eOrderPaymentEntry;
	}

}
