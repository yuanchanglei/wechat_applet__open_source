
package com.platform.wsdlClint.E3SynOderInfo;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.platform.wsdlClint.E3SynOderInfo package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _ListOfEOrderInfo_QNAME = new QName("http://www.siebel.com/xml/LP%20Order%20Syn%20E3%20IO", "ListOfEOrderInfo");
    private final static QName _ListOfEOrderOutputInfo_QNAME = new QName("http://www.siebel.com/xml/LP%20Order%20E3%20Outputs%20IO", "ListOfEOrderOutputInfo");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.platform.wsdlClint.E3SynOderInfo
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link ListOfEOrderOutputInfo }
     * 
     */
    public ListOfEOrderOutputInfo createListOfEOrderOutputInfo() {
        return new ListOfEOrderOutputInfo();
    }

    /**
     * Create an instance of {@link ListOfEOrderOutputInfoTopElmt }
     * 
     */
    public ListOfEOrderOutputInfoTopElmt createListOfEOrderOutputInfoTopElmt() {
        return new ListOfEOrderOutputInfoTopElmt();
    }

    /**
     * Create an instance of {@link EOrderOutputEntry }
     * 
     */
    public EOrderOutputEntry createEOrderOutputEntry() {
        return new EOrderOutputEntry();
    }

    /**
     * Create an instance of {@link ListOfEOrderInfo }
     * 
     */
    public ListOfEOrderInfo createListOfEOrderInfo() {
        return new ListOfEOrderInfo();
    }

    /**
     * Create an instance of {@link EOrderEntry }
     * 
     */
    public EOrderEntry createEOrderEntry() {
        return new EOrderEntry();
    }

    /**
     * Create an instance of {@link ListOfEOrderInfoTopElmt }
     * 
     */
    public ListOfEOrderInfoTopElmt createListOfEOrderInfoTopElmt() {
        return new ListOfEOrderInfoTopElmt();
    }

    /**
     * Create an instance of {@link EOrderPaymentEntry }
     * 
     */
    public EOrderPaymentEntry createEOrderPaymentEntry() {
        return new EOrderPaymentEntry();
    }

    /**
     * Create an instance of {@link EOrderLineItemEntry }
     * 
     */
    public EOrderLineItemEntry createEOrderLineItemEntry() {
        return new EOrderLineItemEntry();
    }

    /**
     * Create an instance of {@link ListOfEOrderPaymentEntry }
     * 
     */
    public ListOfEOrderPaymentEntry createListOfEOrderPaymentEntry() {
        return new ListOfEOrderPaymentEntry();
    }

    /**
     * Create an instance of {@link ListOfEOrderLineItemEntry }
     * 
     */
    public ListOfEOrderLineItemEntry createListOfEOrderLineItemEntry() {
        return new ListOfEOrderLineItemEntry();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ListOfEOrderInfo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.siebel.com/xml/LP%20Order%20Syn%20E3%20IO", name = "ListOfEOrderInfo")
    public JAXBElement<ListOfEOrderInfo> createListOfEOrderInfo(ListOfEOrderInfo value) {
        return new JAXBElement<ListOfEOrderInfo>(_ListOfEOrderInfo_QNAME, ListOfEOrderInfo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ListOfEOrderOutputInfo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.siebel.com/xml/LP%20Order%20E3%20Outputs%20IO", name = "ListOfEOrderOutputInfo")
    public JAXBElement<ListOfEOrderOutputInfo> createListOfEOrderOutputInfo(ListOfEOrderOutputInfo value) {
        return new JAXBElement<ListOfEOrderOutputInfo>(_ListOfEOrderOutputInfo_QNAME, ListOfEOrderOutputInfo.class, null, value);
    }

}
