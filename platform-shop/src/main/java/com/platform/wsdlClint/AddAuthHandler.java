package com.platform.wsdlClint;

import java.util.Set;  

import javax.xml.namespace.QName;  
import javax.xml.soap.Name;
import javax.xml.soap.SOAPElement;  
import javax.xml.soap.SOAPException;  
import javax.xml.soap.SOAPHeader;  
import javax.xml.soap.SOAPMessage;  
import javax.xml.ws.handler.MessageContext;  
import javax.xml.ws.handler.soap.SOAPHandler;  
import javax.xml.ws.handler.soap.SOAPMessageContext;  
  
public class AddAuthHandler implements SOAPHandler<SOAPMessageContext> {  
  
    @Override  
    public boolean handleMessage(SOAPMessageContext context) {  
        Boolean output = (Boolean) context.get(MessageContext.MESSAGE_OUTBOUND_PROPERTY);  
        SOAPMessage message = context.getMessage();  
        if (output) {  
            try {  
                SOAPHeader header = message.getSOAPHeader();  
                if (header == null) {  
                    header = message.getSOAPPart().getEnvelope().addHeader();  
                }  
                header.setPrefix("");
                SOAPElement auth = header.addChildElement("Security", "wsse", "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd"); 
                
                SOAPElement auth1 = auth.addChildElement("UsernameToken", "wsse");
                auth1.addAttribute(new QName("xmlns:wsu"), "http://schemas.xmlsoap.org/ws/2002/07/utility");
                  
                SOAPElement name = auth1.addChildElement("Username", "wsse"); 
                name.addTextNode("H5USER");  
  
                SOAPElement password = auth1.addChildElement("Password", "wsse");          
                password.addAttribute(new QName("Type"), "wsse:PasswordText");
                password.addTextNode("H5USER!@3");  
                  
                message.saveChanges();  
  
            } catch (SOAPException e) {  
                e.printStackTrace();  
            }  
        }  
        try {  
            message.writeTo(System.out);  
        } catch (Exception e) {  
            e.printStackTrace();  
        }  
        return true;  
    }  
  
    @Override  
    public boolean handleFault(SOAPMessageContext context) {  
        return true;  
    }  
  
    @Override  
    public void close(MessageContext context) {  
    }  
  
    @Override  
    public Set<QName> getHeaders() {  
        return null;  
    }  
}  