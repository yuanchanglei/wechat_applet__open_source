package com.platform.task;

import com.platform.entity.*;
import com.platform.service.*;
import com.platform.util.wechat.WSDLUtilsForShop;
import com.platform.util.wechat.WechatRefundApiResult;
import com.platform.util.wechat.WechatUtil;
import com.platform.utils.DateUtils;
import com.platform.utils.RRException;
import com.platform.utils.ResourceUtil;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 系统定时任务（管理系统）
 * <p>
 * @author xudong
 * @date 2020年02月15日 下午1:34:24
 */
@Component("ShopTask")
public class ShopTask {
    private Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    private OrderService orderService;
    @Autowired
	private OrderGoodsService orderGoodsService;
    @Autowired
	private UserService userService;
    public void regularDelivery() {
        logger.info("PlatformTask_regularDelivery======定时将已付款的订单进行发货处理");
        Map<String, Object> map =  new HashMap<String, Object>();
        map.put("orderStatus", "201");//已付款标识
        map.put("shippingStatus", "0");//待发货标识
        List<OrderEntity> orderList = orderService.queryList(map);
        for(OrderEntity order : orderList) {
        	 Integer payStatus = order.getPayStatus();//付款状态
             if (2 != payStatus) {
            	 logger.error("此订单未付款******自动发货失败！orderid="+order.getId());
            	 continue;
             }else {
            	 orderService.autoSendGoods(order);
                 order = orderService.queryObject(order.getId());
                 UserEntity user = userService.queryObject(order.getUserId());
                 Map<String, Object> params =new HashMap<String, Object>();
                 params.put("orderId", order.getId());
                 List<OrderGoodsEntity> list=  orderGoodsService.queryList(params);
                 String type ="Shipped";
                 if(ResourceUtil.getConfigByName("sys.demo").equals("1")){
                     //演示环境不需要注册账号
                 }else {
                 	WSDLUtilsForShop.OrderSynchronization(order,list,user,type,null);
                 }
                 logger.info("自动发货已完成！orderid="+order.getId());
             }
        }
    }



}
